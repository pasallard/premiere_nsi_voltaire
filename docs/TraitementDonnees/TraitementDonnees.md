# Traitement de données en tables :microscope:


## ^^1 - Éléments de cours sur le traitement de données en table^^

### 1.1. Bases de données, tables de données

Une des utilisations importantes de l'informatique est le traitement des innombrables données récupérées dans différents domaines, de la santé jusqu'à la publicité. Les données sont regroupées en __tables de données__ et plusieurs tables de données forment une __base de données__.

Des langages et des logiciels spécialisés existent pour traiter ces tables de données, qui peuvent contenir des millions de données. Nous allons explorer quelques fonctions de base de ces langages, en les programmant en Python.

Les tables de données sont structurées : chaque ligne d'une table de données est appelé un __enregistrement__, et chaque enregistrement possède les mêmes champs, qui sont identifiés par des __descripteurs__.

![bdd](drawit-diagram-20.png){: .center}


### 1.2. Le format CSV


Le format de fichier __CSV__ est un format standard pour un fichier de données. Il est reconnu par tous les éditeurs de texte (Bloc-Note, Notepad, Word, LibreOffice Writer, etc.) et les tableurs (Excel, LibreOffice Calc, etc.). 

CSV siginifie ***Comma Separated Values***, c'est-à-dire "valeurs séparées par des virgules" ; mais en pratique, on peut aussi choisir un autre délimiteur (point-virgule, tabulation...). 

Le contenu d'un fichier CSV est stocké sous forme de texte, ce qui le rend facilement compréhensible à la fois par les humains et par les machines.

!!! warning "Gestion des nombres à la lecture d'un fichier CSV"

    À la lecture d'un fichier CSV dont certaines colonnes sont des champs de type numérique, 
    
    * un tableur (Excel, LibreOffice Calc, etc.) les traite bien comme des nombres ;
    * mais Python considère que tous les champs sont des chaînes de caractères, et il faudra prévoir dans le code une conversion en nombre grâce aux fonctions `int` et `float`.



!!! question "Première exploration d'un fichier CSV"
    Dans cet exercice, le délimiteur des fichiers csv est le `";"`.

    * Télécharger le [fichier "countries.csv"](countries.csv) (clic-droit et `enregistrer la cible du lien sous`).
    * Retrouver ce fichier avec l'explorateur de fichier puis, sur l'icône de ce fichier, faire un clic-droit puis `Ouvrir avec`.
    * Sélectionner un éditeur de texte (Bloc-notes ou Notepad++) : la forme brute avec les délimiteurs est visible.
    * Ouvrir ensuite ce même fichier  "countries.csv" avec un tableur (Excel ou LibreOffice) : le formatage des enregistrements en ligne et colonnes est visible.

    _Remarque :_ les données proviennent de l'ONU et la langue utilisée est l'anglais.



### 1.3. Lecture d'une table de données en Python.

Ouvrir un éditeur Python (Thonny par exemple) 

!!! info "Conseils pour la lecture d'un fichier CSV en Python" 

    * Ouvrir un éditeur Python (Thonny par exemple) et faire `Fichier/Nouveau` et puis `Fichier/Enregistrer sous` pour enregistrer ce programme vide pour l'instant dans votre répartoire de travail.
    * Le fichier `countries.csv` doit être déplacé dans le même répertoire que le programme Python.
    * Un fichier peut être lu dans un programme Python, grâce à la fonction `open('NomDuFichier','r')` (où l'option `r` est l'abréviation de _read_).
    * Il ne faut pas oublier de fermer le fichier, avec la méthode `close()`.



!!! question "Exploration rudimentaire d'un fichier CSV en Python"

    Copier-coller le code ci-dessous puis exécuter le programme :

    ```python
    import csv
    #traitement rudimentaire, avec un tableau à deux dimensions
    pays = []
    monFichier = open('countries.csv','r') # Ouverture du fichier .csv
    monLecteur = csv.reader(monFichier, delimiter=';') # Création d'un objet "lecteur"
    for ligne in monLecteur:
        pays.append(ligne)
    monFichier.close()  # Toujours fermer le fichier !!!

    # Visualisation de toutes les données
    for p in pays :
        print(p)

    ```

    Que constatez-vous ?

??? danger "Solution"

    On voit bien à l'affichage tout le contenu du fichier, mais il y a des inconvénients :

    - Les données ne sont pas structurées : la première ligne est la ligne des **descripteurs** (ou **champs**), alors que les lignes suivantes sont les valeurs de ces descripteurs.
    - On a perdu le lien entre un descripteur et une valeur : pour un enregistrement, plusieurs nombres apparaissent et on ne sait plus forcément lequel correspond à la population, lequel à la superficie, etc.


!!! question "Exploration d'un fichier CSV à l'aide d'un tableau de dictionnaires"

    Remplacer le code précédent par celui ci-dessous puis exécuter le programme :

    ```python
    import csv
    # traitement plus élaboré : un tableau de dictionnaires
    pays = []
    monFichier = open('countries.csv','r')   # Ouverture du fichier .csv
    monLecteur = csv.DictReader(monFichier, delimiter=';')   # Objet DictReader (itérateur)
    for ligne in monLecteur:
        pays.append(dict(ligne)) # Conversion en dictionnaire grâce à la fonction dict
    monFichier.close() # Toujours fermer le fichier !!!

    # Visualisation de toutes les données
    for p in pays :
        print(p)
    ```

    Que constatez-vous ?

??? danger "Solution"

    On voit bien à l'affichage tout le contenu du fichier, où chaque enregistrement est stocké sous forme d'un dictionnaire `p` dont les clés sont les **descripteurs** de la table de données.


## ^^3 - Exploitation des données : interrogations simples^^


On peut traduire en Python des questions simples :

* question 1 : combien d'habitants y a-t-il en Finlande ?

```python
reponse1 = [p['Population'] for p in pays if p['Name']=='Finland']
print(reponse1)
```

* question 2 : quels sont les pays où l'on paye en euro ?

```python
reponse2 = [p['Name'] for p in pays if p['Currency_Code'] =='EUR']
print(reponse2)
```

* question 3 : quels sont les pays où la population dépasse 100 millions d'habitants ? _Donner la réponse sous forme d'un couple `(pays, continent)`_

```python
reponse3 = [(p['Name'],p['Continent']) for p in pays if int(p['Population']) > 10**8] # conversion du champ 'Population' en nombre entier
print(reponse3)
```

!!! question "Exercice"

    1. Quels sont les pays dont le nom de la monnaie (`Currency_Name`) est `Pound` et dans quel continent sont-ils ?
    2. Quels sont les pays dont la superficie (`Area`) est inférieure à 100 km^2^ et quelle est leur superficie ? _Attention, les superficies sont des nombres à virgule, à convertir avec la fonction_ `float`.
    3. Quels sont les pays d'Europe qui ont une population comprise entre 20 et 40 millions d'habitants ?

??? danger "Solution"

    1. On saisit l'instruction 
    ```python
    print([(p['Name'],p['Continent']) for p in pays if p['Currency_Name'] == 'Pound'] )
    ```
    et on obtient une liste de 13 couples qui commence par `('Egypt', 'AF')` et qui finit par `('Syria', 'AS')`.
    2. On saisit l'instruction 
    ```python
    print([(p['Name'],p['Area']) for p in pays if float(p['Area']) < 100 ])
    ``` 
    et on obtient une liste de 14 couples qui commence par `('Saint Barthelemy', '21')` et qui finit par `('Vatican', '0.44')`.
    3. On saisit l'instruction
    ```python
    print([(p['Name']) for p in pays if p['Continent'] == 'EU' and int(p['Population']) > 20*10**6 and int(p['Population']) < 40*10**6  ] )
    ``` 
    et on obtient seulement deux pays : Pologne et Roumanie.



## ^^4. Tri d'une table^^


Il existe dans Python une fonction `sorted` qui permet de trier un tableau. Par exemple, l'instruction `tab_trié = sorted(tab)` permet, à partir d'un tableau `tab`, de créer une nouvelle variable `tab_trié` sans modifier le tableau `tab`.

{{ IDEv('scriptsorted') }}

Mais comment trier un tableau de dictionnaires ? 
{{ IDE('scriptsimpsons') }}

Il est normal que cette tentative échoue : Python ne peut pas savoir si on veut trié par age, par ordre alphabétique, etc.

Il faut definir une __clé__ de tri. Ici, nous choisissons de trier par age.

{{ IDEv('scriptagesimpsons') }}

__Revenons__ sur le traitement du fichier `countries` : si on veut trier les pays par ordre croissant de superficie, on exécute le code suivant :

```python
# définition d'une clé
def cle_superficie(p):
    superficie = float(p['Area']) # conversion en nombre flottant
    return superficie

# tri des pays par superficie 

pays_Tri1 = sorted(pays, key = cle_superficie)

# affichage des cinq plus petits pays
for k in range(5):
    print(pays_Tri1[k])
```

!!! question "Exercice"

    Écrire le code pour trier les pays par ordre croissant de population et pour afficher le nom des 8 pays les plus peuplés au monde.

    _Indication_ : pour trier dans l'ordre décroissant, il faut ajouter à la fonction `sorted` le paramètre `reverse = True`.


??? danger "Solution"

    On écrit le code suivant :
    ```pycon
    def cle_popu(p):
    popu = int(p['Population']) # conversion en nombre entier
    return popu
    # tri des pays par population
    pays_Tri2 = sorted(pays, key = cle_popu, reverse=True)
    # affichage des 8 pays les plus peuplés
    for k in range(8):
        print(pays_Tri2[k]['Name'])
    ```

## ^^5. TP : croisement de données^^

Télécharger ce [fichier Jupyter](CroisementDeDonnees.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

## ^^6 - QCM^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Introduction aux bases de données"**.