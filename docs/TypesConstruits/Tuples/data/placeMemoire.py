#--- HDR ---# 
import sys
#--- HDR ---#
monTab = [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0]
monTuple = (100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0)

print("Ce tableau occupe une place mémoire de ", sys.getsizeof(monTab), " octets")
print("Ce tuple occupe une place mémoire de ", sys.getsizeof(monTuple), " octets")
