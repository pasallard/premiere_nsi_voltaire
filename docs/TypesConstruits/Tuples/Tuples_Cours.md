# Les p-uplets (_tuples_ en Python) :dango:


## ^^1 - Définition des tuples^^

Sans le savoir, vous utilisez des __p-uplets__ (appelés *tuples* en langage Python) depuis longtemps en mathématiques. 

Par exemple quand vous utilisez les coordonnées d'un point $A(3,2)$ dans le plan : le co**uple** (3,2) est un _tuple_ à 2 éléments.

![](data/pointPlan2.png){: .center}

Si vous travaillez avec des points de l'espace, vous utiliserez un __triplet__ (ou *3-uplet* ou *tuple* à 3 éléments) pour donner les coordonnées d'un point : $A(2,-2,3)$.

![](data/pointEspace2.png){: .center}

!!! note ""
    En Python, un _tuple_ ressemble au premier abord à un tableau (une liste). Mais il s'en différencie d'abord par l'utilisation des **parenthèses au lieu des crochets**.

    {{ IDE("data/premierTuple")}}

Un petit avantage des _tuples_ par rapport au tableau est le fait qu'ils prennent moins de place en mémoire :

{{ IDE("data/placeMemoire")}}

L'autre grande différence par rapport aux tableaux est qu'un _tuple_ est une collection d'objets **NON MODIFIABLE** (on dit _non mutable_) : voir le paragraphe suivant.  

## ^^2 - Accès aux éléments d'un p-uplet^^

Comme pour une liste ou une chaîne de caractère, l'accès à un élément d'un p-uplet se fait par un indice entre **crochets**, comme dans le code ci-dessous :

```python
people = ('Ada', 'Lovelace', 1815, 1852) 
prenom = people[0] # la variable prenom est alors égale à 'Ada'
naissance = people[2] # la variable naissance est alors égale à 1815
```

!!! question "Exercice"

    En appuyant sur le bouton "Lancer", vous allez exécuter un code caché qui crée un p-uplet `tuple_mystere`.

    {{IDE("data/tuple_mystere")}}
    
    {{ multi_qcm(
        [
            "Quel nom de mousquetaire y a-t-il en deuxième position dans ce tuple ?",
            ["Athos", "Portos", "Aramis", "d'Artagnan"],[2],
        ],
        )}}

    ??? danger "Explication"
        On saisit dans la console `tuple_mystere[1]`, car l'élément en 2^e^ position a un indice égal à 1.


!!! warning "Impossible de modifier un élément d'un p-uplet"
    Il est impossible de : 
    
    * changer juste un élément d'un tuple : vérifiez-le en saisissant dans la console de l'exercice précédent `tuple_mystere[1]="Batman"`.
    * ajouter un élément à la fin d'un tuple avec un `append` : vérifiez-le en saisissant dans la console `tuple_mystere.append("Robin")`.

    Si on est obligé de changer un élément d'un _tuple_, on est obligé de redéfinir complètement le _tuple_ : vérifiez-le en saisissant dans la console `tuple_mystere = ("Batman", "Robin")`, puis visualiser le "nouveau" `tuple_mystere`.

Il existe une **autre possibilité** pour récupérer les valeurs des éléments d'un _tuple_, **quand on connait le nombre d'éléments** du _tuple_. Supposons par exemple que l'on dispose d'un _tuple_ `monTuple` à deux éléments. L'instruction
```python
(x,y) = monTuple
```
est équivalente (et plus courte) à la double affectation
```python
x = monTuple[0]
y = monTuple[1]
```

## ^^3 - Parcours d'un p-uplet^^

La fonction `len` fonctionne également avec des _tuples_ et, comme pour les listes, on peut parcourir les éléments d'un _tuple_ avec une boucle POUR :

* soit à l'aide de leurs indices,  avec une instruction du type `for k in range(len(monTuple))` ;
* soit directement, avec une instruction du type `for elt in monTuple`.


!!! question "Exercice"
    Dans une partie cachée du code ci-dessous, un tuple `myFriend` a été créé. Compléter les pointillés pour permettre l'affichage de chaque élément individuel de ce tuple.

    {{IDE("data/tuple_mystere2")}}

    ??? danger "Solution"
        On écrit :
        ```python 
        # parcours version 1
        for k in range(len(myFriend)) :
            print(myFriend[k]) 

        # parcours version 2
        for elt in myFriend :
            print(elt)
        ```

## ^^4 - Fonctions et p-uplets^^

Une fonction Python peut :

- prendre en entrée un ou plusieurs _tuple(s)_ ;
- renvoyer un _tuple_ (créé au sein de la fonction ou bien créé directement au niveau du `return`)

!!! question "Exercice"
    En géométrie repérée, le milieu $M$ de deux points $A(x_A,y_A)$ et $B(x_B,y_B)$ est le point dont les coordonnées sont les moyennes de celles de $A$ et de $B$ : on a donc les formules $\begin{cases} x_M = \frac{x_A+x_B}{2}\\ y_M = \frac{y_A+y_B}{2}\end{cases}$.

    On veut écrire une fonction `milieu` qui prend en entrée deux _tuples_ représentant les coordonnées de deux points A et B et qui renvoie le tuple représentant les coordonnées de leur milieu M.

    Compléter le code suivant :

    {{ IDE("data/milieu")}}

    ??? danger "Solution"
        Une solution est la suivante :
        ```python
        def milieu(pointA, pointB):
            """ Calcule les coordonnées du milieu des deux points A et B.
            Entrée : deux tuples à deux éléments 
            Sortie : un tuple"""
            (xA,yA) = pointA
            (xB,yB) = pointB
            xM = (xA+xB)/2
            yM = (yA+yB)/2
            pointM = (xM, yM)
            return pointM # on peut gagner une ligne en écrivant return (xM, yM)
        ```


## ^^5 - En synthèse^^

Il y a beaucoup de similarités entre _tuples_ et _tableaux_ :

- un _tuple_ se déclare avec des parenthèses et un tableau se déclare avec des crochets ;
- le maniement de base est identique (accès, longueur, parcours des éléments, ...) ;
- mais on ne peut pas modifier "l'intérieur" d'un _tuple_ (pas de changement d'affectation d'un élément, pas de `append`, etc.)

Quel peut être l'intérêt d'un tuple par rapport à un tableau ?

- Justement son caractère protégé : jamais une portion de code ne pourra venir modifier les informations contenues dans le tuple. 
- L'utilisation des tuples est économe en ressources-système par rapport à une liste (en particulier, parce que sa taille est fixe).


## ^^6 - Exercices et TP^^

* Exercices et TP: télécharger le [carnet Jupyter](NSI1_tuples_Exos_TP.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.


## ^^7 - QCM d'entrainement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Tuples"**.
