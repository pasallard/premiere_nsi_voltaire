# Les Tableaux (partie I) :postbox:

## ^^1 - Définition d'un tableau (ou d'une liste)^^

Les tableaux (ou *listes* dans la terminologie Python) font partie de ce qu'on appelle les *données composites* (nous verrons plus tard les *tuples* et les *dictionnaires*). Elles permettent de regrouper de manière structurée différentes valeurs.

En voici un premier exemple : `eleve1 = ["Alice", "Dupont", 2005, 14.2 ]`. 

Dans ce tableau `eleve1`, nous avons successivement 

* une chaine de caractères (`str`), qui semble représenter le prénom ;
* une autre chaine de caractères (`str`), qui semble représenter le nom de famille ;
* un nombre entier (`int`), qui peut représenter l'année de naissance de l'élève ;
* et un nombre flottant (`float`), qui peut représenter la moyenne trimestrielle de l'élève.

!!! note ""
    Dans un tableau, les éléments sont séparés par des **virgules**, et tout l'ensemble est délimité par des **crochets**.

Dans la suite du cours, nous n'utiliserons que des tableaux dont les **éléments sont de même type**.

!!! done "Exemples"
    
    * un tableau de chaines de caractères : `semaine = ["Lundi", "Mardi","Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]`

    * un tableau de nombres : `notes_Alice = [12, 14, 9, 16]`

En Python, le *type* d'un tableau est `list`, ce qui explique pourquoi on emploie aussi le terme de __liste__ à la place de __tableau__.

{{IDE("data/tab_type")}}

!!! question "Exercice : création d'un tableau"
    Créer un tableau `saisons` qui contiendra les noms des quatre saisons de l'année.

    {{ terminal() }}

    ??? danger "Solution"
        On écrit `saisons = ["printemps", "été", "automne", "hiver"]`.


## ^^2 - Accès aux éléments d'un tableau^^
 

!!! tip "Rang ou indice d'un élément d'un tableau :heart:"
    On peut considérer un tableau comme un ensemble de **cases numérotées** : le numéro d'une case s'appelle le **rang** ou l'**indice** de l'élément.

    On numérote les cases d'un tableau **à partir de zéro** !

    Par exemple, dans le tableau `semaine = ["Lundi", "Mardi","Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]`, la chaine de caractères `"Lundi"` a pour indice 0, `"Mardi"` a pour indice 1, etc.

{{ IDE("data/tab_semaine")}}

!!! note ""
    On accède à un élément d'un tableau en mettant entre crochets l'__indice__ de l'élément (_voir l'exemple ci-dessus_).

    On peut créer une variable pour "récupérer" cette valeur, comme par exemple :
    ```python
    premier_jour = semaine[0]
    ```


!!! question "Exercice : découvrir les éléments d'un tableau"
    En appuyant sur le bouton "Lancer", vous allez exécuter un code caché qui crée un tableau `tab_mystere`.

    {{ IDE("data/tab_mystere1") }}

    {{ multi_qcm(
        [
            "Quel mot y a-t-il en deuxième position dans ce tableau ?",
            ["Renard", "Corbeau", "Cigale", "Fourmi"],[2],
        ],
        )}}

    ??? danger "Solution"
        L'élément en 2^e^ position est celui de rang 1 : on saisit alors dans le terminal `tab_mystere[1]`.

        _Remarque_ : on peut aussi "tricher" en affichant tout le tableau avec un `print(tab_mystere)` puis voir celui qu'il y a en deuxième position ! 

!!! warning "Erreur classique : Index out of range"
    Utiliser un indice qui "sort du tableau" provoquera une erreur `list index out of range`.

    Vous pouvez le vérifier en saisissant dans la console ci-dessus `tab_mystere[100]`.

## ^^3 - Modification d'un élément d'une liste^^

Une liste est MODIFIABLE : on dit que les listes sont des objets **mutables**.

C'est une différence fondamentale avec un autre type de données (les *tuples*) que nous verrons plus tard. 

!!! note ""
    Pour modifier un élément d'une liste, on lui affecte une nouvelle valeur. 

    Par exemple, si on veut modifier le premier élément de la liste `famille = ["Bart", "Lisa", "Maggie", "Homer"]` et le remplacer par `"Milhouse"`, on écrit : `famille[0] = "Milhouse"`.

    {{ IDE("data/tab_famille")}}


!!! question "Exercice"
    Avec le tableau `famille` ci-dessus, écrire l'instruction qui permet de remplacer `"Homer"` par `"Marge"`.

    ??? danger "Solution"
        On écrit `famille[3] = "Marge"` car il s'agit de l'élément de rang 3 (celui en 4^e^ position !).


## ^^4 - Longueur d'un tableau^^

La longueur d'un tableau est donnée par la fonction `len()`. Cela permet donc de connaître le nombre d'éléments dans le tableau.

Par exemple, avec le tableau `famille` ci-dessus, `len(tableau)` vaut 4.

!!! question "Exercice : découvrir la longueur d'un tableau"
    {{ IDE("data/tab_mystere2") }}
    
    {{ multi_qcm(
        [
            """Quelle est la longueur du tableau `tab_mystere` rencontré précédemment ?""",
            ["6", "7", "8", "9"],[3],
        ],
        shuffle = False
        )}}

    ??? danger "Solution"
        On saisit dans la console `len(tab_mystere)`.

!!! warning "Erreur classique : l'indice du dernier élément d'un tableau"
    Comme les indices démarrent à 0, le dernier élément d'un tableau `tab` a pour indice `len(tab)-1`.


!!! question "Exercice : découvrir le dernier élément d'un tableau"
    {{ multi_qcm(
        [
            """Quel est le dernier élément du tableau `tab_mystere` rencontré précédemment ?""",
            ["Renard", "Corbeau", "Cigale", "Fourmi", "Lièvre", "Tortue"],[6],
        ],
        )}}
    ??? danger "Solution"
        Puisque ce tableau a pour longueur 8, on saisit dans la console `tab_mystere[7]`.

!!! question "QCM d'entrainement"
    Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Tableaux (introduction)"**.


## ^^5 -  Parcours des éléments d'une liste^^

Une première méthode pour parcourir l'ensemble des éléments d'un tableau `tab` est d'utiliser une boucle bornée (boucle `for`) où l'indice de boucle (`k` dans l'exemple ci-dessous) prend toutes les valeurs possibles des rangs des éléments du tableau grâce à l'instruction `for k in range(len(tab))`.

{{ IDEv("data/tab_famille_2")}}

!!! question "Exercice"
    Dans le script ci-dessous, les deux tableaux de nombres `a` et `b` ont exactement la même taille.

    On veut trouver le nombre qui est exactement à la même place dans les deux tableaux.

    Compléter les pointillés.

    {{ IDE("data/parcours1")}}

    ??? danger "Solution"

        ```python
        for k in range(len(a)) :
            if a[k] == b[k] :
                print(a[k], " au rang ", k, " est à la même position dans les deux tableaux")
        ```

!!! question "Exercice"
    Décrire le fonctionnement du programme suivant. Que représente la valeur de la variable `M` à la fin du programme ? 

    ```python
    notes_Alice = [12, 14, 9, 16]
    S = 0
    for k in range(len(notes_Alice)):
        S = S + notes_Alice[k]
    M = S / len(notes_Alice)
    ```

    ??? danger "Solution"
        Grâce à la boucle `for`, on additionne toutes les valeurs du tableau et on stocke le résultat dans la variable `S`. Puis on divise cette somme par la longueur du tableau (ici 4) : cela revient à faire une **moyenne** des quatres valeurs. On peut ainsi supposer que la variable `M` représente la moyenne des notes d'Alice.

## ^^6 - Tableaux et chaines de caractères^^

Par beaucoup d'aspects, une chaine de caractère (de type `str`) peut être manipulée comme une liste (de type `list`).

!!! done "Exemple"
    
    {{IDE("data/mot_liste")}}

Ceci sera très utile pour des applications de cryptographie (codage/décodage de messages secrets) : voir le TP "Codage de César".

## ^^7 - Construction de grands tableaux^^

Quand on veut construire de grands tableaux, il est pénible d'écrire chaque élément un par un.

Une première méthode (_nous en verrons d'autres plus tard_) pour construire un tableau de taille fixée (de taille 25 dans l'exemple ci-dessous) et rempli de '0' est la suivante :

```python 
tab = [0] * 25
```
!!! done ""
    Recopier cette instruction dans la console, puis visualiser la variable `tab`.

{{ terminal() }}

On peut ensuite remplir ce tableau `tab` avec les valeurs de notre choix en modifiant chaque élément du tableau. Par exemple, si on veut que le tableau `tab` contienne les 25 premiers nombres impairs, on écrit :

```python
for k in range(len(tab)) :
    tab[k] = 2*k+1
```

!!! question "Exercice : liste des carrés parfaits"
    On appelle _carré parfait_ un nombre entier qui est le carré d'un autre nombre entier : par exemple 36 est un carré parfait car 36 = 6^2^.

    La liste des carrés parfaits commence donc ainsi : 0, 1, 4, 9, 16, 25, 36...

    Écrire le code permettant de créer une liste `carres_parfaits` contenant les 15 premiers carrés parfaits.

    {{ IDE("data/tab_parfaits")}}

    ??? danger "Solution"
        On écrit le script suivant :
        ```python
        carres_parfaits = [0] * 15
        for k in range(len(carres_parfaits)) : 
            carres_parfaits[k] = k**2
        ```

!!! note ""
    Nous avons utilisé plus haut le symbole `*` pour créer un grand tableau. Le symbole `+` sert, quant à lui, à _concaténer_ deux tableaux, c'est-à-dire à les "coller" l'un après l'autre.

{{ IDE("data/concat_tab")}}


## ^^8 - Tableaux et fonctions^^

Un tableau est une _variable_ (de type `list`), comme peuvent l'être des nombres (de type `int` ou `float`) ou des chaines de caractères (de type `str`). 

Une fonction Python peut donc :

- recevoir un tableau en paramètre ;
- renvoyer (`return`) un tableau.

!!! done "Exemple"
    La fonction `decompose` prend une chaine de caractères `mot` en argument et renvoie le tableau composé de chacune des lettres de ce mot.

    Tester cette fonction en faisant des appels en console avec des mots de votre choix. 

    {{ IDE("data/decompose_mot")}}

!!! question "Exercice"

    On donne le code d'une fonction `carré`.

    {{ IDE("data/tab_carre")}}
    
    {{ multi_qcm(
        [
            """Quelle valeur va être renvoyée après l'appel `carré([2,5,8])` ?""",
            ["[4,25,64]","[0,0,0]","[0,1,4]", "[4,10,16]" ],[1],
        ],
        )}}
    ??? danger "Explication"
        En console, on peut saisir `carré([2,5,8])` pour visualiser le résultat de cet appel.

        On voit que la fonction :

        - crée d'abord un tableau `tab_carre` rempli de 0 et de même taille que le tableau `tab` passé en argument ;
        - puis modifie chaque valeur (_remplace chaque 0_) par `tab[k]**2`, c'est-à-dire par le carré de chaque nombre du tableau de départ.

## ^^9 - Tableaux à deux dimensions (listes de listes, matrices)^^

Vous avez déjà rencontré des tableaux à deux dimensions, comme par exemple un "Sudoku" :
![](data/sudoku.png){: .center}

Ce sudoku, ce "tableau à deux dimensions", peut être représenté comme un ***tableau de tableaux*** : 
```python
sudoku = [ [3,9,1], # on a sauté une ligne 
            [4,8,7], # pour faciliter la lecture
            [6,5,2]] # mais ce n'est pas obligé
```

Le (grand) tableau `sudoku` composé de trois (petits) tableaux qui correspondent aux lignes du Sudoku. Ainsi, `sudoku[0]` est égal à `[3,9,1]`, tandis que `sudoku[2][0]` est égal au nombre 6 (car `sudoku[2]` est le petit tableau `[6,5,2]`).

> Remarque : la notation `sudoku[2][0]` est en réalité un raccourci de la notation `(sudoku[2])[0]`, où les parenthèses délimitent clairement l'ordre des opérations.

!!! question "Exercice"

    On saisit dans un éditeur Python les deux lignes suivantes de code :

    ```python
    tab = [ [1,2,3,4], [5,6,7,8], [9,10,11,12]]
    var = tab[1][3]
    ```
    {{ multi_qcm(
        [
            """Quelle est la valeur de la variable `var` ?""",
            ["8","3","10","9"],[1],
        ],
        )}}
    ??? danger "Explication"
        `tab[1]` est l'élément de rang 1 de `tab` : donc `tab[1] = [5,6,7,8]`. Puis on prend l'élément de rang 3 de ce "petit tableau : c'est celui qui est en 4^e^ position !


## ^^10 - Exercices et TP^^

* Exercices généraux (tableaux simples) : télécharger le [carnet Jupyter](NSI1_tableaux_Exos_1.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.
* Exercices généraux (tableaux à deux dimensions) : télécharger le [carnet Jupyter](NSI1_tableaux_Exos_1_Matrices.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.
* TP jeu du Morpion : télécharger d'abord le [carnet Jupyter](NSI1_tableaux_TP_Morpion.ipynb), puis le [programme Python](mon_morpion.py) pour jouer avec une interface graphique.

## ^^11 - QCM d'entrainement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Tableaux Partie I"**.

## ^^12 - Projet Cryptographie^^

* Introduction à la cryptographie avec le chiffre de César : télécharger le [carnet Jupyter](NSI1_tableaux_TP_Cesar.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.
* [Fichier pdf](Fiches_Projets_Crypto.pdf) de présentation du projet Crypto.
