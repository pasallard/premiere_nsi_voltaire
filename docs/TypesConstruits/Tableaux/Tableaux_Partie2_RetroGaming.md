# TP : Retrogaming et utilisation de tableaux


Le but du TP est de faire fonctionner un jeu de combat spatial basé sur ***Pyxel***, un moteur de jeu vidéo rétro pour Python.


<br />
<center>
<img src="https://pasallard.gitlab.io/premiere_nsi_voltaire/TypesConstruits/Tableaux/data/pyxel-tutoriel-05.gif" width=128 />
<img src="https://pasallard.gitlab.io/premiere_nsi_voltaire/TypesConstruits/Tableaux/data/pyxel-tutoriel-06.gif" width=128 />
</center>


!!! note ""
    Pour cela, il faudra manipuler des tableaux :

    - les coordonnées du vaisseau, des tirs et des ennemis sont des tableaux de la forme `[x,y]`, où les coordonnées `x` et `y` sont comptées en nombre de pixels depuis le coin en haut à gauche. Par exemple, `[30, 120]` représente un objet situé à 30 pixels du bord gauche et à 120 pixels du bord supérieur.
    - les différents tirs et les différents ennemis sont stockés dans des tableaux `liste_tirs` et `listes_ennemis` contenant ces "petits tableaux" de coordonnées. Par exemple, `tirs_liste = [ [60,50], [45,72], [105,20] ]` représente trois tirs aux positions (60,50), (45,72) et (105,20).


## Environnement de travail

Ouvrir un éditeur Python comme Thonny ou EduPython.

## Étape 1 : déplacement du vaisseau

- Télécharger le fichier [space_1.py](data/space_1.py).
- Analyser le code, en particulier la fonction `draw()`.
- Compléter les lignes 20 et 26 puis tester le jeu.


## Étape 2 : création des tirs

- Télécharger le fichier [space_2.py](data/space_2.py) et copier/coller son contenu à l'emplacement indiqué.
- Analyser le code, en particulier la fonction `draw()`.
- Compléter le code des fonctions `tirs_creation` et `tirs_deplacement` puis tester le jeu.

## Étape 3 : création des ennemis

- Télécharger le fichier [space_3.py](data/space_3.py) et copier/coller son contenu à l'emplacement indiqué.
- Analyser le code, en particulier la fonction `draw()`.
- Compléter le code des fonctions `ennemis_creation` et `ennemis_deplacement` puis tester le jeu.

## Étape 4 : gestion des explosions et du nombre de vies

- Télécharger le fichier [space_4.py](data/space_4.py) et copier/coller son contenu à l'emplacement indiqué.
- Analyser le code, en particulier la fonction `draw()`.
- Compléter le code des fonctions `vaisseau_suppression` et `ennemis_suppression` puis tester le jeu.

## Étape 5 : améliorations graphiques

- Télécharger le fichier [space_5.py](data/space_5.py) et copier/coller son contenu à l'emplacement indiqué.
- Suivre les instructions pour dessiner, pixel par pixel, votre vaisseau, vos tirs et vos ennemis.

## Étape 6 : ajouter d'autres fonctionnalités
- Ajouter et afficher un score, selon la règle "1 point par ennemi abattu". _Indication : le jeu affiche déjà le nombre de vies, il faut s'inspirer de cette partie du code._
- Ajouter des rochers qui bloquent vos tirs mais qui ne sont pas détruits par les tirs.
- Et à vous d'imaginer d'autres améliorations :stuck_out_tongue_winking_eye: