# Les Tableaux (partie II) :postbox:

> Beaucoup des techniques que nous allons voir dans cette partie sont spécifiques à des langages de programmation comme Python et ne peuvent pas être utilisées avec d'autres langages comme Java ou C/C++.

## ^^1 - Parcours d'un tableau ; test d'appartenance à un tableau^^

### 1.1 - Parcours d'un tableau

Nous avons vu précédemment une première méthode pour parcourir les éléments d'un tableau :

{{ IDE("data/tab2_parcours1")}}


Il existe en Python une méthode plus directe de parcours d'une liste, _plus proche du langage anglais de tous les jours_ :

{{ IDE("data/tab2_parcours2")}}

!!! question "Exercice"
    Que va afficher le code suivant ? 

    ```python
    irregulier = ["bijou","chou", "caillou","hibou"]
    for mot in irregulier:
        print(mot+"x")
    ```
    ??? danger "Solution"
        Il va afficher successivement "bijoux","choux", "cailloux" et "hiboux".


### 1.2 - Test de l'appartenance à un tableau

On peut tester si un élément `x` appartient à un tableau `tab` à l'aide d'une instruction conditionnelle de la forme `if x in tab` : 

{{ IDE("data/tab2_appartient")}}


## ^^2 - Ajout d'un élement à une liste : méthode **append()**^^

On reprend l'exemple de la liste `famille = ["Bart", "Lisa", "Maggie"]`. Observez l'effet de l'instruction suivante :

{{IDE("data/tab2_append1")}}

La __méthode__ `append()` rajoute donc un élément **à la fin** de la liste.

Très souvent, la méthode `append()` est utilisée à partir d'une liste vide `[]` à laquelle on rajoute peu à peu des éléments, comme dans l'exemple suivant :

{{IDE("data/tab2_append2")}}


!!! question "Exercice"
    On veut écrire une fonction `sup20` qui prend en argument un tableau de nombres et qui renvoie un autre tableau où on a gardé uniquement les nombres supérieurs à 20. Par exemple, on veut que `sup20([30,10,20,40])` renvoie `[30,40]`.

    Proposer un code qui utilise la méthode `append`.

    {{ IDE("data/tab2_sup20")}}

    ??? danger "Solution"
        ```python
        def sup20(tab) :
            newtab = [] #initialisation d'un tableau vide
            for nb in tab :
                if nb > 20 :
                    newtab.append(nb)
            return newtab
        ```    


## ^^3 - Construction d'une liste  *par compréhension*^^

Pour construire un tableau, nous avons vu trois techniques :

- créer de toutes pièces la tableau avec tous ses éléments, comme dans le cas du tableau `famille` ci-dessus ;
- créer un tableau de taille fixée puis modifier chaque élément à l'aide d'une boucle `for`, comme dans le code ci-dessous :

``` python
mon_tableau = [0] * 10 # création d'un tableau de taille 10 rempli avec des zéros
for k in range(len(mon_tableau)) :
        mon_tableau[k] = k**2 # on remplit le tableau avec le carré des entiers successifs
```

- ou bien partir d'un tableau vide et le remplir successivement avec la méthode `append`, comme par exemple :

``` python
mon_tableau = [] # création d'un tableau vide
for k in range(10) :
        mon_tableau.append(k**2) # on remplit le tableau avec le carré des entiers successifs
```

Il existe en Python une quatrième méthode, dite _construction par compréhension_ : 

```python
carres_parfaits = [k**2 for k in range(10)]
```

!!! done "Construction par compréhension (1)"
    Tester cette création du tableau `carres_parfaits` dans la console ci-dessous, puis le visualiser :

    {{ terminal()}}


Encore plus fort : on peut utiliser les éléments d'un tableau déjà construit pour en construire un nouveau : 

{{ IDE("data/tab2_comprehension1")}}

Cette construction par compréhension peut aussi se combiner avec l'instruction conditionnelle `if` pour former une instruction assez proche du langage anglais de tous les jours :

```python
mes_impairs = [nb for nb in range(10) if nb%2 == 1]
```

!!! done "Construction par compréhension (2)"
    Tester cette création du tableau `mes_impairs` dans la console ci-dessous, puis le visualiser :

    {{ terminal()}}


> Cette technique s'appelle en informatique un "**filtrage** de données **à la volée**"

_Remarque_ : on aurait obtenu le même résultat avec l'instruction `mes_impairs_bis = [nb for nb in range(1,10,2)]` (le vérifier en console !).

!!! question "Exercice"
    
    On saisit dans un éditeur Python les deux lignes suivantes de code :

    ```python
    maListe = [-3, -6, 4, 7, -2, 1, -3, 5]
    tab = [nb**2 for nb in maListe if nb < 0 ]
    ```

    {{ multi_qcm(
        [
            "Que contient alors la variable `tab` ?",
            ["[-3,-6,-2,-3]", "[9,36,4,9]", "Rien (tableau vide)", "[4,1]"],[2],
        ],
        )}}




## ^^4 - Tableaux à deux dimensions^^

La construction par compréhension est également possible pour les "tableaux de tableaux" (tableaux à deux dimensions) :

{{ IDE("data/tab2_comprehension2")}}

On peut aussi construire ce même tableau élément par élément, en partant d'un tableau vide :

{{IDE("data/tab2_append3")}}

## ^^5 - Complément  : la copie de liste, un phénomène inquiétant ! (niveau avancé)^^

À votre avis, que vaut la variable `b` à la fin de l'exécution du code suivant :
```python
# Ici, les variables a et b sont des nombres (de type int)
a = 3
b = a
a = 5
```

??? danger "Réponse"
    La variable `b` vaut 3. Même si la variable `a` a changé de valeur par la suite, cela n'a pas changé la valeur de `b`.

Mais avec des tableaux, on n'a pas le même phénomène :

{{ IDEv("data/tab2_copytab")}}

Le tableau `tab_b` est lui **aussi** devenu égal à `[12, 5, 7]`, alors que la modification sur l'élément `tab_a[0]` n'a été faite qu'**après** l'affectation `tab_b = tab_a`.  
Les listes `tab_a` et `tab_b` sont en fait strictement et définitivement identiques, elles sont simplement deux dénominations différentes d'un même objet. 

Mais alors, comment **copier** le contenu d'une liste vers une autre ?

Parmi plusieurs solutions, celle-ci est simple et efficace : la méthode `copy()`.

{{ IDEv("data/tab2_copytab2")}}



## ^^6 - Exercices et TP^^

* Exercices généraux : télécharger le [carnet Jupyter](NSI1_tableaux_Exos_2_Avance.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* Exercices de synthèse : télécharger le [carnet Jupyter](NSI1_tableaux_Exos_2_Synthese.ipynb).


## ^^7 - QCM d'entrainement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Tableaux Partie II"**.