# dans le code précédent, effacer les parties UPDATE et DRAW
# puis copier le contenu de ce fichier
# et le coller en-dessous la fonction ennemis_deplacement(ennemis_liste)

# initialisation des explosions
explosions_liste = []

# nombre de vies
vies = 4

# gestion des collisions

def vaisseau_suppression(vies):
    """disparition du vaisseau et d'un ennemi si contact
    Rappel : les ennnemis et le vaisseau sont des carrés 8x8 """

    for ennemi in ennemis_liste:
        # il y a contact s'il y a moins de 8 pixels d'écart en x et en y
        if -8 <= vaisseau_x - ennemi[0] <= 8  and ... :
            ennemis_liste.remove(ennemi)
            vies = ... # le nombre de vies diminue d'une unité
            # on ajoute l'explosion
            explosions_creation(vaisseau_x, vaisseau_y)
    return vies


def ennemis_suppression():
    """disparition d'un ennemi et d'un tir si contact
     Rappel : les ennnemis sont des carrés 8x8 mais un tir est un rectangle 1x4"""
    for ennemi in ... :
        for tir in ... :# boucles imbriquées !
            if -1 <= tir[0] - ennemi[0] <= 8  and tir[1] - ennemi[1] <= 8 :
                ennemis_liste.remove(...)
                tirs_liste.remove(...)
                # on ajoute l'explosion
                explosions_creation(ennemi[0], ennemi[1])

def explosions_creation(x, y):
    """explosions aux points de collision entre deux objets"""
    explosions_liste.append([x, y, 0])


def explosions_animation():
    """animation des explosions"""
    for explosion in explosions_liste:
        explosion[2] +=1
        if explosion[2] == 12:
            explosions_liste.remove(explosion)                

# =========================================================
# == UPDATE
# =========================================================
def update():
    """mise à jour des variables (30 fois par seconde)"""

    global vaisseau_x, vaisseau_y, tirs_liste, ennemis_liste, vies, explosions_liste

    # mise à jour de la position du vaisseau
    vaisseau_x, vaisseau_y = vaisseau_deplacement(vaisseau_x, vaisseau_y)

    # creation des tirs en fonction de la position du vaisseau
    tirs_liste = tirs_creation(vaisseau_x, vaisseau_y, tirs_liste)

    # mise a jour des positions des tirs
    tirs_liste = tirs_deplacement(tirs_liste)

    # creation des ennemis
    ennemis_liste = ennemis_creation(ennemis_liste)

    # mise a jour des positions des ennemis
    ennemis_liste = ennemis_deplacement(ennemis_liste)

    # suppression des ennemis et tirs si contact
    ennemis_suppression()

    # suppression du vaisseau et ennemi si contact
    vies = vaisseau_suppression(vies)

    # evolution de l'animation des explosions
    explosions_animation()    

# =========================================================
# == DRAW
# =========================================================
def draw():
    """création des objets (30 fois par seconde)"""

    # vide la fenetre (clear screen)
    pyxel.cls(0)

    # si le vaisseau possede des vies le jeu continue
    if vies > 0:

        # affichage des vies            
        pyxel.text(5,5, 'VIES:'+ str(vies), 7)

        # vaisseau (carre 8x8, couleur 1 = bleue)
        pyxel.rect(vaisseau_x, vaisseau_y, 8, 8, 1)

        # tirs  (rectangle 1x4, couleur 10 = jaune)
        for tir in tirs_liste:
            pyxel.rect(tir[0], tir[1], 1, 4, 10)

        # ennemis  (carre 8x8, couleur 14 = rose)
        for ennemi in ennemis_liste:
            pyxel.rect(ennemi[0], ennemi[1], 8, 8, 14)

        # explosions (cercles de plus en plus grands)
        for explosion in explosions_liste:
            pyxel.circb(explosion[0]+4, explosion[1]+4, 2*(explosion[2]//4), 8+explosion[2]%3)            

    # sinon: GAME OVER
    else:

        pyxel.text(50,64, 'GAME OVER', 7)

pyxel.run(update, draw)