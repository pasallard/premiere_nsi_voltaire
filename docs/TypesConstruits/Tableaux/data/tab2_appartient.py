from random import randint
maListe = [2,3,5,7]
alea = randint(1,10) # nombre aléatoirement choisi entre 1 et 10
print("Nombre choisi aléatoirement : ", alea)
if alea in maListe:
    print("Le nombre ", alea, " appartient à la liste")
else:
    print("Le nombre ", alea," n'appartient pas à la liste")