import pyxel, random

# taille de la fenetre 128x128 pixels
# ne pas modifier
pyxel.init(128, 128, title="Jeu NSI")

# position initiale du vaisseau
# la position (0,0) correspond au coin en haut à gauche
vaisseau_x = 60
vaisseau_y = 60

def vaisseau_deplacement(x, y):
    """déplacement avec les touches de directions"""

    if pyxel.btn(pyxel.KEY_RIGHT):
        if (x < 120) : # si ça ne fait pas sortir du cadre
            x = x + 1 # on augmente de 1 l'abscisse x 
    if pyxel.btn(pyxel.KEY_LEFT):
        if (x > 0) : # si ça ne fait pas sortir du cadre
            x = ... # compléter ici
    if pyxel.btn(pyxel.KEY_DOWN):
        if (y < 120) : # si ça ne fait pas sortir du cadre
            y = y + 1 # aller vers le bas signifie augmenter le y de 1 
    if pyxel.btn(pyxel.KEY_UP):
        if (y > 0) : # si ça ne fait pas sortir du cadre
            y = ... # compléter ici
    return x, y


# =========================================================
# == UPDATE
# =========================================================
def update():
    """mise à jour des variables (30 fois par seconde)"""

    global vaisseau_x, vaisseau_y

    # mise à jour de la position du vaisseau
    vaisseau_x, vaisseau_y = vaisseau_deplacement(vaisseau_x, vaisseau_y)


# =========================================================
# == DRAW
# =========================================================
def draw():
    """création des objets (30 fois par seconde)"""

    # vide la fenetre (clear screen)
    pyxel.cls(0)

    # vaisseau (carre 8x8, couleur 1 = bleue)
    pyxel.rect(vaisseau_x, vaisseau_y, 8, 8, 1)

pyxel.run(update, draw)
