# dans le code précédent, effacer les parties UPDATE et DRAW
# puis copier le contenu de ce fichier
# et le coller en-dessous la fonction vaisseau_deplacement(x, y)

# initialisation des tirs
tirs_liste = [] 
# ce tableau contiendra les coordonnées des différents tirs
# on aura par exemple tirs_liste = [ [60,50], [45,72], [105,20] ] pour représenter trois tirs


def tirs_creation(x, y, tirs_liste):
    """création d'un tir avec la barre d'espace"""
    if pyxel.btnr(pyxel.KEY_SPACE):
        tir = [x+4, y-4] # coordonnées du tir par rapport à la position (x,y) du coin supérieur gauche du vaisseau
        .... # compléter ici pour ajouter ce tir à la liste des tirs
    return tirs_liste

def tirs_deplacement(tirs_liste):
    """déplacement des tirs vers le haut et suppression s'ils sortent du cadre"""
    for ... in ... : # compléter ici pour parcourir la liste des tirs
        tir[1] -= 1 # on diminue l'ordonnée y pour aller vers le haut
        if  tir[1] < -8 : # si le tir sort du cadre, on l'enlève du tableau tirs_liste
            tirs_liste.remove(tir)
    return tirs_liste

# =========================================================
# == UPDATE
# =========================================================
def update():
    """mise à jour des variables (30 fois par seconde)"""

    global vaisseau_x, vaisseau_y, tirs_liste

    # mise à jour de la position du vaisseau
    vaisseau_x, vaisseau_y = vaisseau_deplacement(vaisseau_x, vaisseau_y)

    # creation des tirs en fonction de la position du vaisseau
    tirs_liste = tirs_creation(vaisseau_x, vaisseau_y, tirs_liste)

    # mise a jour des positions des tirs
    tirs_liste = tirs_deplacement(tirs_liste)

# =========================================================
# == DRAW
# =========================================================
def draw():
    """création des objets (30 fois par seconde)"""

    # vide la fenetre (clear screen)
    pyxel.cls(0)

    # vaisseau (carre 8x8, couleur 1 = bleue)
    pyxel.rect(vaisseau_x, vaisseau_y, 8, 8, 1)

    # tirs (rectangle 1x4, couleur 10 = jaune)
    for tir in tirs_liste:
        pyxel.rect(tir[0], tir[1], 1, 4, 10)

pyxel.run(update, draw)
