# Les dictionnaires :bookmark_tabs:


## ^^1 - Introduction^^

Prenons l'exemple d'un répertoire téléphonique devant contenir des noms et des numéros de téléphone. Nous pouvons le mémoriser simplement comme un tableau (ou liste) de tableaux de la forme `[nom,numéro]`


{{ IDEv('script1') }}


Si nous voulons appeler *Kelly*, nous avons deux possibilités avec un tel tableau :

* soit il faut savoir que les informations concernant *Kelly* sont dans le cinquième élément de la liste (ce qui ne semble pas très pratique et réaliste)

{{ IDE('script2') }}



* soit nous cherchons dans le tableau en partant du premier élément de la liste jusqu'à ce que nous trouvions *Kelly* (ce qui revient à feuilleter son répertoire) : cela nécessite d'utiliser une boucle pour parcourir le tableau.

{{ IDE('script3') }}

Vous conviendrez que ce n'est pas pratique pour accéder à son numéro de téléphone. 

Il semblerait **plus pratique d'associer un nom à un numéro**, autrement dit d'associer une **clé** (_ici le nom_) à une **information** ou **valeur** (_ici le numéro de téléphone_).



***C'est ce que les dictionnaires permettent !***

Un dictionnaire est un ensemble ^^non ordonné^^ de **paires clé/valeur**, délimité par des __accolades__ :



!!! success "Exemple"
    ```python
    annuaire = { "Alice" : 5142, "Bob" : 5197, "Charlie" : 5720,
    "Antoine" : 5729, "Kelly" : 5234, "Florence" : 5345, "Guy" : 5186 }
    ```

* une **clé** peut être de type `str`(chaîne de caractères), `int`(nombre entier), `float`(nombre à virgule flottante), `tuple` ( à condition que les tuples ne contiennent que des entiers, des flottants ou des n-uplets et pas des listes). Dans l'exemple ci-dessus, les clés sont les prénoms des personnes (type `str`).
* les **valeurs** pourront être de tout type sans exclusion. Dans l'exemple ci-dessus, les valeurs sont les numéros de téléphones (type `int`).

!!! note "Rappel"

    - crochets [ ] → listes
    - parenthèses ( ) → tuples
    - accolades { } → dictionnaires



En Python, le dictionnaire est un objet **mutable**, autrement dit, on peut le modifier.

Un dictionnaire permet avec un accès très rapide à la valeur à partir de la clé, bien plus rapide que si on avait utilisé un tableau.


## ^^2 - Utilisation basique d'un dictionnaire en Python^^

Les dictionnaires Python se représentent entre __accolades__ `{}`. Les différentes paires sont séparées par des virgules et sont de la forme `clé: valeur`.


**2.1.** Une première méthode pour **créer** un dictionnaire est de donner dans des accolades la série des  paires `clé : valeur`.

<!-- !!! example "Exemple : création manuelle d'un dictionnaire" -->
{{ IDE('script4') }}


On constate qu'on a bien créé une variable `annuaire`d'un type nouveau : `dict`


**2.2.** L'**accès** à une valeur d'un dictionnaire se fait par sa clé, toujours à l'aide de __crochets__.

{{ IDEv('script9') }}


<!-- Un dictionnaire se révèle très pratique dans les interactions avec l'utilisateur, par exemple pour coder/décoder des échanges d'information. 

{{ IDE('script10') }} -->


**2.3.** L'utilisation d'une clé inconnue génère une erreur :

{{ IDEv('script11') }}


**2.4.** Le dictionnaire étant un objet *mutable*, on peut **modifier** la valeur associée à une clé ou **ajouter** une nouvelle paire clé/valeur.

{{ IDEv('script5') }}

> On note qu'il n'y a pas de méthode `append ` pour les dictionnaires.

!!! question "Exercice 1"
     * Créer un dictionnaire `zoo` dont les clés sont les noms d'animaux suivants : _chat_, _panda_, _lion_, _girafe_ et dont les valeurs sont la description en une phrase de cet animal.
     * Consulter ensuite la valeur de la clé _chat_.
     * Ajouter ensuite la clé _mammouth_ ainsi que sa valeur.

    {{ IDE() }}

**2.5.** On peut aussi créer un dictionnaire en ajoutant les éléments un à un, par exemple à partir d'un dictionnaire vide et d'une boucle ` for`.


!!! success "Exemple : création d'un dictionnaire qui associe chaque lettre majuscule à sa position dans l'alphabet" 
    {{ IDE('script6') }}

**2.6.** La **taille** d'un dictionnaire est le nombre de paires clés/valeurs. Comme pour un tableau, on peut obtenir la taille d'un dictionnaire grâce à la fonction `len`. 

Essayer avec `len(zoo)` :
{{ terminal() }}

## ^^3 -  Parcours d'un dictionnaire^^

Il est possible de parcourir un dictionnaire de trois manières :

- parcourir l'ensemble des **clés** avec la méthode `keys()` ;
- parcourir l'ensemble des **valeurs** avec la méthode `values()` ;
- parcourir l'ensemble des **paires clés/valeurs** avec la méthode `items()`.

!!! example "Parcours selon les clés" 
    {{ IDE('script14') }}


    _Remarque_ : on obtient le même résultat si on écrit simplement `for prenom in annuaire` mais la syntaxe `annuaire.keys()` permet d'éviter toute ambigüité.

!!! example "Parcours selon les valeurs" 
    {{ IDE('script15') }}

!!! example "Parcours selon les paires clés/valeurs" 
    {{ IDE('script16') }}

!!! question "Exercice 2"
    On donne le dictionnaire `semaine` ci-dessous.

    Écrire le code pour afficher toutes les clés de ce dictionnaire, puis pour afficher toutes les valeurs du dictionnaire

    {{ IDE('scriptSemaine') }}


## ^^4 - Test d'appartenance à un dictionnaire^^

On peut tester l'appartenance d'une valeur ou d'une clé grâce au mot clé `in`.

{{ IDE('script17') }}


!!! question "Application directe"
    Quelles sont les valeurs des booléens suivants :

    * `"Florence" not in annuaire.keys()`

    * `5186 in annuaire.values()`

    _Vérifiez vos réponses dans une console !_




!!! question "Exercice 3"
    On dispose d'un dictionnaire `scores` dont les clés sont des prénoms de joueurs et les valeurs sont leurs scores à un jeu vidéo. 

    Par exemple, `scores = {"Alice": 5, "Bob": 3, "Charlie": 8}`.

    Écrire une fonction `marque_un_point` qui prend en paramètres `dico` un tel dictionnaire de scores et `joueur` une chaine de caractères et qui renvoie un dictionnaire dans lequel :
    
    * le score de `joueur` augmente de 1 point si `joueur` est déjà une clé du dictionnaire `dico`
    * et si `joueur` n'est pas une clé du dictionnaire `dico`, on ajoute la clé `joueur` à `dico` en lui associant la valeur 1.

    Par exemple, avec le dictionnaires `scores` ci-dessus, on veut avoir le fonctionnement suivant :

    ```pycon
    >>> marque_un_point(scores, "Alice")
    {"Alice": 6, "Bob": 3, "Charlie": 8}
    >>> marque_un_point(scores, "Elena")
    {"Alice": 6, "Bob": 3, "Charlie": 8, "Elena": 1}
    ```

    {{ IDE() }}

    ??? danger "Solution"
        Voici un code possible :
        ```python
        def marque_un_point(dico, joueur):
            if joueur in dico.keys() :
                dico[joueur] += 1
            else :
                dico[joueur] = 1
            return dico
        ```



## ^^5 - Techniques avancées avec les dictionnaires^^


### ^^5.1 - Création d'un dictionnaire par conversion d'un tableau^^

On peut créer un dictionnaire à partir d'un tableau en utilisant la fonction `dict`, appliquée à un tableau contenant ... :

- des 2-uplets (tuple à deux éléments).
- ou des tableaux à deux éléments.

!!! example "Exemple : création d'un dictionnaire par conversion d'un tableau" 
    {{ IDE('script7') }}


### ^^5.2 - Création d'un dictionnaire par compréhension^^

La méthode de création par __compréhension__ utilisée pour créer un tableau fonctionne aussi avec les dictionnaires. Elle évite de créer d'abord un dictionnaire vide puis d'écrire une boucle.

{{ IDE('script8') }}

!!! question "Exercice"
    Créer un dictionnaire en compréhension contenant les puissances de 2 jusqu'à de $2^{16}$. Chaque clé sera un nombre $n$, de 0 à 16 inclus, et sa valeur sera de $2^{n}$
    {{ IDE() }}


### ^^5.3 - Suppression d'une paire clé/valeur^^

 Pour **supprimer** une paire clé/valeur d'un dictionnaire, on peut utiliser la fonction `del`.

{{ IDE('script13') }}


## ^^6 - Questions de synthèse^^


Dans cet exercice, on considère le ***dictionnaire*** suivant :


`voyelles = { 'a': True, 'b': False, 'c': False}`



{{ multi_qcm(
    [
        "Quelle est la valeur de `#!py voyelles[1]` ?",
        ["a","b","True","False", "Ceci génère une erreur d'exécution"],[5],
    ],
    [
        "Quelle est la valeur de `#!py voyelles['a']` ?",
        ["True","False", "Ceci génère une erreur d'exécution"],[1],
    ],
    [
        "Quelle instruction permet de modifier le dictionnaire `#!py voyelles` de façon à ce que sa nouvelle valeur soit 
`#!py { 'a': True, 'b': False, 'c': False, 'e': True}` ?",
        ["voyelles['e'] = True","voyelles.append('e')","voyelles.append('e', True)","ce n'est pas possible car un dictionnaire n'est pas modifiable"],[1],
    ],
    [
        """
        Quel affichage obtient-on lors de l'exécution du code ci-dessous ?

        ```python
        for cle in voyelles.keys():
           print(cle, end=' ') # l'option end=' ' supprime les sauts de ligne entre chaque print
        ```
        """,
        ["a b c","(a, True) (b, False) (c, False)", "True False False"],[1],
    ],
    [
        """
        Quel affichage obtient-on lors de l'exécution du code ci-dessous ?

        ```python
        for truc in voyelles.items():
            print(truc, end=' ')
        ```
        """,
        ["a b c","(a, True) (b, False) (c, False)", "True False False"],[2],
    ],
    shuffle = False
    )}}



## ^^7 - QCM^^

Faire le  [QCM en  ligne](https://pasallard.gitlab.io/qcm_nsi_premiere/QCM_Dictionnaires_Short.html){target="_blank"}.

