def comptage_lettres(mot):
    dico = ... # initialisation d'un dictionnaire vide
    for lettre in ... : # on parcourt toutes les lettres de mot
        if ... : # si lettre est déjà une clé de dico
            ... # on augmente la valeur correspondante de 1
        else :
            ... # sinon on ajoute une paire clé/valeur à dico avec lettre comme clé et 1 comme valeur
    return ...

# un test
assert comptage_lettres("abracadabra") == {'a': 5, 'b': 2, 'r': 2, 'c': 1, 'd': 1}, "problème 1"