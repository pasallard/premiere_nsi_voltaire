# Le codage des entiers naturels :family_mwgb:

## ^^1. Introduction^^

Un nombre entier naturel sert à représenter une quantité d'objets : des moutons, des sangliers, des arbres, des étoiles, etc.

À l'origine de l'humanité, une quantité était ***représentée*** par un nombre équivalent de cailloux (_calculus_ en latin), de bâtons sur une tablette d'argile ou de marques sur un tronc d'arbre.

Toutes les grandes civilisations ont élaboré des méthodes de ***représentation*** des nombres entiers, comme par exemple la numération romaine.
![](data/Entiers_Romains.jpg){: .center} 

Puisque les humains utilisaient leurs dix doigts (_digitus_ en latin) pour compter, ils sont progressivement venus à utiliser la numération en base __10__ ou  __système décimal__ que nous utilisons maintenant.

Mais ce système décimal n'est pas bien adapté pour le monde du numérique, où l'on préfère utiliser le __système binaire__ (base 2) et le système __héxadécimal__ (base 16).


## ^^2. Partie théorique : système décimal, binaire et héxadécimal^^

Télécharger le [support de cours en version pdf](NSI1_Entiers_Cours.pdf).

## ^^3. QCM d'entrainement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Les entiers naturels (systèmes décimal, binaire, héxadécimal)"**.

## ^^4. Exercices en Python^^

* Exercices de conversion binaire/décimal : télécharger le [carnet Jupyter](ExosPythonBinaireDecimal.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* TP sur les fonctions internes de Python : télécharger le [carnet Jupyter](BinaireHexaAvecPython.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.


