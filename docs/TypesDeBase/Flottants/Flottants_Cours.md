# Le codage des nombres réels : les nombres flottants :sailboat:

Nous avons déjà étudié la représentation des entiers relatifs en binaire. La connaissance de cette représentation est essentielle pour pouvoir comprendre la représentation des nombres réels que nous appelons **flottants** en informatique, et qui correspondent aux nombres de type `float` en Python.

En introduction, quelles sont les valeurs des booléens `b1` et `b2` suivants : `True` ou bien `False` ? _Vous avez la console pour confirmer ou contredire vos réponses..._

* `b1 = (0.25 + 0.5 == 0.75)`
* `b2 = (0.1 + 0.2 == 0.3)`

{{ terminal()}}

??? note ":astonished: Surpris ou pas ?"
    Le premier cas ne nous surprend pas : Python évalue à `True` l'égalité de `0.25 +0.5` et de `0.75`.

    Par contre, le second cas est surprenant : l'égalité de `0.1+0.2` et de `0.3` est évaluée à `False` par Python.

    Nous expliquerons plus bas ce phénomène.


## ^^1. Écriture d'un nombre réel^^

En notation décimale, les chiffres à droite de la virgule représentent des dixièmes, centièmes, millièmes....

Ainsi $43,21_{(10)} = 4 \times 10^1+3 \times 10^{0}+2 \times 10^{-1}+1 \times 10^{-2}$

!!! tip "Notation binaire à virgule"
    Par analogie, on peut utiliser la notation binaire avec une virgule, en convenant que les chiffres après la virgule représentent les demi ($\frac{1}{2} = 2^{-1}$), quart ($\frac{1}{4} = 2^{-2}$), huitième ($\frac{1}{8} = 2^{-3}$), etc.

    Ainsi l'écriture $1,1011_{(2)}$ signifie $1 \times 2^0+1 \times 2^{-1}+0 \times 2^{-2}+1 \times 2^{-3}+ 1 \times 2^{-4} = 1 + 0,5 + 0,125 + 0,0625$ et cela représente donc le nombre qui a pour écriture décimale $1,6875$.

### 1.1. Conversion de binaire à décimal

La conversion de binaire à décimal pour les entiers est aisée, et la conversion de binaire à décimal pour les "nombres à virgules" n'est pas plus compliquée :relaxed: 

En effet, cela revient à faire des additions de puissances de 2, sachant que les exposants peuvent être positifs ou négatifs.

!!! question "Exercice"
    Déterminer l'écriture décimale du nombre qui s'écrit  $0,11101_{(2)}$ en binaire.

    ??? danger "Solution"
        On écrit $0,11101_{(2)} = 2^{-1}+2^{-2}+2^{-3}+2^{-5}= \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \frac{1}{32}=0.5+0.25+0.125+0.03125 = 0.90625_{(10)}$.



### 1.2. Conversion de décimal à binaire

Il s'agit maintenant d'obtenir une écriture binaire à partir de écriture décimale d'un "nombre à virgule".


Prenons l'exemple de 60,6875~(10)~ : 
 
* on commence par coder 60 : 60 = 32 + 16 + 8 + 4 donc 60 s'écrit  11 1100~(2)~ en base 2.

* puis on s'occupe de la partie décimale après la virgule : 0,6875, et on lui applique l'algorithme suivant :

    - on effectue des multiplications succesives par 2, sans reporter la partie entière ;
    - et on s'arrête quand il n'y a plus rien à reporter (_quand on arrive sur 1,0_) ;
    - on lit alors les parties entières de **haut en bas**, ce qui donne 0,6875~(10)~ = 0,1011~(2)~

<figure markdown>
|  produit par 2| | partie décimale à la ligne suivante|sens de lecture|
|--------|-|---------|:-:|
|0,6875x2|=|**1**,375|⬇|
|0,375x2 |=|**0**,75 |⬇|
|0,75x2  |=|**1**,5  |⬇|
|0,5x2   |=|**1**,0  |⬇|
</figure>
    
* il ne reste plus qu'à recoller les morceaux pour obtenir : 60,6875~(10)~ = 111100,1011~(2)~.

!!! question "Exercice"
    Déterminer l'écriture binaire du nombre 4,375~(10)~.

    ??? danger "Solution"
        Le codage binaire de 4 est 100~(2)~.

        Puis on applique l'algorithme à la partie décimale 0,375 :

        |  produit par 2| | partie décimale à la ligne suivante|sens de lecture|
        |--------|-|---------|:-:|
        |0,375x2|=|**0**,75|⬇|
        |0,75x2  |=|**1**,5  |⬇|
        |0,5x2   |=|**1**,0  |⬇|

        Donc 0,375~(10)~ = 0,011~(2)~.

        Conclusion : 4,375~(10)~ = 100,011~(2)~.

!!! warning "Cas où l'algorithme ne s'arrête pas"
    De même qu'il y a des nombres "simples" qui ont une écriture décimale infinie (pensez par exemple à $\frac{1}{3}$), il y a des nombres "simples" qui ont une écriture binaire infinie !

    Prenons le cas de 0,1~(10)~ et appliquons-lui l'algorithme :

    
    |  produit par 2| | partie décimale à la ligne suivante|sens de lecture|
    |--------|-|---------|:-:|
    |0,1x2|=|**0**.2 |⬇|
    |0,2x2|=|**0**.4 |⬇|
    |0,4x2|=|**0**.8 |⬇|
    |0,8x2|=|**1**.6 |⬇|
    |0,6x2|=|**1**.2 |⬇|
    |0,2x2 (déjà vu...)|=|**0**,4 |⬇|
    |0,4x2 (déjà vu...)|=|**0**,8 |⬇|
    |0,8x2 (déjà vu...)|=|**1**,6 |⬇|
    |0,6x2 (déjà vu...)|=|**1**,2 |⬇|
    |etc  | |etc...  |⬇|

    Ainsi le nombre $0,1_{(10)}$ a une écriture **infinie** en base 2 : 0,1~(10)~ = 0,0 0011 0011 0011 ...~(2)~

!!! info "Début d'explication du pseudo-bug 0,1+0,2 qui ne égal pas 0,3"
    Un ordinateur n'a pas une mémoire infinie et il ne peut donc pas stocker en mémoire une écriture binaire infinie. Il est donc obligé de faire un arrondi, et tous les arrondis sont source d'erreurs :scream: !


## ^^2. Représentation des flottants en machine^^

Comme évoqué ci-dessus, la mémoire d'un ordinateur n'est pas infinie donc la première chose à faire est de **fixer un nombre de bits** pour coder un nombre réel : le plus généralement, les systèmes informatiques utilisent 32 ou 64 bits.

Une approche naïve pour représenter les nombres réels en machine serait alors :

- d'attribuer une partie des bits pour la partie entière
- et le reste des bits pour la partie "décimale"

Cette approche présente l'inconvénient de devoir aussi réserver une partie des bits pour coder la position de la virgule dans le nombre, ce qui fait perdre de la place mémoire. Pour éviter cet inconvénient, on fait appel à la notation scientifique, __mais en version binaire__ !

!!! note "Écriture scientifique"
    L'écriture scientifique décimale de 

    - 123456,7 est 1,234567 × 10^5^
    - 0,000 000 32 est 3,2 × 10^-7^

    Par analogie, l'écriture scientifique binaire de 

    - 111100,101~(2)~ est 1,1110 0101~(2)~ × 2^5^ (avec un exposant 5 car on a décalé la virgule de 5 rangs vers la gauche)
    - 0,00011~(2)~ est 1,1~(2)~ × 2^-4^ (avec un exposant -4 car on a décalé la virgule de 4 rangs vers la droite)

    _Remarque_ : dans un passage à une écriture scientifique, on fait "flotter la virgule" d'où le nom de ***flottant***.

!!! info "Vocabulaire"
    On appelle **mantisse** la "première partie" de l'écriture scientifique d'un nombre.

    Par exemple, la mantisse de 1,1~(2)~ × 2^-4^  est 1,1~(2)~.

    On remarque que, dans l'écriture scientifique binaire d'un nombre, sa mantisse commence toujours par 1 !

    L'**exposant** est le nombre qui apparait dans la puissance de 2 (cas binaire) ou 10 (cas décimal) : par exemple, l'exposant de 1,1~(2)~ × 2^-4^  est −4.

!!! tip "Principe du codage en mémoire d'un nombre réel sur 32 bits"

    Une fois que l'on connait l'écriture binaire scientifique d'un nombre, on utilise les 32 bits à notre disposition de la façon suivante :
    
    * le premier bit (_bit de poids fort_) sert à coder le signe du nombre : 0 pour +, 1 pour −
    * sur les 8 bits suivants, on code en binaire l'exposant (ce qui donne la possibilité de coder les exposants allant de -127 à +127)
    * sur les 23 derniers bits, on code la mantisse

    ![simple précision](data/IEEE754_simple_precision.png){: .center}


> Remarque : il y a en réalité deux subtilités supplémentaires. 
D'une part, il ne sert à rien de coder le 1 qui est en tête de la mantisse (car c'est toujours 1) donc il n'est pas codé. Et d'autre part, l'exposant étant potentiellement négatif et plutôt que de le coder comme un entier signé par _complément à deux_, il a été choisi de lui ajouter 127 (en se disant qu'on n'aura jamais des exposants plus petits que -127).


**Traitons un exemple** :  l'écriture en mémoire sur 32 bits du nombre 60,6875~(10)~

* on a déterminé plus haut son écriture binaire : c'est 111100,1011~(2)~

* son écriture scientifique binaire est alors 1,11100 1011~(2)~ × 2^5^ 

* il s'agit d'un nombre positif donc  le **<span style="background:yellow">bit de poids</span>** fort est  **0**
- l'**<span style="background:green">exposant</span>** vaut 5 : on lui ajoute 127, ce qui donne 132 qui se code en binaire 1000 0100~(2)~
- la **<span style="background:blue;color:white">mantisse</span>** sans le 1 initial est 111001011 

L'écriture sur 32 bits de ce nombre est alors :
<table>
    <tr>
        <th style="background:yellow">S</th>
        <th colspan=8 style="background:green">exposant</th>
        <th colspan=23 style="background:blue;color:white">mantisse</th>
    </tr>
    <tr>
        <td style="background:yellow">0</td>
        <td style="background:green">1</td>
        <td style="background:green">0</td>
        <td style="background:green">0</td>
        <td style="background:green">0</td>
        <td style="background:green">0</td>
        <td style="background:green">1</td>
        <td style="background:green">0</td>
        <td style="background:green">0</td>
        <td style="background:blue;color:white">1</td>
        <td style="background:blue;color:white">1</td>
        <td style="background:blue;color:white">1</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">1</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">1</td>
        <td style="background:blue;color:white">1</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
        <td style="background:blue;color:white">0</td>
    </tr>
</table>


!!! info "Conversion en ligne"
    Sur ce [site](https://www.binaryconvert.com/convert_float.html){target="_blank"}, il suffit de saisir un nombre flottant pour obtenir son codage binaire.

!!! tip "Que retenir ?"
    Une bonne nouvelle : vous ne serez pas interrogé sur la dernière partie (partie 2) du cours :sweat_smile:.
    
    Mais vous devez :
    
    * maitriser la partie 1 ;
    * et savoir que manier des nombres flottants en Python (et dans n'importe quel outil informatique) est délicat à cause des erreurs d'arrondis qui surviennent dès qu'un nombre a une écriture binaire infinie, comme c'est le cas pour 0,1~(10)~ par exemple.


## ^^3. QCM d'entrainement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Les nombres réels"**.

## ^^4. Exercices et TP^^

Télécharger le [carnet Jupyter](NSI1_Flottants_TP.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.


