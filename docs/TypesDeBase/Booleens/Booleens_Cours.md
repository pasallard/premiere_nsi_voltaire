# Les valeurs booléennes :yin_yang:
## ^^1. Découverte des valeurs booléennes^^

Tout le monde sera d'accord pour dire que la phrase « 3 est plus grand que 8 » est fausse.

Et qu'en pense Python ? Saisissez pour voir dans la console `3 > 8` :

{{terminal()}}

Le langage Python attribue donc une **valeur** à l'expression `3>8`, à savoir la valeur `False`.

Inversement, il va attribuer la valeur `True` à l'expression `3>1` (vérifiez-le en console !). 

!!! tip "Définition d'une valeur booléenne, définition d'une variable booléenne :heart:"
    Il n'y a que deux **valeurs booléennes** : `True` et `False` (en Python).

    Selon le contexte ou le langage utilisé, les deux valeurs booléennes pourront être notées :

    * Vrai et Faux ;
    * 0 et 1, avec la convention fréquente que 0 = Faux et 1 = Vrai.

    Quelque chose qui ne peut valoir que `True` ou `False` sera alors appelé une **variable booléenne**. 

En Python, on crée une variable booléenne de la même manière que l'on crée n'importe que variable : en lui donnant un nom et en lui affectant une valeur. 

!!! done ""
    Par exemple, après l'affectation `stupide = 3>8`, la variable `stupide` vaut `False` : vérifiez-le en console !

!!! tip "Variables booléennes et instruction conditionnelle `SI... ALORS`"
    Quand vous écrivez en Python une instruction conditionnelle `if ...`, vous utilisez sans le savoir des variables booléennes.

    Dans le code suivant 
    ```python
    monAge = 17 # modifiez avec la valeur de votre choix
    if monAge >= 18 :
        print("Vous êtes majeur")
    else :
        print("Vous êtes mineur")
    ```

    l'expression `monAge >= 18` est une variable booléenne (car elle est soit vraie, soit fausse). 
    
    On peut explicitement créer et utiliser une variable booléenne `estMajeur` comme dans le code suivant :

    {{ IDE("data/monAge")}}


## ^^2. Exercices (partie 1)^^

Télécharger le [carnet Jupyter](NSI1_Booleen_Exos_Partie1.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

## ^^3. La théorie sur les expressions booléennes^^

Télécharger le [support de cours en version pdf](NSI1_Booleen_Theorie.pdf).

## ^^4. Exercices (partie 2)^^

Télécharger le [carnet Jupyter](NSI1_Booleen_Exos_Partie2.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

## ^^5. QCM d'entrainement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Les booléens"**.