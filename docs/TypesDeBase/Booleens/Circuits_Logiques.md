# TP : Circuits logiques

## ^^1. Les booléens, les transistors et les portes logiques^^

Un transistor est comme une brique élémentaire d'un circuit électronique. De façon simplifiée, il fonctionne comme un interrupteur :

- il a deux pattes d'entrée, appelées Base (B) et Collecteur (C) et une patte de sortie appelée Émetteur (E)
![](data/transistor.webp){ width="20%" .center}

- la patte  d'entrée B est comme la commande de l'interrupteur :

    - si la patte B n'est pas alimentée (courant = 0), alors la sortie E est toujours à 0 
    - si la patte B est alimentée (courant = 1), alors elle laisse passer ce qui vient de C vers la sortie E

On se rend compte que ce fonctionnement correspond à l'opérateur booléen `ET` : en effet, le booléen `B et C` a la même valeur que `E`.  On parle alors de _porte logique ET_.

<iframe style="width: 100%; height: 220px; border: 0" src="https://logic.modulo-info.ch/?mode=tryout&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgjPgAywDsRAuvAgCZQngCGANvQL6ya4HGz4CslanUgAmeGBbsqILAFcALmi55IqAMzqShIYhH42MgOaMFAU1XoQCgJ4YzUEAEEAcgBEQ8bJdG8dM5DR1WGgZeSVIfkN4ZkYAIzNmSxUg8j5iGXMADwiQAFEQDhS1ADYSckyzHMcAIULOEG80Mr5BSurIEABhQpkAdwQAJws0VBJ1KlRxUNhUfj4KCjYgA"></iframe>

En associant plusieurs transistors par des montage en série ou en parallèle, on fabrique d'autres portes logiques : _OU_ (:flag_gb:  OR) et _NON_ (:flag_gb: NOT) par exemple. 

!!! done "" 
    Connectez les deux entrées A et B à la porte _OR_ puis reliez la sortie de la porte à S.

<iframe style="width: 100%; height: 220px; border: 0" src="https://logic.modulo-info.ch/?showonly=or,not,and&mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgjPgAywDsRAuvAgCZQngCGANvQL6ya4HGz4CslanUgAmeGBbsqILAFcALmi55IqAMzqShIYhH42M5owBGAU2ar0IbFfXk+xGQrMAPJZBABlEBxVoANhJyZzcPEABBX04bbjUgvkFQ9ygQACFfCjYgA"></iframe>


## ^^2. Circuit logique 1^^

!!! done "Une loi de Morgan"
    * Assembler les différents composants pour former le circuit logique `NOT(A AND B)` et connecter la sortie de ce circuit sur S0.
    * En gardant les mêmes entrées A et B, former le circuit logique `NOT(A) OR NOT(B)` et connecter la sortie de ce second circuit sur S1.
    * Comparer les valeurs de S0 et S1 en testant toutes les configurations possibles pour les valeurs de A et de B.

<iframe style="width: 100%; height: 320px; border: 0" src="https://logic.modulo-info.ch/?showonly=or,not,and&?mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgnAAywDshAuvAgCZTHgCGANnQL6ya4HEBMArBSq1IAZnhhmbSiCwBXAC5pOeSKj5lSgxMICM7ZWnW8B0mlB6tpTBgCMApkxXoQ2JwDZiZafLsAPRZAgAIIg+i5cqh6w-Fo+-lAgAEKhHOHuOp6xfgEgAMqEKQaRGdEm8HE5uTqh5KxAA"></iframe>


## ^^3. Circuit logique 2^^

À l'aide de tables de vérité, nous avons prouvé "sur papier" que l'opérateur "OU eXclusif" `XOR` peut être obtenu par une combinaison de `AND`, `OR` et `NOT` :

``` a XOR b =  (a AND NOT(b) ) OR ( NOT(a) AND b )```

!!! done "Opérateur XOR"
    
    * Assembler les différents composants pour former le circuit logique `(A AND NOT(B) ) OR ( NOT(A) AND B )` et connecter la sortie de ce circuit sur S0.
    * En gardant les mêmes entrées A et B et avec le composant "OU eXclusif", former le circuit logique `A XOR B` et connecter la sortie de ce second circuit sur S1.
    * Comparer les valeurs de S0 et S1 en testant toutes les configurations possibles pour les valeurs de A et de B.

<iframe style="width: 100%; height: 320px; border: 0" src="https://logic.modulo-info.ch/?showonly=or,not,and,xor&?mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgnAAywDshAuvAgCZTHgCGANnQL6ya4HEBMArBSq1IAZnhhmbSiCwBXAC5pOeSKj5lSgxMICM7ZWnW8B0mlB6tpTBgCMApkxXoQ2JwDZiZafLsAPRZAgAIIg+i5cqh6w-Fo+-lAgAEKhHOHuOp6xfgEgAMqEKQaRGdEm8HE5uTqh5KxAA"></iframe>

## ^^Pour aller plus loin^^

Sur le simulateur [Logic](https://logic.modulo-info.ch/) développé par Jean-Philippe Pellet, vous pouvez construire toutes sortes de circuits logiques. 