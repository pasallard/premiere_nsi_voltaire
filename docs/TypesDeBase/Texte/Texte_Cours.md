# Codage des caractères d'un texte :book:

## ^^1. Pourquoi l'encodage des textes ?^^

Lors des premières transmissions d'information (télégraphe optique de Chappe en 1794, télégraphe électrique de Cooke et Wheatstone en 1938, ...), il a été nécessaire de définir un code pour représenter les différents symboles utilisés dans le langage courant (lettres, chiffres, ponctuations, …). Ce code peut être visuel (drapeaux, code Chappe), ou bien codé à l'aide de plusieurs impulsions (code Morse), etc.

| code Chappe | télégraphe de Chappe | 
| ----------- | ----------- |
|![](codeChappe.png) | ![](tourChappe.jpg) | 

C'est ce que l'on appelle **l'encodage**.

Un ordinateur utilisant un système binaire pour stocker, transmettre et utiliser les données, il a fallu choisir un moyen de **représenter les différentes lettres et symboles à l'aide de 0 et de 1**.

## ^^2. Quelle représentation choisir ?^^

Au début de l'informatique, chaque machine/constructeur possédait son propre système d'encodage.

Cette prolifération d'encodages possède plusieurs inconvénients :

* transfert de données difficile entre utilisateurs ;
* changement d'appareil compliqué ;
* obligation pour un programmeur d'apprendre plusieurs systèmes différents d'encodage.


Le besoin pour un encodage **standardisé** s'est rapidement fait ressentir.

### ^^2.1. Le code ASCII^^

En 1963,  une première norme apparaît aux États-Unis :  l'A.S.C.I.I. (_American Standard Code for Information Interchange_). Elle définit un standard pour ___coder les caractères en binaire___.


!!! done ""
    Exemple : le caractère `A` a le code 65 (noté `100 0001` en binaire et `41` en hexadécimal).


* Nombres de bits utilisés :  7 bits, soit 2^7^ = 128 symboles différents.

* Symboles codables : les chiffres de 0 à 9, l'alphabet latin (majuscules et minuscules mais sans les accents ni les caractères spéciaux comme "œ" ou "ç"), la ponctuation et quelques caractères non imprimables (par exemple retour à la ligne). Voir [ici](https://ascii.cl/){target="_blank"} la liste complète des 128 caractères codés. 

* Avantages : prend peu de place en mémoire machine (7 bits par caractère).

* Inconvénients :

    - n'est pas adapté aux langues autres que l'anglais.
    - seulement 128 symboles codés.


### ^^2.2. La norme ISO 8859-1^^


La norme ISO 8859-1 (aussi appelée `Latin-1`) est une norme internationale qui apparaît en 1986, qui étend le code ASCII. 

!!! done ""
    Exemple : le caractère `è` a le code 232 (noté `1110 1000` en binaire et `E8` en hexadécimal).

* Nombres de bits utilisés :  8 bits, soit 2^8^ = 256 symboles différents.

* Symboles codables : tous ceux de la norme ASCII (en ajoutant un 0 devant les 7 bits du codage en ASCII) + beaucoup de caractères latins manquant en ASCII (par exemple les lettres accentuées, le ç, le ñ espagnol, …). Voir [ici](https://fr.wikipedia.org/wiki/ISO/CEI_8859-1){target="_blank"} la liste complète des 256 caractères codés. 

* Avantages :
    
    - bien adapté aux pays occidentaux,
    - encodage compatible avec la norme ASCII.

* Inconvénients :

    - certains caractères (comme "œ" ou le symbole "€") sont  manquants ;
    - besoin d'autres normes pour les langues qui n'utilisent pas l'alphabet latin (grec, russe, arabe, chinois, etc),
    - pas forcément adapté aux moyens de communications modernes (pas de smileys :cry:, …).

### ^^2.3. Unicode^^

L'encodage **UTF-8** (_Unicode Transformation Format_) apparaît en 1993. C'est l'encodage principal du standard Unicode qui apparaît à la fin des années 1990 avec comme objectif fournir un codage pour n'importe quel caractère de n'importe quel système d'écriture .

!!! done ""
    Exemple :  :heart_eyes: a le code 128 525 (`1F60D` en hexadécimal, `1 1111 0110 0000 1101` en binaire) .

* Nombres de bits utilisés : entre 8 et 32 bits (1 à 4 octets) mais "uniquement" 2^21^ = 2 097 152 symboles différents.

* Symboles codables : à peu près tous ceux dont on a eu besoin un jour : voir [ici](https://unicode-table.com/fr/){target="_blank"}. Même les emojis ont leur code Unicode ! 

* Avantages :

    - universel : contient les symboles nécessaires pour la plupart des langues existantes ;
    - compatible avec la norme ASCII ;
    - compatible avec les symboles autres que les lettres : smileys / emojis, notes de musique,etc.


* Inconvénients : prend plus de place en mémoire que le code ASCII (logique pour coder davantage de choses) et est plus difficile à encoder/décoder (nombre de bits variable).


!!! tip ""
    L'UTF-8 est maintenant l'encodage le plus répandu sur internet  : il est utilisé par plus de  95 % des sites internets  ( [source](https://w3techs.com/technologies/cross/character_encoding/ranking){target="_blank"} ).


## ^^3. QCM d'entrainement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Les textes"**.

## ^^4. Exercices en Python^^

Télécharger le [carnet Jupyter](NSI1_EncodageTexte_TP_Python.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.