# Le codage des entiers relatifs :night_with_stars:

Les entiers relatifs (:flag_gb: :flag_us: _integers_) correspondent, en Python, aux nombres de type `int`.

!!! info "Entier relatif, entier signé"
    Un entier relatif est un nombre entier positif ou négatif? Par exemple, 13 et −15 sont des entiers relatifs.

    En informatique, on préfère parler d'**entiers signés**, pour insister sur le fait qu'il faut aussi coder le signe du nombre.

On sait comment coder en binaire un nombre entier naturel (entier positif) : il reste donc à trouver une méthode pour coder aussi les nombres négatifs.

> _Remarque_ : par souci de simplicité dans ce cours, on se limite au codage des entiers sur 4 bits. Dans les exercices pratiques, vous aurez à utiliser un codage sur un octet, c'est-à-dire sur 8 bits : il suffira de généraliser ce qui est dit ici dans le cours. 

## ^^1. Représentation en binaire d'un entier signé : méthode naïve^^

La première idée qui vient est de rajouter à gauche un bit pour le signe, avec la convention :

* 0 pour le signe +
* 1 pour le signe −

On aurait alors par exemple (en notant en gras le bit de poids fort qui code le signe):

<figure markdown>
| entier relatif en écriture décimale |codage avec la méthode naïve (sur 4 bits) |
| :---: | :---: |
| +2 | **0**010 |
| −2 | **1**010 |
</figure>

Le gros inconvénient de cette méthode naïve est l'algorithme usuel de l'addition en binaire ne fonctionne plus :sob:. Par exemple, alors qu'on devrait avoir 2 + (-2) = 0, l'addition binaire 0010~2~ + 1010~2~ donne 1100~2~ ce qui ne vaut pas zéro !

Il faudrait alors revoir l'algorithme de l'addition binaire (faire une distinction selon que le bit de poids fort est à 0 ou à 1), mais de nos jours les systèmes informatiques préfèrent ne pas utiliser cette méthode naïve.

## ^^2.  Représentation en binaire d'un entier signé : méthode du **complément à un**^^

### 2.1. Un détour par l'univers graphique

Faisons d'abord un petit détour par l'univers graphique, avec une situation simple : celle d'une image en noir et blanc avec 16 niveaux de gris correspondant au _codage d'un pixel sur 4 bits_. Chaque pixel est codé par un nombre binaire avec la convention :  (0000)~2~ pour le noir (c'est-à-dire 0) , (1111)~2~ pour le blanc (c'est-à-dire 15), (0011)~2~ pour le gris foncé (c'est-à-dire 3), etc.

![](niveauxGris.png){: .center}

Le négatif d'une image en noir et blanc, c'est l'image obtenue en remplaçant le noir par le blanc, le gris foncé par le gris clair, etc.

![](negatif.png){: .center}

Cela revient à dire que :
<figure markdown>
| le nombre binaire | a pour négatif |
| :---: | :---: |
| 0000 (noir) | 1111 (blanc) |
| 0011 (gris foncé) | 1100 (gris clair) |
| 1100 (gris clair) | 0011 (gris foncé) |
| 1111 (blanc) | 0000 (noir) |
</figure>
On se rend compte que l'opération "coder le négatif du pixel" est très simple :sunglasses: ! 

Avez-vous trouvé la méthode ?

??? danger "Réponse"
    Il suffit de remplacer les 0 par des 1 et les 1 par des 0.

### 2.2. Application au codage des entiers signés

La méthode du **complément à 1** consiste à :

* garder le principe de rajouter un bit à gauche pour coder le signe d'un entier ;
* et appliquer le principe des pixels pour obtenir le négatif : remplacer les 0 par des 1 et les 1 par des 0. On peut aussi le voir en disant qu'on remplace chaque bit par le bit qu'il lui manque pour atteindre 1 (d'où le nombre de _complément à un_).

<figure markdown>
| entier relatif en écriture décimale |codage en complément à un (sur 4 bits) |
| :---: | :---: |
| +2 | **0**010 |
| −2 | **1**101 |
| +5 | **0**101 | 
| −5 | **1**010 |
</figure>

L'inconvénient de cette méthode du "complément à un" est que le nombre zéro (qui est égal à son opposé : 0 = −0) a alors deux codages : 0000~2~ et 1111~2~ (sur 4 bits).

Et il faut se souvenir de cette "bizarrerie" quand on applique l'algorithme de l'addition binaire. En effet, l'addition d'un nombre N et de son opposé −N codés en "complément à un" sur 4 bits donne toujours 1111~2~ : par exemple pour 2 et −2, 0010~2~ + 1101~2~ = 1111~2~.  Ça fait bien zéro, mais il faut se souvenir que 1111~2~ est le deuxième codage de zéro :grimacing:.

## ^^3.  Représentation en binaire d'un entier signé : méthode du **complément à deux**^^

À cause de la "bizarrerie" ci-dessus, la quasi-totalité des systèmes informatiques utilise une variante du "complément à un", qui s'appelle le **complément à deux**. 

* on commence comme dans le complément à un,
* et on rajoute 1 (_addition binaire de 1_).

<figure markdown>
| entier relatif en écriture décimale |codage en complément à deux (sur 4 bits) | _explication_ |
| :---: | :---: | :--- |
| +2 | 0010 | |
| −2 | 1110 | car 1101~2~ + 1 = 1110~2~ |
| +5 | 0101 | |
| −5 | 1011 | car 1010~2~ + 1 = 1011~2~ |
</figure>
On remarque qu'avec la méthode du complément à deux :

* le bit de poids fort (_le plus à gauche_) donne le signe du nombre : 0 pour +, 1 pour − 
* le bit de poids faible (_le plus à droite_) donne encore la parité du nombre : 0 pour les entiers pairs, 1 pour les entiers impairs 
* on peut montrer que l'algorithme de l'addition binaire fonctionne bien, _en convenant de rester sur le nombre de bits fixés au départ_ (c'est-à-dire en ne tenant pas compte d'un éventuel 1 qui apparaitrait en plus à gauche du fait des retenues). Par exemple, la somme 5 + (−2) donne avec le codage ci-dessus : 0101~2~ + 1110~2~ = ~~1~~ 0011~2~ (le 1 de gauche est barré car il sort des 4 bits fixés au départ), ce qui est bien le codage de +3 :+1:.

## ^^4. Exercices d'application^^

:warning: Dans tous les exercices, on travaille avec un codage sur 8 bits.

**Exercice 1** 

!!! question "Donner le codage d'un entier signé"
    
    {{ multi_qcm(
        [
            "Dans la méthode du complément à deux, quelle est la représentation binaire sur 8 bits du nombre −26 ?",
            ["-001 1010", "1110 0100", "1110 0101", "1110 0110"],[4],
        ],
        )}}

    ??? danger "Explication"
        On commence par écrire 26 en binaire : 26 = 16+8+2 donc 26 s'écrit (sur 8 bits) 0001 1010.

        Puis on complémente à 1 : cela devient 1110 0101.

        Puis on ajoute 1 (_addition usuelle en binaire_) : cela donne 1110 0110.

**Exercice 2**

Retrouver l'écriture décimale d'un nombre à partir de son écriture binaire en complément à deux nécessite de commencer par regarder le bit de poids fort :

* si le bit vaut 0, c'est facile : le nombre est positif et on décode l'écriture binaire comme d'habitude ;
* si le bit vaut 1 : on a un nombre négatif et le décodage se fait en "parcourant en sens inverse" les étapes du codage : d'abord **soustraire** 1 puis prendre le complément à un. 

!!! question "Décodage d'une représentation binaire en complément à deux"
    

    {{ multi_qcm(
        [
            "Quel est l'entier signé dont la représentation binaire sur 8 bits en complément à deux est : 1100 0110 ?",
            ["−58", "198", "−56", "−70"],[1],
        ],
        ) }}

    ??? danger "Explication"
        Le bit de poids fort vaut 1 : ce sera donc un nombre négatif.

        On soustraie alors 1 : 1100 0110~2~ - 1 = 1100 0101~2~.

        Puis on remplace les 0 par des 1 et les 1 par des 0 : cela donne 0011 1010~2~.

        On convertit en écriture décimale : cela donne 2+8+16+32 = 58.

        Et on termine en rajoutant le signe, pour conclure à −58.

## ^^5. QCM d'entrainement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Les entiers relatifs"**.



