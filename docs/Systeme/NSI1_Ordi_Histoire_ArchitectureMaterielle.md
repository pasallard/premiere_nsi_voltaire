# Quelques éléments d'histoire et d'architecture des ordinateurs 

## ^^1 - Une brève histoire des ordinateurs^^

### ^^1.1 - Quelques ancêtres des machines automatiques^^

La machine d'Anticythère, construite par les Grecs de l'Antiquité, est le premier calculateur mécanique de l'histoire.

![](img/anticythere2.png){: .center}

Les  horloges, les orgues de barbarie et le métier à tisser automatique de Jacquard (1801) ont pu inspirer les premiers inventeurs des machines à calculer.

| Horloge | Orgue de barbarie | Métier à tisser Jacquard |
| ----------- | ----------- |----------- |
|![](img/horloge2.png) | ![](img/barbarie2.png) | ![](img/jacquard2.png) |



### ^^1.2 - Machines à calculer mécaniques^^

Les premières machines à calculer mécaniques apparaissent au __XVII<sup>ème</sup> siècle__ avec :

- la [Pascaline](https://youtu.be/hSl2WFfCTD8){target="_blank"} de Blaise  __Pascal__, capable d'effectuer addition et soustraction.

![](img/Pascal.png){: .center}


-  la [première machine capable d'effectuer les quatre opérations](https://youtu.be/_CpQGv3jdL8){target="_blank"}, réalisée par __Leibniz__.

![](img/Leibniz.png){: .center}

Au __XIX<sup>ème</sup> siècle__, 

- Charles __Babbage__ conçoit les plans d'une __machine analytique__. Elle ne fut jamais réalisée de son vivant mais elle comportait une mémoire, une unité de calcul et une unité de contrôle, comme dans les ordinateurs modernes, ainsi que des périphériques de sortie (des équivalents de nos claviers et imprimantes). 

| Babbage | Carte perforée|
|----------- |----------- |
| ![](img/Babbage.png) |![](img/carteperfor.png)|

- __Ada Lovelace compose les premiers programmes__ pour la machine analytique, elle a compris qu'une telle machine est __universelle__ et peut exécuter n'importe quel programme de calcul.

![](img/Lovelace.png){: .center}

###  ^^1.3 - Fondements théoriques de l'informatique^^

Dans un article fondateur de __1936__ _"On computable numbers, with an application to the entscheidungsproblem"_, __Alan Turing__, définit précisément la notion de calcul et la relie à l'exécution d'un algorithme par une machine imaginaire qui servira de modèle aux ordinateurs modernes.

![Alan Turing](img/Turing.jpg){: .center}

En __1937__, __Claude Shannon__ démontre comment il est possible d'effectuer des calculs à l'aide de l'électricité avec des __relais électromagnétiques__ en prolongeant les travaux du logicien Georges __Boole__ (1815-1864). Il explique comment construire un additionneur à quatre **chiffres binaires** qu'il désigne pour la première fois sous le terme de __bit__ (contraction de _binary digit_).

![](img/Shannon.png){: .center}

### ^^1.4 - Machines à programmes externes^^

La seconde guerre mondiale accélère la réalisation de machines à calculs pour calculer des trajectoires balistiques ou déchiffrer des codes secrets.

- En Allemagne, Konrad Zuse réalise en __1941__, le __Z1__, première machine entièrement automatique lisant son programme sur une __carte perforée__. 
- Aux États-Unis, Howard Aiken conçoit le __Mark I__. Ces premières __machines électromécaniques__ sont colossales, et occupent des pièces entières.
- En __1945__, Mauchly et Eckert conçoivent avec l'__ENIAC__ une première machine utilisant des __tubes à vide__.

![](img/ENIAC.png){: .center}

###  ^^1.5 - L'ordinateur, une machine universelle à programme enregistré^^

Les __premiers ordinateurs__ apparaissent aux États-Unis et en Angleterre, juste après-guerre, ils sont réalisés __selon l'architecture décrite par John Von Neumann__ dans son rapport sur la construction de l'[EDVAC](https://fr.wikipedia.org/wiki/Electronic_Discrete_Variable_Automatic_Computer){target="_blank"}.

![](img/Von_Neumann.png){: .center}

__Un ordinateur est une machine programmable__, capable d'exécuter tous les programmes calculables sur une machine de Turing et dont __les programmes et les données sont enregistrés dans la même mémoire__.

### ^^1.6 - Miniaturisation et démocratisation^^

Dans les __années 1950__, les firmes DEC, BULL et surtout IBM développent les premiers ordinateurs commerciaux et les progrès technologiques s'enchaînent :

- Le __transistor__, inventé en __1947__, remplace progressivement les __tubes à vide__. 

| Invention du transistor | Transistors actuels (non miniaturisés) |
|----------- |----------- |
| ![](img/Transistor.png)| ![](img/transist2.png)|

- À partir de la __fin des années 1960__, la densité de transistors par unité de surface des plaques de silicium constituant les circuits intégrés, double environ tous les 18 mois. Cette progression est popularisée sous le nom de __loi de Moore__, du nom du fondateur d'Intel. La miniaturisation accompagne la progression des capacités de calcul et la démocratisation des ordinateurs. 

| Premier circuit intégré | Premier microprocesseur|
|----------- |----------- |
| ![](img/Circuit_integre.png)| ![](img/TI7400.png)|

- En __1971__, l'apparition du __microprocesseur Intel 4004__, marque les __débuts de la micro-informatique__.

| Microprocesseurs | Début de l'informatique grand public|
|----------- |----------- |
| ![](img/Chips.png) |![](img/vieuxpc2.png)|

- Avec l'essor du réseau Internet et de ses applications comme le Web et l'explosion des télécommunications mobiles, les objets se transforment en ordinateurs : __smartphones, objets connectés__,...

## ^^2. Architecture de Von Neumann^^
### ^^2.1 - Une architecture ancienne et pourtant toujours actuelle^^

L'architecture générale des ordinateurs a été développée dans le cadre d'un [projet EDVAC](https://fr.wikipedia.org/wiki/Electronic_Discrete_Variable_Automatic_Computer){target="_blank"} par [John Von Neumann](https://fr.wikipedia.org/wiki/John_von_Neumann){target="_blank"} (et d'autres scientifiques de l'époque), mathématicien et physicien américano-hongrois en __1945__. 

Ce modèle propose une structure dans laquelle __le programme est stocké dans la mémoire de la machine__. 

On parle maintenant de __modèle de Von Neumann__ et il est toujours utilisé dans les ordinateurs actuels.


__Le modèle de Von Neuman à l'origine__ :
![Von Neuman originel](img/modeleNeuman-originel2.png){: .center}


### ^^2.2 - Le modèle de Von Neumann^^

L'architecture de von Neumann décompose l'ordinateur en __4 parties distinctes__ :

- l'__unité arithmétique et logique__ (__UAL__ ou __ALU__ en anglais) ou unité de traitement : son rôle est d'effectuer les opérations de base, au niveau du bit. 
- l'__unité de contrôle__ (__CU__ Control Unit), chargée du __« séquençage » des opérations__, joue un rôle de coordonnateur.
- la __mémoire__ qui __contient à la fois les données et le programme__ indiquera à l'unité de contrôle quels sont les calculs à faire sur ces données.
- les dispositifs d'__entrée-sortie__ permettent de communiquer avec le monde extérieur.

> __Remarque :__ 
L'unité de commande et l'unité arithmétique et logique forment le __processeur__.


Lorsque une opération est terminée, l'unité de commande passe à l'instruction suivante du programme. 

La __fréquence d'exécution du processeur est contrôlée par un signal d'horloge__. Les cadences des processeurs usuels sont de l'ordre du GHz à l'heure actuelle.


### ^^2.3 - Quelques variations à l'architecture de von Neumann^^
#### 2.3.1 - Faire perdurer la mémoire, alimentation éteinte

La __mémoire vive__ (__RAM__ , pour _Random Access Memory_) de l'ordinateur a besoin d'être alimentée en permanence pour garder les données. À chaque extinction de l'ordinateur, elle est perdue : on la qualifie de __mémoire volatile__. Mais c'est dans la mémoire vive qu'est chargé le système d'exploitation de l'ordinateur (Windows, Linux, etc) et que sont stockées provisoirement les données dont il a besoin lors de l'exécution de programmes (navigateur Web, outils bureautiques, etc.). C'est en quelque sorte la mémoire à court-terme de l'ordinateur. 

Pour résoudre le problème de non persistance de la RAM, on recourt à deux types de __mémoires non volatiles__ :

- La __mémoire morte__ est une mémoire qui ne peut être que lue (__ROM__ Read Only Memory) : elle contient  le __micrologiciel (firmware)__ de l'ordinateur (__BIOS__ ou UEFI) qui est le programme qui se charge à chaque allumage de l'ordinateur, avant même le lancement du système d'exploitation.
- La __mémoire de masse__. Pour stocker les données et les programmes, on ajoute un périphérique appelé mémoire de masse : un __disque dur__ (HDD = Hard Disk Drive) et/ou une __mémoire Flash__ (SSD = Solid State Drive). Cette mémoire est capable de __stocker une grande quantité de données__, mais à l'inconvénient d'être __beaucoup moins rapide__ que la mémoire vive. C'est pour cela que lors du lancement d'un programme les données nécessaires à son exécution sont généralement transférées vers la RAM pour une exécution plus rapide.

!!! info "Un disque dur en action"
    Cliquer [ici](https://www.youtube.com/watch?v=9eMWG3fwiEU){target="_blank"} pour voir en vidéo un disque dur en action.


#### 2.3.2 - Accélérer les accès à la mémoire

Les performances des processeurs augmentant, le temps d'accès à la RAM peut être un frein à l'exécution des tâches du processeur. 

Pour remédier à ce goulot d'étranglement, les processeurs contiennent maintenant des __registres__ et de la __mémoire cache__, des types de mémoire encore plus rapides que la RAM.

Dans un ordinateur, il y a donc __plusieurs niveaux de mémoire, leur capacité (liée à leur coût) étant inversement proportionnelle à leur vitesse__.

![Mémoires](img/Memoires.png){: .center}

<figure markdown>
|Mémoire| Temps d'accès| Débit |Capacité|
|:---:|:---:|:---:|:---:|
|Registre| 1 ns|| ≈ Kilooctet|
|Mémoire cache| 2−3 ns|| ≈ Megaoctet|
|RAM| 5−60 ns| 1−20 Gio/s |≈ Gigaoctet|
|Disque dur |3−20 ms |10−320 Mio/s |≈ Teraoctet|
</figure>

#### 2.3.3 - Multiplier les processeurs

On utilise aujourd'hui des architectures **multiprocesseurs** afin d'améliorer la rapidité d'exécution sans augmenter la fréquence d'horloge.

![Modèle de Von Neumann Multi-processeurs](img/Von_Neumann_multi_processeurs.png){: .center}

### ^^2.4 - L'ordinateur actuel^^

!!! info "Structure et fonctionnement général d'un ordinateur contemporain"
    Cliquer [ici](https://youtu.be/cDnvRSIpNUQ){target="_blank"} pour voir une animation du CEA.

## ^^3. Exercice d'application^^

!!! done "Accéder au micrologiciel (BIOS/UEFI) de l'ordinateur"
    Suivre les instructions de votre professeur.

## ^^4 - QCM d'entraînement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Architecture matérielle"**.



