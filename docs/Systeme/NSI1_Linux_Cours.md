# Les commandes Shell Linux :penguin:
 

## ^^1. Le Shell, une Interface Homme-Machine pour les systèmes d'exploitation^^

Une interface entre l'utilisateur et le système d'exploitation s'appelle un __[Shell ou interpréteur de commandes](https://doc.ubuntu-fr.org/shell){target="_blank"}__.

Le rôle d'un Shell est de prendre une entrée de l'utilisateur, de la traduire en instructions compréhensibles par le système d'exploitation et de renvoyer la réponse du système à l'utilisateur.

Il existe deux grandes catégories de Shell :

- les __interfaces graphiques__ (système de fenêtres "pilotables" à la souris) qu'on retrouve dans les systèmes d'exploitation grand public tels que __Windows__ ;
- les __interfaces textuelles comme Bash__, le plus commun sur les systèmes de la famille UNIX.

À la "préhistoire" des systèmes d'exploitation, il n'y avait pas d'interface graphique : toutes les interactions "système d'exploitation - utilisateur" se faisaient par l'intermédiaire de __lignes de commandes__, des suites de caractères, souvent ésotériques, saisies par l'utilisateur. Aujourd'hui, même si les interfaces graphiques modernes permettent d'effectuer la plupart des opérations, il est important de connaître quelques-unes de ces lignes de commandes.

De manière générale, une commande *Shell* est constituée du **nom de la commande** suivi d'un ou plusieurs **arguments**. Des **options**, toujours précédées d'un tiret, peuvent modifier le comportement de la commande :

```console
nom_commande -option1 -option2 ... arg1 arg2 arg3 ...
```

!!! done "Découvrir les lignes de commandes"
    Nous allons découvrir ces commandes en nous connectant à un mini-serveur UNIX (un _Raspberry Pi version 4_ fonctionnant sous Linux), avec lequel nous allons interagir grâce à une console (appelée aussi "terminal").
    
    Votre professeur vous fournira les consignes pour établir cette connexion. 



## ^^2. Les commandes usuelles sous Linux^^

Dans ce memento, nous présentons les principales commandes du *shell* Linux.
Un memento plus complet en ligne est disponible [ici](https://juliend.github.io/linux-cheatsheet/){target="_blank"}.

!!! info "Conseils pratiques"
    Pour faciliter la saisie des commandes avec le clavier, il existe quelques raccourcis clavier bien pratiques :

    *  la touche de tabulation ++tab++ permet d'appeler la complétion automatique qui propose de compléter automatiquement le mot. Par exemple si on saisit `whoa`, l'appui sur la touche de tabulation complète automatiquement la commande en `whoami` (_Who am I?_) car il n'y a qu'une seule commande qui commence par `whoa`.

    * les flèches de direction Haut et Bas permettent de naviguer dans l'historique des commandes.


### ^^2.1. Lister les fichiers et sous-répertoires du répertoire courant^^
 
La commande `ls` (comme _list_) permet d'afficher des informations sur répertoire ou un fichier  :

* Sans argument ni option, `ls` liste le contenu du répertoire courant :

```console
Bidule@raspberrypiv4:~/bac_a_sable $ ls
arborescence.txt dossier1  dossier2  dossier3  fichier1  fichier2  fichier3  fichier4
``` 


* Avec l'option `-l` (:warning: un "L" en minuscule, pas le chiffre 1), elle affiche des informations détaillées sur chacun des fichiers contenus dans le répertoire :

```console
Bidule@raspberrypiv4:~/bac_a_sable $ ls -l
total 32
-rw-r--r-- 1 Bidule eleveNSI  337 28 oct. 08:15 arborescence.txt
drwxr-xr-x 2 Bidule eleveNSI 4096 28 oct. 08:15 dossier1
drwxr-xr-x 2 Bidule eleveNSI 4096 28 oct. 08:15 dossier2
...........
-rw-r--r-- 1 Bidule eleveNSI   47 28 oct. 08:15 fichier4
```

* Si on ne veut afficher que ce qui commence par `fic`, on donne en argument `fic*`, où l'étoile `*` remplace n'importe quelle chaîne de caractères :
        
        
```console
Bidule@raspberrypiv4:~/bac_a_sable $ ls fic*
fichier1  fichier2  fichier3  fichier4
```  

!!! info "Le caractère spécial `*` "
    Le caractère spécial `*`<a id="globbing"></a>, qui remplace n'importe quelle chaîne de caractères, peut être utilisé en début, en fin ou "au milieu" d'une expression. 

    Ainsi `*.txt` désigne tous les fichiers qui se terminent par `.txt`, tandis que `*perso*` désigne tous les fichiers qui contiennent `perso` dans leur nom. _Attention_, le `*` peut très bien remplacer une chaîne de caractères vide : le fichier dénommé `perso.doc` vérifie donc bien le _motif_ ( :flag_gb: :flag_us: _pattern_) `*perso*`.


### ^^2.2. Lecture de contenu d'un fichier texte^^

Il est possible de consulter le contenu d'un fichier texte via la console, sans utiliser d'éditeur de texte, avec la commande `cat` :

```console
Bidule@raspberrypiv4:~/bac_a_sable $ cat fichier4
Cours NSI : fichier 4
Merci de votre lecture !
```

À titre pédagogique, on a représenté l'arborescence de ce répertoire `bac_a_sable` dans le fichier `arborescence.txt`. Ouvrez-le pour voir !


### ^^2.3.  Naviguer dans l'arborescence du système de fichiers^^ 


**a)** La commande `pwd` (_print work directory_) permet d'afficher le chemin absolu du répertoire courant (ou répertoire de travail). C'est utile si on est un peu perdu dans l'arborescence.

```console
Bidule@raspberrypiv4:~/bac_a_sable $ pwd
/home/Bidule/bac_a_sable
```

!!! info "Le _prompt_"
    On peut voir le répertoire courant dans le **prompt** (_ce qu'il y a avant le `$` dans la ligne de commande_), sachant que le symbole tilde `~` est un raccourci pour désigner le répertoire personnel de l'utilisateur.

**b)** La commande `cd` (_change directory_) permet de changer de répertoire courant.


*  Suivie du nom d'un sous-répertoire, elle permet de "descendre" dans ce sous-répertoire :

```console
Bidule@raspberrypiv4:~/bac_a_sable $ cd dossier1
Bidule@raspberrypiv4:~/bac_a_sable/dossier1 $ pwd
/home/Bidule/bac_a_sable/dossier1
```

*  Avec un **espace suivi de deux points** (`cd ..`), elle permet de "remonter" dans le répertoire parent :

```console
Bidule@raspberrypiv4:~/bac_a_sable/dossier1 $ cd ..
Bidule@raspberrypiv4:~/bac_a_sable $ pwd
/home/Bidule/bac_a_sable
```

* Sans argument ou avec un tilde `~`, elle ramène l'utilisateur directement dans son répertoire personnel `/home/user`.

```console
Bidule@raspberrypiv4:~/bac_a_sable $ cd
Bidule@raspberrypiv4:~ $ pwd
/home/Bidule
```

* On peut fournir à `cd` un chemin absolu ou relatif  mais il faut que le chemin soit uniquement constitué de répertoires, et pas de noms de fichiers !

```console
Bidule@raspberrypiv4:~ $ cd /home/pi/bac_a_sable/dossier1
Bidule@raspberrypiv4:~/bac_a_sable/dossier1 $ cd
Bidule@raspberrypiv4:~/ $ cd bac_a_sable
Bidule@raspberrypiv4:~/bac_a_sable/ $
```


### ^^2.4. Copier, supprimer, déplacer un fichier ou un répertoire^^


**a)** La commande `mv` (_move_) sert à déplacer ou renommer des fichiers ou des répertoires. 

Elle prend deux arguments `nom1` et `nom2` :

* le fichier `nom1` est renommé en `nom2` si `nom2` n'est pas déjà un répertoire  ; 
* et sinon `nom1` est déplacé dans `nom2`.

!!! done ""
    Exemple de renommage :

```console
Bidule@raspberrypiv4:~/bac_a_sable $ ls
arborescence.txt dossier1  dossier2  dossier3 fichier1  fichier2  fichier3  fichier4
Bidule@raspberrypiv4:~/bac_a_sable $ mv fichier1 fichier1_new
Bidule@raspberrypiv4:~/bac_a_sable $ ls
arborescence.txt dossier1  dossier2  dossier3 fichier1_new  fichier2  fichier3  fichier4
```

!!! done ""
    Exemple de déplacement d'un fichier dans un autre répertoire :

```console
Bidule@raspberrypiv4:~/bac_a_sable $ ls dossier1
test1.txt test2.txt
Bidule@raspberrypiv4:~/bac_a_sable $ mv fichier1_new dossier1
Bidule@raspberrypiv4:~/bac_a_sable $ ls dossier1
fichier1_new  test1.txt  test2.txt
```

!!! done ""
    Exemple de déplacement d'un répertoire dans un autre répertoire :

```console
Bidule@raspberrypiv4:~/bac_a_sable $ mv dossier1 dossier2
Bidule@raspberrypiv4:~/bac_a_sable $ ls dossier2
dossier1  son1.mp3  son2.mp3
```

**b)** La commande `cp` (_copy_) permet de copier des fichiers. Elle s'utilise comme `mv`, sauf que le fichier `nom1` n'est pas supprimé. 
Par défaut `cp` ne copie que des fichiers : pour copier un répertoire et son contenu, il faut lui passer l'option `-r` (pour `recursive`).

!!! done ""
    Exemple de copie d'un fichier dans un autre répertoire :

```console        
Bidule@raspberrypiv4:~/bac_a_sable $ cp fichier2 dossier2
Bidule@raspberrypiv4:~/bac_a_sable $ ls dossier2
dossier1 fichier2 son1.mp3  son2.mp3
```

!!! done ""
    Exemple de copie d'un répertoire dans un autre répertoire

```console 
Bidule@raspberrypiv4:~/bac_a_sable $ cp dossier2 dossier3
cp: -r non spécifié ; omission du répertoire 'dossier2'
Bidule@raspberrypiv4:~/bac_a_sable $ cp -r dossier2 dossier3
Bidule@raspberrypiv4:~/bac_a_sable $ ls dossier3
dossier2 photo1.jpg
```


**c)** La commande `rm` (_remove_) permet de supprimer les fichiers qu'on lui passe en argument. Pour supprimer un répertoire et son contenu, il faut lui passer l'option `-r` (comme pour `mv`). 

:warning: Attention,  `rm` ne déplace pas les fichiers vers une corbeille, ils sont supprimés définitivement !

!!! done ""
    Exemple de suppression d'un fichier :

```console 
Bidule@raspberrypiv4:~/bac_a_sable $ ls
arborescence.txt dossier2  dossier3 fichier2  fichier3  fichier4
Bidule@raspberrypiv4:~/bac_a_sable $ rm fichier2
Bidule@raspberrypiv4:~/bac_a_sable $ ls
arborescence.txt dossier2  dossier3 fichier3  fichier4
```

!!! done ""
    Exemple de suppression d'un répertoire

```console 
Bidule@raspberrypiv4:~/bac_a_sable $ rm dossier3
rm: impossible de supprimer 'dossier3': est un dossier
Bidule@raspberrypiv4:~/bac_a_sable $ rm -r dossier3
Bidule@raspberrypiv4:~/bac_a_sable $ ls
arborescence.txt dossier2  fichier3  fichier4 
```

<!---
#  Expansion des noms de fichiers et *globbing* 

On peut agir en masse sur des  fichiers grâce aux mécanismes d'expansion de la ligne de commandes : certains caractères spéciaux indiquent au *shell* qu'il   peut les remplacer par des ensembles de caractères. On peut ainsi décrire des motifs (ou *pattern*) pour décrire des ensembles de  noms de fichiers.

Pour les exemples, on  considère un répertoire `rep4` qui contient plusieurs fichiers :

        Bidule@raspberrypiv4:~/bac_a_sable/rep4$ ls
        image1.gif  image1.jpg  image1.png  image2.gif  image2.jpg  image2.png  image3.gif  image3.jpg  image3.png  image4.gif

  *  Le caractère spécial `*` représente n'importe quelle suite de caractères. Par exemple pour lister les fichiers dont le nom de base se termine par 1 et l'extension par `g` on peut écrire :
                Bidule@raspberrypiv4:~/bac_a_sable/rep4$ ls *1.*
                image1.gif  image1.jpg  image1.png

  *  Le caractère spécial `?`  représente un caractère unique quelconque. Par exemple pour lister les fichiers dont le nom de base se termine par 1 et l'extension comporte trois caractères et se termine par `g` on peut écrire :
                Bidule@raspberrypiv4:~/bac_a_sable/rep4$ ls *1.??g
                image1.jpg  image1.png

 *  Dans un nom de fichier existant`{a..z}`  représente un caractère  entre `a` et `z`. Par exemple pour lister les fichiers `image1.png`, `image2.png` et `image3.png` on peut écrire :   
                Bidule@raspberrypiv4:~/bac_a_sable/rep4$ ls image{1..3}.png
                image1.png  image2.png  image3.png
-->


!!! question "Escape Game numéro 1"
    * Aller dans le répertoire `escapeGame1` en saisissant la commande `cd ../escapeGame1`.
    * Afficher le contenu du fichier `consigne1.txt` grâce à la commande `cat consigne1.txt`, puis suivre les instructions.


### ^^2.5. Création d'un répertoire, d'un fichier^^

**a)** La commande `mkdir` (`make directory`) sert à créer un nouveau répertoire.

```console    
Bidule@raspberrypiv4:~/bac_a_sable $ mkdir dossier4
Bidule@raspberrypiv4:~/bac_a_sable $ ls
arborescence.txt dossier2  dossier4  fichier3  fichier4
```

**b)** Pour créer (puis éditer) un fichier texte, on peut utiliser un petit éditeur de texte en mode console : `nano nom_du_fichier`

```console
Bidule@raspberrypiv4:~/bac_a_sable $ nano fichier5
```

On peut alors :

* saisir, corriger ou supprimer du texte ;
* sauvegarder avec les touches ++ctrl+"S"++ puis quitter avec ++ctrl+"X"++ .

### ^^2.6. Recherche d'un fichier^^

La commande `find` suivie de l'option `-name` permet de rechercher des fichiers d'après leur nom à l'intérieur du répertoire courant.

* La commande `find -name 'test2.txt'` permet de localiser le fichier `test2.txt` à l'intérieur du répertoire courant :

```console        
Bidule@raspberrypiv4:~/bac_a_sable $ find -name 'test2.txt'
./bac_a_sable/dossier2/dossier1/test2.txt
```

* Si on ne connaît pas le nom complet du fichier, on peut utiliser le caractère `*` qui est comme un _joker_  🃏 (voir [ici](#globbing) ) :

```console
Bidule@raspberrypiv4:~/bac_a_sable $ find -name 'te*.txt'
./bac_a_sable/dossier2/dossier1/test1.txt
./bac_a_sable/dossier2/dossier1/test2.txt
```


### ^^2.7. Gestion des droits sur les fichiers^^

Considérons le contenu  du répertoire `~/bac_a_sable` affiché de façon détaillée avec la commande `ls -l` :

```console
Bidule@raspberrypiv4:~/bac_a_sable $ ls -l
total 12
-rw-r--r-- 1 Bidule eleveNSI  337 28 oct. 08:15 arborescence.txt
drwxr-xr-x 3 Bidule eleveNSI 4096 28 oct. 08:15 dossier2
drwxr-xr-x 2 Bidule eleveNSI 4096 28 oct. 08:15 dossier4
-rw-r--r-- 1 Bidule eleveNSI   22 28 oct. 08:15 fichier3
-rw-r--r-- 1 Bidule eleveNSI   47 28 oct. 08:15 fichier4
```

**a)**  Les 10 premiers caractères d'une ligne représentent  les droits sur le fichier (ou le répertoire) :

!!! done "Exemple dans le cas de `fichier3`"
    Pour `fichier3` on a `-rwxr--r--` :

    * le premier caractère `-` indique qu'il s'agit d'un fichier.
    * le premier bloc de trois caractères `rwx` représente les droits pour le _propriétaire_ (u, comme _user_), ici `Bidule`, du fichier :  _lecture (r)_ et _écriture (w)_ et _exécution (x)_. 
    * le second bloc de trois caractères `r--` représente les droits pour le _groupe_ (g), ici `eleveNSI`, du fichier :  uniquement des droits de _lecture (r)_ et des tirets `-` qui marquent l'absence de droit d'écriture et d'exécution
    * le dernier bloc de trois caractères `r--` représente les droits pour les _autres_ (o, comme _others_) utilisateurs du fichier : uniquement des droits de _lecture (r)_.

!!! done "Exemple pour `dossier2`"
    Pour `dossier2`, on a `drwxr-xr-x` :

    * le premier caractère `d` indique qu'il s'agit d'un répertoire (`d` comme _directory_).
    * les trois blocs de trois caractères suivants énumèrent les droits en _lecture (r)_, _écriture (w)_, _exécution (x)_ des trois types d'utilisateurs du répertoire : _propriétaire_, _groupe_ et _autres_.

**b)** Le propriétaire d'un fichier peut changer les droits d'un fichier ou d'un répertoire avec la commande `chmod` dont la syntaxe est :

```chmod [-R] [ugoa][+-=][rwx] nom_du_fichier```

* Les options entre crochets désignent :
    * `u` : le propriétaire
    * `g` : le groupe
    * `o` : les autres utilisateurs
    * `a` : tous les utilisateurs
    * `+` : ajouter le(s) droit(s)
    * `-` : enlever le(s) droit(s) 
    * `=` : fixer le(s) droit(s)
    * `r` : droit de lecture
    * `w` : droit d'écriture
    * `x` : droit d'exécution 
    * `-R` : récursivement (nécessaire pour agir sur un répertoire)

!!! done "Quelques exemples"

    * Ajouter le droit d'exécution au propriétaire (_user_) sur `fichier3` :
    `Bidule@raspberrypiv4:~/bac_a_sable $ chmod u+x fichier3`
      
    * Enlever le droit de lecture au groupe et aux autres sur `fichier4` :
    `Bidule@raspberrypiv4:~/bac_a_sable $ chmod go-r fichier4`

    * Enlever le droit de lecture au groupe et aux autres sur le répertoire `dossier2` ainsi que tout son contenu :
    `Bidule@raspberrypiv4:~/bac_a_sable $ chmod -R go-r dossier2`

    * Fixer les droits à écriture (seulement) pour tous sur `arborescence.txt` :
    `Bidule@raspberrypiv4:~/bac_a_sable $ chmod a=w arborescence.txt`

    * Affichage des droits après les modifications précédentes :

    ```console
    Bidule@raspberrypiv4:~/bac_a_sable $ ls -l
    total 12
    --w--w--w- 1 Bidule eleveNSI  337 28 oct. 08:15 arborescence.txt
    drwx--x--x 3 Bidule eleveNSI 4096 28 oct. 08:15 dossier2
    drwxr-xr-x 2 Bidule eleveNSI 4096 28 oct. 08:15 dossier4
    -rwxr--r-- 1 Bidule eleveNSI   22 28 oct. 08:15 fichier3
    -rw------- 1 Bidule eleveNSI   47 28 oct. 08:15 fichier4
    ```

    * _Hors programme_ : il est maintenant possible d'exécuter `fichier3`, en saisissant la commande `sh fichier3` (`sh` est l'abréviation de _Shell_). 


!!! question "Escape Game numéro 2"

    * Aller dans le répertoire `escapeGame2` en saisissant la commande `cd ../escapeGame2`.
    * Afficher le contenu du fichier `consigne0.txt` grâce à la commande `cat consigne0.txt`, puis suivre les instructions.


## ^^3 - QCM d'entraînement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Commandes Linux"**

## ^^4 - Pour les rapides : Terminus^^

Faire le [jeu en ligne Terminus](http://luffah.xyz/bidules/Terminus/) sur les commandes Linux.


## ^^5 - Pour aller plus loin^^

Vous êtes devenus fans de la ligne de commande ? 

* Installez [Termux](https://play.google.com/store/apps/details?id=com.termux) sur votre smartphone Android ou [iSH](https://apps.apple.com/us/app/ish-shell/id1436902243?at=1010l392g&ct=htg801216) sur votre iPhone.

* Inscrivez-vous au MOOC [Maîtriser le shell Bash](https://www.fun-mooc.fr/fr/cours/maitriser-le-shell-bash/).
