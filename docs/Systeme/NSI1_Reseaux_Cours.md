# Introduction aux réseaux de données :steam_locomotive:

Ce que l'on appelle __Internet__ est un réseau mondial de réseaux. 

Ces réseaux servent à connecter différents terminaux (ordinateurs, smartphones, consoles de jeu...).


Afin de communiquer ensemble, ces terminaux, très différents les uns des autres, utilisent des __protocoles__. Les protocoles permettent de préciser la forme des messages à s'envoyer et dans quel ordre, tout cela pour que les communications puissent aboutir. Les deux principaux protocoles à la base d'Internet sont __TCP__ et __IP__.

!!! info "Transport des données sur Internet"
    Regarder  la [vidéo d'introduction](https://www.youtube.com/watch?v=AYdF7b3nMto){target="_blank"}, en activant si besoin le sous-titrage.

## ^^1. Composantes d'un réseau informatique^^

Détaillons les principaux éléments présents dans un réseau informatique.

### 1.1. Les terminaux (ordinateurs, smartphones, serveurs,...). 

Chacun de ces appareils possèdent une carte réseau (Ethernet, Wifi, 4G, 5G, ...) repérée par son "adresse MAC" (_Media Access Control_).  Une adresse MAC est un **code unique** attribué à la carte réseau lors de sa fabrication en usine.

Cette adresse est codée sur 48 bits, présentés sous la forme de 6 octets en hexadécimal. Exemple : ```fc:aa:14:75:45:a5```

!!! done "Trouver une adresse MAC"
    Trouver l'adresse MAC de votre ordinateur, puis celle de votre smartphone.

??? info "Une petite aide"
    * Pour un PC sous Linux, taper `ip addr` dans un terminal.
    * Pour un PC sous Windows, cliquer sur l'icone de réseaux puis sur "Propriétés".
    * Pour un smartphone sous Android, aller dans "_Réglages_" ou "_Paramètres_", puis dans "_À propos du téléphone_", puis dans "_État_" (ou "_Toutes les spécifications_" puis "_État_").
    * Pour un smartphone sous iOS : je ne maitrise pas les iPhones mais il doit bien exister un tutoriel sur le Web.

### 1.2. Les switchs (ou _commutateurs_)

Un switch permet de connecter plusieurs appareils entre eux par câble Ethernet mais il les différencie grâce à leur adresse MAC. 

Le switch et les terminaux qui lui sont reliés forment alors un réseau local ( :flag_gb: :flag_us: LAN pour _Local Access Network_) : ils peuvent communiquer entre eux sans passer par Internet.

![](images/Ethernet.png){: .center}


### 1.3. Les routeurs

Un routeur (ou passerelle ou _gateway_) permet de connecter un réseau local à Internet. Il différencie chaque appareil connecté grâce à une adresse IP locale (_Internet Protocol_).

L'adresse IP locale d'un appareil est attribuée par le routeur lors de la connection de l'appareil. À la différence de son adresse MAC, l'adresse IP locale d'un ordinateur peut changer d'un jour à l'autre ; on parle d'adressage dynamique (DHCP : _Dynamic Host Configuration Protocol_).

!!! done "Trouver une adresse IP locale"
    Trouver l'adresse IP locale de votre ordinateur, et la comparer à l'adresse de votre voisin/voisine.

??? info "Une petite aide"
    * Pour un PC sous Linux, taper `ip addr` dans un terminal.
    * Pour un PC sous Windows, cliquer sur l'icone de réseaux puis sur "Propriétés".

Le routeur dispose de sa propre adresse IP locale mais, pour pouvoir communiquer avec l'extérieur, il dispose aussi d'une adresse **IP externe**.


!!! done "Trouver une adresse IP externe"
    À l'aide par exemple du site [https://mon-ip.info/](https://mon-ip.info/){target="_blank"}, déterminer :
    
    * l'adresse IP externe du routeur sur lequel est connecté votre ordinateur, puis la comparer à celle de votre voisin/voisine ;
    * puis celle de votre smartphone.

!!! question "Exercice" 
    Décrire le réseau ci-dessus, en explicitant chaque composante ainsi que la nature des adresses IP mentionnées.
    
    ![](images/Reseau-1.png){: .center}

??? danger "Solution"
    On voit :
        
    * des terminaux, avec leurs adresses IP locales (192.168.0.4 et 201.9.118.44) ;
    * un switch ;
    * deux routeurs, avec leurs adresses IP locales (192.168.0.1 pour le routeur de gauche) et externes (82.30.12.18 toujours pour le routeur de gauche) ; 
    * une antenne 3G/4G/5G, qui joue le rôle de routeur ;
    * un relais wifi, qui joue le rôle de switch.


Chez vous, la box de votre opérateur joue simultanément le rôle de switch et de routeur :

- switch car elle répartit la connexion entre les différents dispositifs (ordinateurs branchés en ethernet, smartphone en wifi, tv connectée...)
- routeur car elle fait le lien entre ce sous-réseau domestique (les appareils de votre maison) et le réseau internet.

![](images/boxmaison.png){: .center}

Les routeurs tiennent à jour des __tables de routage__ pour savoir vers où envoyer les données en fonction des adresses IP : nous en reparlerons en Terminale.

### 1.4. Les serveurs DNS

Pour nous connecter à un site Web, nous saisissons son adresse Web (son URL : _Universal Resource Locator_) comme par exemple [fr.wikipedia.org](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal){target="_blank"}, ou bien nous cliquons sur un hyperlien qui contient l'URL de ce site Web. Or la communication sur Internet repose sur des adresses IP : il faut donc qu'il y a un **système de "traduction"** d'une adresse Web (URL) en adresse IP.

C'est le rôle des serveurs DNS (_Domain Name Serveur_), qui tiennent à jour des annuaires faisant correspondre une adresse Web (ou _nom de domaine_) et adresse IP. 
![](images/dns-explained.png){: .center}

Le fonctionnement est transparent pour l'utilisateur mais vous pouvez changer le paramétrage par défaut de votre ordinateur ou smartphone pour utiliser le serveur DNS de votre choix (et non celui de votre fournisseur d'accès Internet), par exemple celui proposé par la [FDN](https://www.fdn.fr/actions/dns/){target="_blank"}.

### 1.5. Retour sur les adresses IP  (_Internet Protocol_)

> _Remarque préliminaire_ : nous ne parlerons que des adresses IPv4 (version 4 du protocole IP) mais pas des adresses IPv6. Les adresses IPv4 restent encore largement utilisées, malgré la saturation des adresses disponibles.

Une adresse IP est un nombre entier codé sur 4 octets, soit 4 × 8 = 32 bits. 
Il est d'usage de l'écrire en décimal, avec un point entre chaque octet, comme par exemple `255.1.15.224` (au lieu de l'écriture binaire 11111111.00000001.00001111.11100000).

!!! Exercice "Adresse valide ou pas?"
    
    {{ multi_qcm(
        [
            "Que dire de l'adresse IP `123.231.321.0` ?",
            ["C'est une adresse valide dans tous les cas","C'est une adresse invalide dans tous les cas", "C'est une adresse valide uniquement comme adresse locale"],[2],
        ],
        )}} 

??? danger "Explication"
    Le plus grand nombre qu'on peut écrire sur un octet est `1111 1111` (en binaire), soit 255 en décimal. Le nombre 321 dépasse donc ce qu'on est capable d'écrire sur un octet, ce qui rend l'adresse invalide.

### 1.6. QCM Composantes d'un réseau

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Réseaux (partie 1)"**


## ^^2. TP Exploration des réseaux sous Linux^^

Si vous n'êtes pas sur un PC portable NSI, démarrer votre ordinateur sur la clé USB _bootable_ remise par votre professeur.

### 2.1. Connaître sa configuration matériel

* Ouvrir un terminal pour entrer en ligne de commande.
* Saisir la commande `ip addr` (`ipconfig` sur les anciennes versions de Linux) et repérer votre adresse IP locale ainsi que votre adresse MAC.


### 2.2. Communiquer sur le réseau local (_LAN chatting_).

Cette activité est à faire à deux : dans la suite, on les appelle ___élève1___ et ___élève2___.

* ___élève1___ saisit dans son terminal la commande `nc -l -p 1234` (avec un "L" minuscule, abréviation de _listen_ ; l'option `-p 1234` ouvre le _port de communication_ numéro 1234).
* ___élève2___ demande à ___élève 1___ son adresse IP locale puis saisit dans son terminal la commande `nc XXX.XXX.XXX.XXX 1234` (en remplaçant les XXX.XXX.XXX.XXX par l'adresse IP de ___élève1___), puis il commence à _chatter_ (par exemple en écrivant `c'est moi!!`)
* Vous communiquez maintenant via le réseau local (_LAN_), sans passer par Internet !

### 2.3. Tester et tracer la communication entre deux terminaux.

Le protocole IP repose sur la __communication par paquets__ (voir [plus bas](#paquet)). 

**a)** La commande `ping` permet de lancer l'envoi de paquets quelconques dans le seul but de tester la communication entre deux terminaux.

```console
toto@machine: ~$ ping -c 4 education.gouv.fr
```
On obtient un résultat comme ceci :
```console
PING education.gouv.fr (185.75.143.24) 56(84) octets de données.
64 octets de MEN-WEBEDU-PROXY01.dedie.ate.info (185.75.143.24) : icmp_seq=1 ttl=55 temps=16.8 ms
64 octets de MEN-WEBEDU-PROXY01.dedie.ate.info (185.75.143.24) : icmp_seq=2 ttl=55 temps=16.7 ms
64 octets de MEN-WEBEDU-PROXY01.dedie.ate.info (185.75.143.24) : icmp_seq=3 ttl=55 temps=19.4 ms
64 octets de MEN-WEBEDU-PROXY01.dedie.ate.info (185.75.143.24) : icmp_seq=4 ttl=55 temps=15.5 ms

--- statistiques ping education.gouv.fr ---
4 paquets transmis, 4 reçus, 0% packet loss, time 3185ms
rtt min/avg/max/mdev = 15.493/17.100/19.365/1.408 ms

```

La commande `ping` envoie des très petits paquets vers un hôte IP qui va répondre en envoyant des paquets en retour. L'option `-c 4` permet de limiter l'envoi à 4 paquets. On obtient à la fin quelques statistiques sur la qualité de la communication (pourcentage de paquets perdus, temps moyen pour effectuer un "aller-retour", etc.)

On peut aussi indiquer une adresse IP comme argument de la commande `ping` (par exemple  `ping -c 4 185.75.143.24`)

**b)** La commande `traceroute` permet de repérer les différents routeurs par lesquels ont transité les paquets lors d'une communication par IP.

```console
toto@machine: ~$ traceroute education.gouv.fr
```
Les éventuels `***` qui apparaissent correspondent à des routeurs que `traceroute` n'a pas pu identifier correctement (habituellement ceux qui sont éloignés du point de départ).

### 2.4. Résolution DNS

Pour connaitre l'adresse IP d'un site Web, on peut interroger directement un serveur DNS avec la commande `host`:

```console
toto@machine: ~$ host education.gouv.fr
education.gouv.fr has address 185.75.143.24
```

### 2.5. Application

!!! question "TP d'application"
    Compléter la fiche activité personnalisée remise par votre professeur.

### 2.6. QCM  Réseaux sous Linux

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Réseaux (partie 2)"**

## ^^3. Transport par paquets, protocole TCP^^ <a id="paquet"></a>

### 3.1. Les notions de base 

Imaginons que Mme. X veuille envoyer le message suivant « Bonjour cher ami, comment allez-vous ? » à M. Y via le réseau Internet.

Selon le protocole TCP/IP, le message de Mme X est découpé en plusieurs __paquets__ de taille fixe. À chaque paquet est rajouté un en-tête (contenant notamment l'adresse IP de départ et l'adresse IP de destination) pour former une __trame__.

_Dans l'illustration ci-dessous, on a représenté fictivement 4 paquets et on a gardé les notations alphabétiques alors qu'en réalité tout est converti en binaire (codage ASCII ou UTF8) et que les paquets font environ 1500 octets._

![Découpage paquets](images/decoupage_paquets.png){: .center}

Chaque trame (chaque paquet) est envoyée via Internet depuis l'ordinateur de Mme X à celui de M. Y et deux trames différentes peuvent emprunter des chemins différents.

Le protocole TCP (_Transport Control Protocol_) permet :

* de garantir que chaque paquet est bien arrivé à destination
* et si ce n'est pas le cas, de redemander à l'expéditeur de le renvoyer
* et de "recoller" les paquets dans le bon ordre.

Sans entrer dans les détails, cette garantie de bonne arrivée à destination repose sur le principe d'un accusé de réception (_acknowlegement_) : quand l'émetteur envoie un paquet, il attend un accusé de réception du destinataire ("paquet numéro N bien reçu") et s'il ne l'obtient pas au bout d'un certain temps, il renvoie le paquet en question.

### 3.2. QCM Transport de données sur un réseau

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Réseaux (partie 3)"**

## ^^4. TP Simulation d'un réseau^^

Nous utilisons le logiciel [Filius](https://www.lernsoftware-filius.de/Herunterladen){target="_blank"}, déjà installé sur votre PC.

### 4.1. Premier réseau local

Suivre le [tutoriel vidéo n°1](https://www.youtube.com/watch?v=nzuRSOwdF5I){target="_blank"} afin de créer le réseau local ci-dessous et d'installer un terminal de ligne de commande sur la première machine :

![](images/f1.png){: .center}

!!! done "Tester un ping"
    Tester le ```ping``` de la machine ```192.168.0.1```  vers la machine ```192.168.0.3``` et observer les échanges de données sur le réseau.


### 4.2. Connexion de deux réseaux avec un routeur

À l'aide du [tutoriel vidéo n°2:](https://www.youtube.com/watch?v=xyK6ThdQeR0){target="_blank"}, créer le réseau ci-dessous.

![](images/f5.png){: .center}

!!! info ""
    Il est important de dire à chaque machine qu'une passerelle est maintenant disponible pour pouvoir sortir de son propre sous-réseau. Il faut donc aller sur la machine ```192.168.0.1``` et lui donner l'adresse de sa passerelle, qui est ```192.168.0.254```.

    ![](images/passerelle.png)

    Attention, il faut faire de même pour toutes les machines, notamment pour ```192.168.1.2``` (:warning: avec la bonne passerelle) pour le test ci-dessous. 


!!! done "Test de la communication"
    - Testons un `ping` entre  ```192.168.0.1``` et ```192.168.1.2```.
    - Effectuons un ```traceroute``` entre  ```192.168.0.1``` et ```192.168.1.2```.



### 4.3. Réseau avec un serveur Web et un serveur DNS

### 4.3.1. Ajout d'un serveur web

- Reprenons le réseau de la partie 2 et connectons un ordinateur au SwitchB, sur l'adresse `192.168.1.30`. Installons dessus un `Serveur Web` ainsi qu'un `Éditeur de texte`, puis démarrons-le. 
![](images/serveurweb.png){: .center}

- Sur la machine `192.168.0.1`, rajoutons un `Navigateur Web`. En tapant dans la barre d'adresse l'adresse IP du Serveur web, la page d'accueil de Filius s'affiche.  
![](images/nav1.png){: .center}

- Revenons dans la configuration du Serveur Web : à l'aide de l'éditeur de texte, ouvrons le fichier ` index.html`  se trouvant dans le dossier `webserver` et modifions-le à notre guise. 

- Sur  la machine `192.168.0.1`, rechargeons la page du Navigateur Web pour visualiser les modifications effectuées.


#### 4.3.2. Rajout d'un serveur DNS

- Rajoutons un serveur DNS minimal, qui n'aura dans son annuaire d'un seul site. Il faut pour cela raccorder une nouvelle machine, et installer dessus un `Serveur DNS`.  
![](images/dns.png){: .center}

- Sur ce serveur DNS, associons l'adresse ```http://www.vivelansi.fr``` (_ou toute autre adresse de votre choix_) à l'adresse IP ```192.168.1.30```.  
![](images/vivelansi.png){: .center}

- De retour sur notre machine ```192.168.0.1```, spécifions maintenant l'adresse du serveur DNS :  
![](images/specdns.png){: .center}

- Depuis le navigateur web de la machine ```192.168.0.1```, le site ```http://www.vivelansi.fr``` (_ou l'adresse de votre choix_) est maintenant accessible.  
![](images/dnsex.png){: .center}

- On peut vérifier la bonne configuration en tapant `host www.vivelansi.fr` (_ou l'adresse de votre choix_) en ligne de commande dans la machine ```192.168.0.1```.

### 4.4. Simulation d'une panne sur un réseau complexe

Télécharger (_clic droit puis Enregistrer le lien sous_) puis ouvrir dans Filius le fichier [reseau_complexe.fls](reseau_complexe.fls)

![](images/Reseau_complexe.png){: .center}

!!! done "Simulation d'une panne"
    - Faire un `traceroute`  entre l'ordinateur M14 et l'ordinateur M9. Noter le chemin parcouru (captures d'écran…).
    - Afin de simuler une panne, supprimer le câble réseau qui relie le routeur F au routeur E : pour cela, aller dans la configuration du routeur E, puis "gérer les connexions" puis supprimer la connexion avec le routeur F.
    - Refaire un `traceroute`  entre M14 et M9. Que constatez-vous ?

