# Programmation avancée en Python :rocket:


## ^^1. Variables locales, variables globales^^

### 1.1 Notion d'espace de noms

!!! tip "Définitions :heart:"
    - Les variables définies dans le corps d'une fonction sont appelées **variables locales**.
    - Les variables définies dans le corps du programme (_sous-entendu_ : pas à l'intérieur d'une fonction) sont appelées **variables globales**.


On dit que les fonctions créent leur «espace de noms» (*espace* est à prendre au sens d'*univers*, de *monde parallèle*), un espace qui leur est propre.

Quelles sont les règles régissant ces espaces de noms ? Les frontières entre ces espaces sont elles poreuses ? 

### 1.2 Règles d'accès en lecture et en modification d'une variable suivant son espace d'origine

!!! tip "Règles d'accès aux variables locales et globales :heart:"
    - **règle 1 :** une **variable locale** (définie au cœur d'une fonction) est **inaccessible** hors de cette fonction.
    - **règle 2 :** une **variable globale** (définie à l'extérieur d'une fonction) est **accessible** en **lecture** à l'intérieur d'une fonction.
    - **règle 3 :** une **variable globale** (définie à l'extérieur d'une fonction) **ne doit pas être modifiée** à l'intérieur d'une fonction.


![image](data/global_regles.png){: .center width=80%}


!!! question "Exercice"
    === "Énoncé"
        On considère les 3 codes ci-dessous. Pour chacun, dire **sans l'exécuter** s'il est valide ou non. S'il ne l'est pas, identifier la règle (parmi celles énoncées ci-dessus) qui est bafouée.

        **code A**
        ```python linenums='1'
        points = 0
        def verdict(reponse):
            if reponse > 10:
                points += 3

        verdict(12)
        ```    

        **code B**
        ```python linenums='1'
        def bouge(x):
            decalage = 25
            x += decalage
            return x

        y = bouge(100)
        print(decalage)
        ```

        **code C**
        ```python linenums='1'
        def test_bac(moyenne):
            if moyenne >= 10:
                print("admis !")

        def coup_de_pouce(note):
            return note + bonus

        bonus = 0.6
        ma_moyenne = 9.5
        ma_moyenne = coup_de_pouce(ma_moyenne)
        test_bac(ma_moyenne)
        ```

    === "Correction code A"
        Ce code n'est pas valide, car il contrevient à la règle 3.

        ```ligne 4``` : la modification de la variable globale ```points``` est interdite.

    === "Correction code B"
        Ce code n'est pas valide, car il contrevient à la règle 1.

        ```ligne 5``` : l'accès à la variable locale ```decalage``` en dehors de la fonction `bouge`  est interdit.

    === "Correction code C"
        Ce code est valide.

        ```ligne 6``` : l'accès à la variable globale ```bonus``` est autorisé, selon la règle 2.            


!!! info "Paramètre d'une fonction = variable locale"
    Quand on définit une fonction avec un paramètre, par exemple `maFonction(monNombre)`, le paramètre (ici `monNombre`) se comporte comme une variable locale.

    Il est notamment impossible d'y accéder en dehors de la fonction.

    Par exemple, le code suivant génère une erreur :

    {{ IDE("data/fonction_bug")}}


!!! warning "Exception à la règle n°3"
    Il y a quand même une exception à la règle n°3 qui stipule qu'on ne doit pas modifier une variable globale dans une fonction : c'est quand cette variable est un tableau/une liste.

    En effet, la manipulation des valeurs du tableau et l'utilisation de méthodes de listes comme `append` conduisent à une **modification "sur place"** de la liste. 
    
    Dans le code ci-dessous, la variable globale `monTableau` a été modifiée après son passage dans la fonction !

    {{IDE("data/revisions_modif_inplace")}}


### 1.3. Masquage d'une variable globale par une variable locale

Un programmeur peu attentif peut créer une variable locale dans une fonction qui porte le même nom qu'une variable globale. Dans cette situation, la variable locale **masque** la variable globale : c'est la variable qui "prend le dessus" tant que l'on est dans la fonction.

!!! question "Exercice"
    Dans le code ci-dessous, le programmeur peu attentif a utilisé le mot `message` pour créer une variable locale et une variable globale.

    _Veuillez répondre au QCM avant d'appuyer sur le bouton Lancer_ :wink:

    {{ IDE("data/fonction_masquage")}}

    {{ multi_qcm(
        [
            "Que va-t-on obtenir à l'affichage après exécution du code ? ",
            ["'Bienvenue Alice' puis 'Bonjour Alice'","'Bienvenue Alice' deux fois de suite","'Bonjour Alice' deux fois de suite"],[1],
        ],
        )}}

    


!!! tip "Une bonne pratique"
    Pour éviter les erreurs, une bonne pratique est de ***ne pas utiliser le même nom*** pour une variable locale et une variable globale.

    Mais ce n'est qu'une bonne pratique, ce n'est pas une obligation : à vos risques et périls !



### 1.4. Deux exercices de synthèse

!!! question "Exercice "

    On considère le code suivant :

    ```python
    def ma_fonction(x):
        x = x + 1
        return 2*x
    # corps du programme
    y = ma_fonction(1)
    print("la variable x vaut ",x, " et la variable y vaut ",y)
    ```

    {{ multi_qcm(
        [
            "Qu'obtient-on à l'affichage après exécution de ce code ?",
            ["la variable x vaut 1 et la variable y vaut 2", 
            "la variable x vaut 1 et la variable y vaut 4",
            "la variable x vaut 2 et la variable y vaut 4",
            "on a un message d'erreur `#!py NameError, name 'x' is not defined`"],[4],
        ],
        shuffle = False)}}

    ??? danger "Explication"
        Pour bien visualiser : copier et exécuter le code dans [Python Tutor](https://pythontutor.com/visualize.html#mode=edit){target="_blank"}

        Quand un programme exécute une fonction, c'est un peu comme s'il partait dans un monde parallèle. Ici, la variable `x` n'existe que dans la fonction : c'est une variable **locale**.
        Elle n'existe plus quand on revient dans le corps du programme.



!!! question "Exercice "
    
    On considère le code suivant :

    ```python
    def ma_fonction(x):
        x = x + 3
        return 2*x
    # corps du programme
    x = 1
    y = ma_fonction(x)
    ```

    {{ multi_qcm(
        [
            "Quelles sont les valeurs des variables `#!py x` et `#!py y` après exécution du code suivant ?",
            ["la variable x vaut 4 et la variable y vaut 8",
             "la variable x vaut 1 et la variable y vaut 8",
             "la variable x vaut 1 et la variable y vaut 2",
            "on a un message d'erreur `#!py NameError, name 'y' is not defined`"],[2],
        ],
        shuffle = False)}}

    ??? danger "Explication"
        Pour bien visualiser : copier et exécuter le code dans [Python Tutor](https://pythontutor.com/visualize.html#mode=edit){target="_blank"}
        
        La variable `x` créée dans le corps du programme et qui vaut 1 est une variable **globale**. Cette variable globale n'est pas modifiée dans la suite du corps de programme : elle reste donc à 1 jusqu'à la fin. 

        Par contre, la fonction `ma_fonction` est définie à l'aide d'un paramètre qui s'appelle aussi `x`, mais ce `x` ne "vit" qu'à l'intérieur de la fonction : c'est une **variable locale** qui **masque** le `x` global. Quand le programme "entre" dans la fonction, le `x` local vaut au départ 1, puis il passe à 4 avec l'instruction `x = x + 3`. La fonction renvoie alors la valeur 2×4 = 8, qui est "capturée" par la variable `y`. 

        > Remarque : le code de l'exemple 2 n'est donné qu'à but pédagogique, pour faire percevoir la distinction entre variable globale et variable locale. Mais ce n'est pas un code très lisible et on évitera d'écrire des programmes avec de telles ambigüités.


### 1.5. QCM d'entrainement

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Programmation Avancée : variables locales et globales"**.

## ^^2. La programmation défensive : sécuriser son code avec des `assert`^^

### 2.1. L'instruction `assert`

La programmation défensive est l'art de prévoir le pire et d'essayer de le détecter avant qu'il ne soit trop tard.

De manière bien plus concrète, il est d'usage d'essayer de répérer si des données (souvent des paramètres d'une fonction) sont susceptibles de créer des problèmes, ou sont hors spécification.

!!! done "Exemple"
    Appuyer sur le bouton _Lancer_ ci-dessous :

    * il ne se passe rien : c'est normal, la fonction est juste définie mais elle n'est pas appelée ;

    * dans la console, taper `politesse('Alice')` (ou mettre votre propre prénom) : tout se passe normalement ;
    * toujours dans la console, `politesse(16)` (on n'importe quel autre nombre) : un message d'erreur **explicite** apparaît !

    Puisque, à la ligne 3, il n'est pas possible de _concaténer_ le texte `"Bonjour"` avec un nombre, on a protégé la fonction en vérifiant en ligne 2 que le paramètre reçu en entrée est bien un texte.

    {{ IDE("data/fonction_assert")}}


Une telle instruction `assert` est suivie :

- d'une condition, c'est-à-dire une __expression booléenne__ qui vaut `True` ou `False`
- éventuellement suivie d'une virgule , et d'une phrase en langue naturelle, sous forme d'une chaine de caractères.

!!! tip "Fonctionnement d'un `assert` "
    Quand l'instruction `assert` teste sa condition, deux cas sont possibles :

    - si la condition est satisfaite, elle ne fait rien (l'interpréteur passe à la ligne suivante)
    - sinon elle arrête immédiatement l'exécution du programme en affichant la phrase qui lui est éventuellement associée. 

Ainsi, plutôt que de faire planter le programme avec des données "abérrantes",  l'instruction `assert` arrête le programme et affiche un message clair pour corriger l'erreur !

!!! note ""
    À notre niveau, on peut considérer qu'un `assert` remplace un test du type `if... then... else` , c'est juste un peu plus rapide à écrire.


### 2.2. QCM d'entrainement

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Programmation Avancée : instruction assert"**.

## ^^3. Documenter une fonction^^


### 3.1 Help !
Si une fonction peut être assimilée à un outil, il est normal de se demander si cet outil possède un mode d'emploi.

Observons les fonctions pré-définies par Python, et notamment une des premières que nous avons rencontrées : la fonction ```print()```. Son mode d'emploi est accessible grâce à la commande ```help(print)``` : essayez dans la console ci-dessous !

{{ terminal() }}

 

### 3.2 Créer le mode d'emploi de ses propres fonctions : les docstrings

Il est possible, voire souhaitable (dès qu'on écrit un code comportant plusieurs fonctions, et/ou qui sera amené à être lu par d'autres personnes), de créer un mode d'emploi pour ses fonctions. 

On appelle cela écrire **la docstring** de la fonction, et c'est très simple : il suffit de l'encadrer par des triples double-quotes ```"""```.

!!! done "Exemple"
    Appuyer sur le bouton _Lancer_ ci-dessous :

    * il ne se passe rien : c'est normal, la fonction est juste définie mais elle n'est pas appelée ;
    * dans la console, taper `help(chat_penible)`

    {{ IDE("data/fonction_docstring")}}


!!! tip "Une autre bonne pratique : le contenu de la docstring"

    Le rôle de cette documentation est de décrire en quelques lignes :

    * à quoi sert la fonction
    * à donner le lien entre les entrées et la sortie
    * à préciser clairement les **spécifications** de la fonction, c'est-à-dire :

        - les **préconditions** qui décrivent les conditions d'utilisation de la fonction, en particulier le ___type___ des paramètres.
        - les **postconditions** qui décrivent ce que la fonction renvoie, en particulier le ___type___ de la sortie.

### 3.3. QCM d'entrainement

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Programmation Avancée : documenter les fonctions"**.

## ^^4. Jeux de tests pour une fonction^^

### 4.1. Pourquoi tester ?

Tester une fonction ne sert pas uniquement à vérifier s'il n'y a pas d'erreur de syntaxe : cela doit avant tout servir à s'assurer que la fonction va bien effectuer ce qu'on veut qu'elle fasse !

!!! warning "Un seul test ne suffit pas"
    Il faut souvent effectuer plusieurs tests pour tester la fonction dans différentes configurations.

    En théorie, ce n'est pas parce qu'un code a passé avec succès 10 000 tests que c'est un code correct : dans vos études supérieures, vous verrez comment **prouver** (_au sens mathématique du terme_) qu'un code est correct.

    En pratique, on se contentera d'effectuer plusieurs tests : c'est l'importance du code qui oriente le volume de tests à effectuer.

!!! done "Exemple"
    Un programmeur aimant bien les fractions remarque que :
    
    * le nombre $\frac{5}{2}$ est plus grand que 1 ;
    * le nombre $\frac{7}{10}$ est plus petit que 1 ;

    et ça lui donne l'idée de s'en servir pour définir la fonction `maximum` ci-dessous.

    _Veuillez répondre au QCM avant d'appuyer sur le bouton Lancer_ :wink:

    {{ IDE("data/fonction_tests")}}

    {{ multi_qcm(
        [
            "Lequel des tests lui montre qu'il a **mal programmé** sa fonction ?",
            ["le test 1", "le test 2", "le test 3", "tous les tests"],[3],
        ],
        shuffle = False)}}



### 4.2. QCM d'entrainement

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Programmation Avancée : tests de fonctions"**.

## ^^5. Exercices et TP^^

Télécharger le [carnet Jupyter](NSI1_Python_Avance_TP.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

---
??? summary "Sources :sparkles:"

    Ce support de cours est fortement inspiré de [celui-ci](https://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.5_Fonctions/cours/).

---