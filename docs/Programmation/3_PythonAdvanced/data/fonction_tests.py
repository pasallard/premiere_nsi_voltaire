def maximum(a,b):
    # cette fonction doit renvoyer le plus grand des deux nombres a et b
    if a/b > 1 :
        return a
    else :
        return b

# test 1
print(maximum(10,3))
#test 2
print(maximum(3,10))
#test 3
print(maximum(3,-10))