# Les bases de la programmation en Python :snake:

Le langage ***Python*** a été développé à partir de 1989 par ***Guido Von Rossum***, à l'université d'Amsterdam. 
Guido était un fan du groupe d'humoristes britanniques [Monty Python](https://fr.wikipedia.org/wiki/Monty_Python){target="_blank"}.

La version de Python actuellement utilisée est la **version 3**. :warning: Si vous récupérez des morceaux de code sur Internet, un code Python version 2 ne fonctionnera plus forcément !

Python est l'un des trois langages de programmation les plus utilisés au monde  : source [Tiobe Index](https://www.tiobe.com/tiobe-index/){target="_blank"}.


## ^^Partie 1^^

### 1.1. Apprendre ou réviser les bases de Python

!!! tip "FutureCoder"
    Aller sur le site [FutureCoder](https://fr.futurecoder.io){target="_blank"} puis :

    - cliquer sur _>>> Débuter le cours_ ;
    - puis sur _Connexion / Inscription_ (pour sauvegarder votre progression et pouvoir continuer de n'importe quel poste) ;
    - puis faire tous les premiers chapitres, de ___la console___ à ___structures conditionnelles if___.

### 1.2. S'entrainer

!!! question "à faire en Devoir Maison"
    Sur le site [France IOI](http://www.france-ioi.org/algo/chapters.php){target="_blank"} :

    - créer un compte en cliquant sur ***Se connecter*** (_en haut à gauche_)
    - dans le bandeau de gauche _Plan du site_, cliquer sur ***Groupes et classes*** : 
    
        - dans _Rejoindre un groupe_, chercher le mot "Voltaire_NSI_2024" ;
        - cliquer sur le lien `Voltaire_NSI_2024` qui est apparu ;
        - saisir le mot de passe : `voltaire`
        - parfait, vous avez bien rejoint le groupe :+1:
    
    - dans le bandeau de gauche _Plan du site_, cliquer sur ***Cours et problèmes*** : 

        - vérifier que **Python** est bien sélectionné dans la liste des langages proposés
        - dans l'onglet "Parcours général", faire **tout le Niveau 1**.
    
    - Vous pouvez faire les différents exercices du Niveau 1 en plusieurs fois : votre progression est enregistrée.

    
### 1.3. Compléments et Exercices

* Compléments sur les calculs et les variables en Python : télécharger le [carnet Jupyter](BasesPython_Complement1.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.
* Compléments sur les boucles `for` et les instructions `if` : télécharger le [carnet Jupyter](BasesPython_Complement2.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.


### 1.4. QCM d'entrainement

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Programmation Python partie 1"**

## ^^Partie 2 : les fonctions (et introduction aux listes) en Python^^

### 2.1. Aller un peu plus loin dans les bases de Python

!!! tip "FutureCoder"
    Aller sur le site [FutureCoder](https://fr.futurecoder.io){target="_blank"} puis faire __dans cet ordre__:
    
    - les parties __compléments sur les chaines__ et __boucles imbriquées__, en passant à la suite dès que vous arrivez sur un exercice contenant le mot ___liste___ ;
    - la partie  __fonctions__ ;
    - et commencer la découverte des __listes__.

### 2.2. Zoom sur les fonctions

Consolidons les acquis par quelques exercices :

!!! question "Savoir appeler une fonction"
    En appuyant sur le bouton _Lancer_, vous allez exécuter un code caché qui crée une fonction `mystere` :

    * qui prend un nombre en entrée,
    * et qui renvoie un autre nombre.

    {{IDE("data/fct_mystere")}}

    {{ multi_qcm(
        [
            """Quelle est la valeur renvoyée par cette fonction `mystere` si on lui fournit le nombre 5 en entrée ?""",
            ["11","5","Un message d'erreur", "21"],[1],
        ],
        )}}

    ??? danger "Explication"
        Dans la console, on saisit `mystere(5)` pour voir ce qui sort de la fonction.

        Ou bien dans le script, on rajoute l'instruction `print(mystere(5))`.

!!! question "Savoir compléter le code d'une fonction"
    On veut simuler un distributeur de boissons qui vend des boissons à 2€. Si l'utilisateur ne donne pas assez d'argent, le distributeur affiche "Pas assez de sous" et rend tout l'argent ; et si l'utilisateur donne au moins 2€, le distributeur affiche "Voici votre boisson" et rend la monnaie sur 2€.

    Pour cela, on a commencé à écrire le code d'une fonction `distributeur` dont le paramètre est appelé `argent`. Compléter ce code pour que cette fonction simule bien le fonctionnement du distributeur.

    {{ IDE("data/fct_distributeur")}}

    ??? danger "Solution"
        Une solution est la suivante :
        ```python
        def distributeur(argent):
        if argent < 2 :
            print("Pas assez de sous")
            monnaie_rendue = argent
        else :
            print("Voici votre boisson")
            monnaie_rendue = argent - 2
        return monnaie_rendue
        ```
        mais on peut aussi inverser les rôles en écrivant `if argent >= 2` etc.




!!! question "Comprendre le fonctionnement d'une fonction (1)"
    On considère la fonction `bidule` suivante, qui prend un nombre `nb` en paramètre.

    ```python
    def bidule(nb) :
        if nb > 5 :
            return 10*nb
        return 2*nb
    ```   

    {{ multi_qcm(
        [
            "Quelle est la valeur renvoyée par l'appel `bidule(8)` ?",
            ["80","80 puis 16", "16", "5"],[1],
        ],
        )}}

    ??? danger "Explication"
        Dès qu'un `return` est exécuté, on sort de la fonction et on n'exécute pas la suite. 
        Vérifiez-le en copiant puis exécutant le code dans un IDE.

!!! question "Comprendre le fonctionnement d'une fonction (2)"
    On considère la fonction `truc` suivante, qui prend un nombre `nb` en paramètre.

    ```python
    def truc(nb) :
        if nb > 5 :
            calcul = 10*nb
        calcul = 2*nb
        return calcul
    ```

    {{ multi_qcm(
        [
            "Quelle est la valeur renvoyée par l'appel `truc(8)` ?",
            ["80","80 puis 16", "16", "5"],[3],
        ],
        )}}

    ??? danger "Explication"
        Puique 8>5, la variable `calcul` vaut 80 à la sortie de l'instruction `if` MAIS cette valeur 80 est "écrasée" par la nouvelle affectation `calcul = 2*nb`. La variable `calcul` vaut alors 16 au moment où elle est renvoyée.
    

### 2.3. S'entrainer

!!! question "à faire en Devoir Maison"
    Sur le site [France IOI](http://www.france-ioi.org/algo/chapters.php){target="_blank"} :

    - dans le "Parcours général", faire **tout le Niveau 2**.
    

### 2.4. Compléments et Exercices

Compléments et exercices sur les fonctions en Python : télécharger le [carnet Jupyter](BasesPython_Complement3.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.


### 2.5. QCM d'entrainement

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Programmation Python partie 2"**

## ^^Partie 3 : Exercices de synthèse^^

Télécharger le [carnet Jupyter](BasesPython_Exos_Synthese.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.