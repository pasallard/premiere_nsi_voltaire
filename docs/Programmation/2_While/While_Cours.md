# Les boucles While en Python :loop:


Les boucles `while` ont été abordées à la fin du Niveau 1 du parcours général de [France IOI](http://www.france-ioi.org/algo/chapters.php){target="_blank"}. 

Il est important de revenir sur ce type d'instruction.

!!! done "Exemple dans une situation concrète :bulb:"
    Vous avez sans doute déjà rencontré la situation suivante : pour entrer dans un immeuble, il faut saisir un code et **tant que** le code n'est pas bon, on recommence à saisir un code.

    C'est l'exemple parfait d'utilisation d'une boucle `while` : si la description en français de tous les jours contient l'expression **tant que**, c'est qu'il faut utiliser une boucle `while` !

    Cette situation peut se coder ainsi (où un mot de passe remplace un code numérique) :

    ```python
    mot_de_passe = "bizarre" # mais l'utilisateur n'est pas censé voir ce mot de passe

    saisie = input("Entrer votre code :" )
    while saisie != mot_de_passe :
          print("Ce n'est pas bon, recommencez !")
          saisie = input("Entrer votre code :" )
    print("Bienvenue")
    ```

    _Testez ce code dans un IDE_ (le `input` marche mal sur le Python en ligne :disappointed:).



## 1. Éléments de cours

Lire le cours sur [ce site](https://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.3_Boucle_while/cours/){target="_blank"}.

## 2. Compléments et exercices sur les boucles `while`

* Télécharger le [carnet Jupyter](BasesPython_Complement4.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.
* Exercices  de codage de petits jeux contre l'ordinateur : télécharger le [carnet Jupyter](NSI1_Python_Jeu_de_Nim.ipynb).
## 3. QCM d'entrainement

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Programmation Python : boucles While"**.


