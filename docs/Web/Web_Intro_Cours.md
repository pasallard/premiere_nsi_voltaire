# Introduction à la programmation Web : HTML, CSS, Javascript :volcano:

![](data/meme.jpg){: .center width=60%}

## ^^1. Le langage HTML (Hyper Text Markup Langage)^^
Lorsque le client (Firefox par exemple) demande au serveur (site Web) le contenu d'une page web, ce dernier lui renvoie, dans le cas le plus simple, une simple page **html**.  

**HTML est un langage dit « à balises »**.  Ce n'est pas à proprement parler un langage de programmation, mais plutôt un langage de description de contenu. 


!!! info "Vidéo introductive"
    Regarder la [vidéo introductive](https://culturenumerique.univ-lille.fr/module2.html#sec_4){target="_blank"} du site Culture Numérique (partie 4.1. HTML: contenu, structure, liens).


Le langage HTML a été inventé en 1992 par [Tim Berners-Lee](https://fr.wikipedia.org/wiki/Tim_Berners-Lee){target="_blank"}. La version actuellement utilisée est le **HTML5**.

!!! done "Exemple de page Web minimale"

    Voici le code HTML d'une page minimale :

    ```html
    <!DOCTYPE html>
    <html lang='fr'>
        <head>
            <meta charset="utf-8">
            <title>Un titre pas très original</title>
        </head>

        <body>
            <p>Ceci est le texte introductif de ma page.</p>
            <p>
            <h1>  Ceci est un titre de niveau 1 </h1>
            Mais sans rien d'intéressant.
            <h2>  Ceci est un titre de niveau 2 </h2>
                <ul>
                  <li> le début d'une liste </li>
                  <li> la suite ... </li>
                </ul>  
            Pour apprendre le fonctionnement des balises, voir <a href="https://developer.mozilla.org/fr/docs/Learn/HTML/Introduction_to_HTML/Getting_started"> ici</a> par exemple !
            </p>
        </body>
    </html>

    ```
    Pour visualiser le rendu de cette page minimale : cliquer [ici](data/HTML_minimal.html){target="_blank"}.


!!! tip "Les balises HTML"
    HTML permet de définir la structure d'une page Web en la découpant en éléments logiques délimités par une `<balise>` ouvrante et une `</balise>` fermante. De manière générale, toute balise ouverte doit ensuite être fermée (_certaines balises sont auto-fermantes_).

Il existe deux grandes catégories de balises :

* __balises en-ligne__ (_inline_), qui ne sont pas affichées après un saut de ligne et qui occupent juste la place nécessaire à leur contenu ; deux éléments en-ligne qui se suivent sont positionnés l'un __à côté__ de l'autre ![blocs_sur](data/blocs_sur.jpg).
* __balises blocs__ (_block_), qui sont affichées après un saut de ligne et qui occupent toute la largeur disponibles ; deux éléments blocs qui se suivent sont positionnés l'un __au-dessus__ de l'autre ![blocs_sur](data/blocs_sous.jpg)

Les principales balises de type __en-ligne__ qu'il faut connaître :

<figure markdown>
| Balise   | Rôle           | Syntaxe                                         |
|----------|----------------------|-------------------------------------------------|
| `<a> `     | lien hypertexte      | `<a href="https:/fr.wikipedia.org">Mon Lien</a>` |
| `<img>`    | insertion d'image    | `<img src="data/tux_640.png" >` |
| `<strong>` | texte très important | `<strong>important</strong>`                      |
| `<em>`     | texte important      | `<em>important</em>`                              |
| `<br>`     | saut de ligne        | `<br>`                                           |
| `<span>`   | "étendue" (balise neutre)       | `<span>contenu</span>`                            |
</figure>

En général, le contenu de la balise `<strong>` est en gras et celui de la balise `<em>` en italique. La balise neutre `<span>` est  utile pour la mise en forme (_voir plus bac : le CSS_).


Les principales balises de type __bloc__ qu'il faut connaître :

<figure markdown>
| Balise   | Rôle           | Syntaxe                                         |
|----------|----------------------|-------------------------------------------------|
| `<p>`    | paragraphe         | `<p>mon paragraphe. </p>`                            |
| `<li>`   | élément d'une liste      | `<li>mon élément</li>`                                      |
| `<ol>`   | liste ordonnée     | `<ol><li>lundi</li><li>mardi</li></ol>`              |
| `<ul>`   | liste non ordonnée | `<ul><li>cahiers</li><li>calculatrice</li></ul>`             |
| `<div>`  | "division" (balise neutre)      | `<div><p>paragraphe 1</p> <p>paragraphe 2</p></div>` |
| `<h1>`   | titre de niveau 1  | `<h1> Partie A </h1>`                                |
| `<h2>`   | titre de niveau 2  | `<h2> Partie A.1 </h2>`                              |
</figure>

Il existe six niveaux de titres de `<h1>` à `<h6>` par ordre décroissant d'importance.

Comme la balise `<span>`, la balise neutre `<div>` est utile pour la mise en forme (CSS).

!!! tip "Pour aller plus loin"

    - les bases du HTML : [https://developer.mozilla.org/fr/docs/Learn/HTML/Introduction_to_HTML/Getting_started](https://developer.mozilla.org/fr/docs/Learn/HTML/Introduction_to_HTML/Getting_started){target="_blank"}
    - liste des balises HTML : [https://jaetheme.com/balises-html5/#liste-balises-html5](https://jaetheme.com/balises-html5/#liste-balises-html5){target="_blank"}
    - Pour trouver des images, son ou  vidéos libres de droit : [https://www.reformedulycee.fr/2019/11/documents-libres-de-droits-nos-sites-preferes/](https://www.reformedulycee.fr/2019/11/documents-libres-de-droits-nos-sites-preferes/){target="_blank"}


!!! question "Exercice (partie 1)"

    - Télécharger (_clic droit puis Enregister le lien sous_) le fichier [index.html](data/index.html), puis l'ouvrir d'abord avec un éditeur de texte basique (BlocNote, Notepad, etc.) puis avec un navigateur (Firefox). Cela vous donne une base pour commencer.
    - Créer deux pages Web sur un sujet de votre choix (musique, sport, histoire, ville ou pays,...). Il doit y avoir au moins une liste, une image, un son ou une vidéo, un lien vers un site Web extérieur et une circulation entre les deux pages.


## ^^2. HTML + CSS^^

!!! tip "Le fond et la forme"
    De manière très simplifiée, on peut dire que le fichier **HTML** s'occupe de la ***structure*** du document tandis que le fichier **CSS**  s'occupe de sa ***mise en forme*** :  couleurs, bordures, disposition des éléments HTML, etc.


L'acronyme CSS signifie _Cascading Style Sheets_. L'idée est de regrouper dans un seul fichier toutes les informations relatives à la mise en forme des éléments de la page HTML. 

Le fichier CSS (souvent nommé ```style.css```) doit être référencé au début du fichier HTML , au sein de la balise ```<head>```.


!!! info "Vidéo introductive"
    Regarder la [vidéo introductive](https://culturenumerique.univ-lille.fr/module2.html#subsec_4_3){target="_blank"} du site Culture Numérique (partie 4.3. Mise en forme).

!!! done " Exemple minimal de couple HTML + CSS"
    Dans cet exemple minimal, on a maintenant deux fichiers :
    
    - un fichier ```index.html``` : 
    ```html
    <!DOCTYPE html>
    <html lang='fr'>
        <head>
            <meta charset="utf-8">
            <title>Un titre pas très original</title>
            <!-- Nouveau : lien vers un fichier de style CSS -->
            <link rel="stylesheet" href="style.css">
        </head>

        <body>
            <p>Ceci est le texte <span class="special">introductif</span> de ma page.</p>
            <p>
            <h1>  Ceci est un titre de niveau 1 </h1>
            Mais sans  <span class="souligne">rien d'intéressant.</span>
            <h2>  Ceci est un titre de niveau 2 </h2>
 
            </p>
            <div id="bloc1">
                <ol>
                    <li> le début d'une <span class="souligne">liste</span> </li>
                    <li> la suite ... </li>
                </ol> 
            </div>
        </body>
    </html>
    ```

    - un fichier ```style.css``` :

    ```css
    h1 {
        text-align: center;
        color: red;
        background-color: darkgreen;
    }
  
    .special {
        color: orange;
        background-color: navy;
        border: solid black;
    }

    .souligne {
        text-decoration: underline red;
    }

    #bloc1 {
        width: 40%;
        border: 5px solid purple;
        background-color: darkturquoise;
        color: white;
        padding:20px;
    }
    ```

    Pour visualiser le rendu de ces fichiers, télécharger et dézipper le [dossier zippé](data/minimal_avec_css.zip).


De manière simplifiée, un fichier ```css``` consiste en un ensemble de règles de la forme : ![règle CSS](data/regleCSS.jpg){: .center width=60%}

!!! tip "Quelques règles CSS"
    Les sélecteurs les plus courants sont :

    - un ***élément*** HTML comme ```p``` pour cibler tous les paragraphes ou ```h1``` pour cibler tous les titres de niveaux 1;
    - un ***identifiant*** avec le préfixe `#` pour cibler _l'unique_ élément qui a été marqué par cet identifiant dans le document HTML avec la syntaxe ```<element id="monidentifiant">``` (exemple : ```bloc1``` dans l'exemple ci-dessus);
    - une ***classe*** avec le préfixe `.` pour cibler un _ensemble d'éléments_, pas forcément de même type, qui ont été marqués par cette classe dans le document HTML avec la syntaxe ```<element class="uneclasse">``` (exemple :  ```special``` ou ```souligne``` dans l'exemple ci-dessus). 

    Quant aux propriétés et aux valeurs possibles, elles sont très nombreuses. Ci-dessous un extrait du _tutoriel Mozilla_ : 

    > Vous verrez rapidement qu'avec les CSS, tout tourne autour de boîtes : définir leurs tailles, leurs couleurs, leurs positions, etc. Les éléments HTML d'une page peuvent, pour la plupart, être vus comme des boîtes placées les unes sur les autres.

    ![boite CSS](data/boiteCSS.jpg)

Chacun de ces blocs prend un certain espace sur la page, que l'on peut adapter de cette façon :

- ```padding``` (_remplissage_ en français) : l'espace autour, proche du contenu (par exemple, l'espace autour du texte d'un paragraphe) ;
- ```border``` (_bordure_) : la ligne qui est juste autour du padding ;
- ```margin``` (_marge_): l'espace extérieur, autour de l'élément.


!!! tip "Pour aller plus loin"
    - Tutoriel Mozilla : [https://developer.mozilla.org/fr/docs/Learn/Getting_started_with_the_web/CSS_basics](https://developer.mozilla.org/fr/docs/Learn/Getting_started_with_the_web/CSS_basics){target="_blank"}
    - Les couleurs en CSS : [http://www.css-faciles.com/couleurs-css.php](http://www.css-faciles.com/couleurs-css.php){target="_blank"}


!!! question "Exercice (partie 2)"
    Reprenez vos pages de l'exercice précédent (_partie 1_) et rajoutez une feuille de style.

## ^^3. HTML + CSS + JavaScript^^

Jusqu'à présent, la page web envoyée par le serveur est **statique** après réception sur l'ordinateur de l'utilisateur (le client).

Le JavaScript va permettre d'interagir __dynamiquement__ avec l'utilisateur, sans avoir à redemander une nouvelle page au serveur.

Inventé en 1995 par [Brendan Eich](https://fr.wikipedia.org/wiki/Brendan_Eich){target="_blank"} pour le navigateur Netscape (un navigateur maintenant disparu mais qui a servi de base à Firefox), le langage JavaScript s'est imposé comme la norme auprès de tous les navigateurs pour apporter de __l'interactivité__ aux pages web.


!!! done " Exemple minimal de couplage  HTML + CSS + JavaScript"
    Il y a maintenant trois fichiers : le fichier ```index.html``` fait référence, au sein d'une balise ```<script>```, à un fichier externe ```script.js``` qui contient le code JavaScript. Il y a aussi toujours une feuille de style CSS ```style.css```.  

    - fichier ```index.html``` : 
    ```html
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Test JS</title>
            <link rel="stylesheet" href="style.css">
        </head>

        <body>
            <h1>Premier test avec JavaScript</h1>
            <p id="monTexte">Et voici du texte</p>
            <button onclick="mettreStyleSpecial()">Special</button>
            <button onclick="enleveStyleSpecial()">Normal</button>
	    </body>

        <!-- le lien vers le fichier JavaScript -->
        <script src="script.js"></script>
    </html>
    ```

    - fichier ```style.css``` :
    ```css
    .specialColor {
    color: orange;
    background-color: navy;
    border: solid black;
    }
    ```

    - fichier ```script.js``` :
    ```javascript
    function mettreStyleSpecial() {
    document.getElementById("monTexte").classList.add('specialColor');
    }

    function enleveStyleSpecial() {
    document.getElementById("monTexte").classList.remove('specialColor');
    }
    ```
    Pour visualiser le rendu de ces fichiers, télécharger et dézipper le [dossier zippé](data/minimal_avec_JS.zip).


!!! note "Explications à propose de l'exemple minimal"
 
    - Dans le code HTML, on a introduit deux nouveaux objets grâce à la balise ```<button>``` : des boutons à cliquer. Chacun de ces boutons a un attribut ```onclick``` qui indique quelle fonction JavaScript est lancée quand on clique sur le bouton.
    - Dans le fichier CSS, une classe ```specialColor``` est définie, mais on constate qu'elle n'est pas utilisée dans le code HTML.
    - Dans le code JavaScript sont définies les fonctions qui vont ajouter ou retirer ce style CSS ```specialColor``` au paragraphe repéré par l'identifiant ```"monTexte"```.


La puissance du JavaScript permet de réaliser aujourd'hui des **interfaces** utilisateurs très complexes au sein d'un navigateur.


!!! tip "Pour aller plus loin"

    - le [guide JavaScript de la fondation Mozilla](https://developer.mozilla.org/fr/docs/Learn/JavaScript){target="_blank"}

!!! question "Exercice (partie 3)"
    Reprenez vos pages de l'exercice précédent (_partie 2_) pour les rendre plus interactives.

    Vous trouverez d'autres exemples simples d'utilisation de Javascript dans ce [dossier zippé](data/ExemplesJS.zip).



## ^^4. QCM d'entrainement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Intro à la programmation Web"**.