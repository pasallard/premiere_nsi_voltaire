def recherche_dichotomique_avec_affichage(tab, val):
    """
    Entrées : 
        tab est un tableau de nombres trié par ordre croissant 
        val est un nombre
    Sortie : un booléen qui vaut True si val appartient au tableau et False sinon
    """
    debut = 0
    fin = len(tab) - 1
    nb_op = 0 # comptage du nombre de tours de boucle
    while debut <= fin: 
        milieu = (debut + fin) // 2
        nb_op +=1 # incrémentation du nombre de tours de boucle
        print("tour de boucle = ", nb_op)
        if tab[milieu] == val:
            return True
        elif val > tab[milieu]:
            debut = milieu + 1
        else:
            fin = milieu - 1
    # la valeur ne se trouve pas dans le tableau
    return False

from random import randint

for taille in [64, 256]:
    print('Tableau de taille :', taille)
    tab = sorted([randint(1, taille) for k in range(taille)])
    val = 0 # val est choisi pour être certain que l'on est dans le pire des cas : cette valeur n'est pas dans le tableau
    trouvé = recherche_dichotomique_avec_affichage(tab, val)
    print(trouvé)