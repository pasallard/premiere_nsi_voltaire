# Illustration du tri par sélection

from tkinter import *
from random import randint
import time

largeur=1200
hauteur=600
police = "Helvetica"

def tri_selection(tableau): 
    nbE=0
    rep=[]
    rep.append((tableau[:],0,0,nbE,"depart"))
    nb = len(tableau)
    for en_cours in range(nb):
        rg_plus_petit = en_cours
        rep.append((tableau[:],en_cours,rg_plus_petit,nbE,"debut_rg_plus_petit"))
        for j in range(en_cours+1,nb) :
            nbE+=1
            rep.append((tableau[:],en_cours,j,rg_plus_petit,nbE,"test"))
            if tableau[j] < tableau[rg_plus_petit] :
                rg_plus_petit = j
                rep.append((tableau[:],en_cours,j,rg_plus_petit,nbE,"etape"))
                rep.append((tableau[:],en_cours,j,rg_plus_petit,nbE,"etapeb"))

        if rg_plus_petit != en_cours :
            nbE+=1
            for x in range ((rg_plus_petit-en_cours)*4+1):
                rep.append((tableau[:],en_cours,rg_plus_petit,x,nbE,"changement"))
            temp = tableau[en_cours]
            tableau[en_cours] = tableau[rg_plus_petit]
            tableau[rg_plus_petit] = temp

        rep.append((tableau[:],en_cours,rg_plus_petit,nbE,"fin_changement"))
    rep.append((tableau[:],en_cours,rg_plus_petit,nbE,"fin"))

    return rep

def modif_speed():
    global speed_animation, speed_low
    speed_low = not(speed_low)
    if speed_low :
        speed_animation = 5
    else :
        speed_animation = 1     
    return None

    
def show_info():
    global detail_info
    detail_info = not(detail_info)
    return None

def toogle_animation():
    global enPause
    if enPause :
        enPause = not(enPause)
    else :
        enPause = not(enPause)
    jouer()
    return None

def ListeNb():
    listenb=[]
    while len(listenb)!=16:
        nb=randint(-99,99)
        if nb not in listenb:
            listenb.append(nb)
    return listenb

def Partie():
    global rep,liste,rang
    global detail_info, enPause
    global speed_animation, speed_low
    detail_info = False
    enPause = True
    speed_animation = 5
    speed_low = True
    bou1["state"]=DISABLED
    bou2["state"]=NORMAL
 
    listenb=ListeNb()
    rep=tri_selection(listenb)
    liste=rep[0]
    rang=0
    jouer()
    return None

def jouer():
    global rep,liste,rang
    
    def bouger():
        global rang, rep,liste, enPause
        if enPause :
            dessiner(liste)
        else :
            can.delete("all")
            rang+=1
            if rang<len(rep):
                liste=rep[rang]
                dessiner(liste)
                if liste[-1] == "changement":
                    can.after(20*speed_animation,bouger)
                else :
                    can.after(100*speed_animation,bouger)
            else:
                liste=rep[len(rep)-1]
                dessiner(liste)
        return None

    def dessiner(liste):
        global rang,rep
        a=(largeur-200)/16
        H=(hauteur+a)/2
        H2=H-a

        def rectangle(i,Width,Fill,Outline):
            can.create_rectangle(100+a*i,hauteur/2,100+a*(i+1),hauteur/2+a,width=Width,fill=Fill,outline=Outline)
            return None
        def texte(i,H,Text,Fill):
            can.create_text(100+a/2+a*i,H,text=Text,fill=Fill,font=(police,str(hauteur//22),"bold"))
            return None
        def affichage_info(enCours):
            can.create_text(100+a/2+a*enCours,H-2*a,text="i = "+str(enCours),fill="red",font=(police,str(hauteur//22),"bold"))
            can.create_text(largeur/2,hauteur/5,text="comparaisons/échanges :  "+str(liste[-2]),fill="#DABEA0",font=(police,str(hauteur//18),"bold"))
            if liste[-1] in ["test", "etape", "etapeb"]:
                can.create_text(largeur/2,hauteur/5+a/2,text="Recherche du nouveau Minimum dans la partie droite du tableau ",fill="#DABEA0",font=(police,str(hauteur//18),"bold"))
            if liste[-1] == "changement":
                can.create_text(largeur/2,hauteur/5+a/2,text="Échange des deux valeurs",fill="#DABEA0",font=(police,str(hauteur//18),"bold"))
            return None
        
        color2 = "grey60"
        color3 = "grey30"
        color4 = "black"

        for i in range(16):
            rectangle(i,1,color2,color3)
            texte(i,H,liste[0][i],"black")

        for i in range(16):
            if detail_info :
                affichage_info(liste[1])

            if i<liste[1]:
                rectangle(i,1,color3,color4)
                texte(i,H,liste[0][i],"white")

            if liste[-1]=="debut_plus_petit":
                if i==liste[1]:
                    texte(i,H,liste[0][i],"red")
                    rectangle(i,3,"","red")

            if liste[-1]=="test":
                if i==liste[1]:
                    texte(i,H2,liste[0][i],"red")
                    rectangle(i,1,color2,color3)
                elif i==liste[3]:
                    texte(i,H2,liste[0][i],"maroon")
                    rectangle(i,1,color2,color3)
                elif i==liste[2]:
                    texte(i,H,liste[0][i],"green")
                    rectangle(i,3,"","green")

            if liste[-1]=="etape":
                if i==liste[1]:
                    texte(i,H2,liste[0][i],"red")
                    rectangle(i,1,color2,color3)
                elif i==liste[2]:
                    texte(i,H,liste[0][i],"maroon")
                    rectangle(i,3,"","maroon")

            if liste[-1]=="etapeb":
                if i==liste[1]:
                    texte(i,H2,liste[0][i],"red")
                    rectangle(i,1,color2,color3)
                elif i==liste[2]:
                    texte(i,H2,liste[0][i],"maroon")
                    rectangle(i,1,color2,color3)

            if liste[-1]=="changement":
                if i==liste[1]:
                    texte(i+liste[-3]/4,H2,liste[0][i],"maroon")
                    rectangle(i,1,color2,color3)
                elif i==liste[2]:
                    texte(i-liste[-3]/4,H2,liste[0][i],"red")
                    rectangle(i,1,color2,color3)

            if liste[-1]=="fin_changement":
                if i==liste[1]:
                    texte(i,H,liste[0][i],"red")

            if liste[-1]=="fin":
                if i==liste[1]:
                    rectangle(i,1,color3,color4)
                    texte(i,H,liste[0][i],"white")
        return None
    # Corps de la fonction jouer
    
    bouger()
    return None



fen=Tk()
xmax = fen.winfo_screenwidth()
ymax = fen.winfo_screenheight()
x0 =  xmax/2-(largeur/2)
y0 =  ymax/2-(hauteur/2)
fen.geometry("%dx%d+%d+%d" % (largeur,hauteur, x0, y0))
tex1 = Label(fen, text="\n <<oo>>  tri par sélection  <<oo>>",font=(police,"35","bold"),fg="ivory4")
tex1.pack()
can=Canvas(height=hauteur-200,width=largeur)
can.pack()


bou1= Button(fen, text='TIRAGE ALEATOIRE', bg="ivory3", fg="white",font=(police,"25"),bd=0,command=Partie)
bou1.pack(side=LEFT,expand=YES, fill=BOTH)


bou2= Button(fen, text='RUN/PAUSE',  bg="ivory3", fg="white",font=(police,"25"),bd=0, state=DISABLED, command=toogle_animation)
bou2.pack(side= LEFT,expand=YES, fill=BOTH)

bou3= Button(fen, text='SPEED',  bg="ivory3", fg="white",font=(police,"25"),bd=0,command=modif_speed)
bou3.pack(side= LEFT,expand=YES, fill=BOTH)

bou4= Button(fen, text='INFOS', bg="ivory3", fg="white",font=(police,"25"),bd=0,command=show_info)
bou4.pack(side=LEFT,expand=YES, fill=BOTH)




fen.mainloop()




