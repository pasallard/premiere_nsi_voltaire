# Algorithmique :roller_coaster:

Dans cette partie, sont traités les thèmes :

* introduction à l'algorithmique :fish_cake:
* algorithmes de tri d'un tableau de données :fork_and_knife:
* algorithme de recherche par dichotomie :broken_heart:
* algorithmes gloutons :boar: