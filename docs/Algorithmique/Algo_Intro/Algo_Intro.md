# Introduction à l'algorithmique :fish_cake:

!!! tip "Notion d'algorithme"
    Un __algorithme est une suite d’instructions élémentaires__ appliquées dans un ordre déterminé, portant sur un nombre __fini__ de données pour arriver, en un __nombre fini d'étapes__, à un certain résultat.

## ^^1. Brève histoire de l'algorithmique^^

Les premiers algorithmes ont été développés __bien avant l'émergence de l'informatique__.

De plus, vous manipulez des algorithmes depuis votre prime enfance : __une recette de cuisine est un exemple concret d'algorithme !__

==**Vers -1800**==

Les plus anciens algorithmes connus remontent il y a presque quatre millénaires.
Les __Babyloniens__ qui vivaient en Mésopotamie (actuel Irak) utilisaient des __algorithmes pour résoudre certaines équations__ (comme celles du second degré).

Voici l'image d'une __tablette__ datant de cette période où plusieurs problèmes du second degré sont résolus par une sorte de __liste d'instructions__ proche de nos algorithmes actuels : 

![tablette BM_13901 actuellement au British Museum](data/Babylone.png){: .center}

==**Vers -300**==

__Euclide__ a proposé entre autre un algorithme, encore utilisé de nos jours, permettant de déterminer le __plus grand commun diviseur__ (le PGCD) entre deux nombres entiers. 

Vous avez sans doute vu cet algorithme d'Euclide au collège, qui repose sur une succession de divisions euclidiennes. Prenons l'exemple du calcul du PGCD de 21 et de 15 :

$$ 21 = 15 \times 1 + 6$$

$$15 = 6 \times 2 + 3$$

$$6 = 3 \times 2  + 0$$


et le PGCD de 21 et 15 vaut 3 car c'est le dernier reste non nul.


En voici une illustration : 

![Euclide](data/Euclide.png){: .center}

==**Vers 800**==

Le mot __algorithme__ vient du nom latinisé du mathématicien __Al-Khwârizmî__. 

![Al-Khwarizmi](data/Al-Khwarizmi.png){: .center}

Ce savant ayant vécu entre 780 et 850 fut membre de la Maison de la Sagesse de Bagdad. Il répertoria les algorithmes connus à son époque et, entre autres travaux, il fut l'auteur entre autre de deux livres importants :

- le premier a conduit au __mot « algèbre »__ actuel ;
- le second a permis la diffusion du système de numération décimal actuel à travers le monde abbasside puis en Europe : ce sont __les « chiffres arabes »__ actuels.



## ^^2. Algorithmique et programmation^^

Les __algorithmes__ ont souvent pour objectif d'être __traduit en programmes__, exécutables.

Le seul langage directement utilisable par le processeur des ordinateurs est le __langage machine__ (_hors-programme en Première_).

Pour faciliter la communication d'informations avec un ordinateur, des informaticiens ont créé des __langages dits de haut niveau__ qui sont plus simples à utiliser, car plus proches du langage naturel.

Il y en a un très grand nombre : Fortran (1955), C/C++ (1972), Java (1995), Javascript (1995), ... Et bien sûr, celui que nous utilisons cette année : le langage Python, créé en 1991 par Guido Von Rossum.

## ^^3. Le pseudo-code^^

Puisqu'il y a un très grand nombre de langages de programmation différents, il est commode d'utiliser une sorte de __langage "universel"__ qui permet d'écrire un algorithme. Le pseudo-code sert à cela.

!!! tip "Pseudo-code"
    Le __pseudo-code__ est proche d'un langage de programmation, sans être identique à l'un ou à l'autre. Il exprime des idées formelles dans __une langue proche du langage naturel__ de ses usagers (pour nous, le français) en lui imposant une forme rigoureuse.


!!! done "Exemple"

    Le parcours séquentiel d'un tableau, qui s'écrit en Python
    ```python
    def parcours(tab) :
        for i in range(len(tab)):  # on parcourt les indices de la liste
            print(tab[i])
    ```

    s'écrit en pseudo-code :

        Parcours(tab)
            POUR i ALLANT DE 1 À longueur(tab)  
                Afficher tab[i]  
            FIN_POUR  
        
!!! warning "Pas de règles strictes mais des conventions"
    Il n'y a __pas de standard normalisé mais seulement des conventions__ partagées par un grand nombre de programmeurs.

    Par exemple, il est fréquent que, en pseudo-code, le rédacteur de l'algorithme fasse le choix de numéroter de 1 à N (plutôt que de 0 à N - 1 comme en Python) les éléments d'un tableau de longueur N.
    
    ^^Quelques règles du pseudo-code :^^

    - Les instructions se font __une ligne à la fois__.
    - On ne se préoccupe pas des types des variables (nombre, chaine de caractères, tableau, ...).
    - Les mots-clés (SI...ALORS, TANT_QUE, etc) doivent être écrits __en gras ou en majuscules__.
    - La fin d'une boucle POUR ou TANT_QUE est clairement identifiée par un mot-clé FIN_POUR et FIN_TANT_QUE ; de même, un SI... ALORS se termine par un FIN_SI. Mais on n'est pas obligé d'indenter les lignes pour délimiter les blocs d'instructions.
    - L'affectation d'une valeur à une variable est représentée par le symbole __←__ ou bien __=__. 


## ^^4. Un exemple : l'algorithme d'Euclide de calcul du PGCD^^

### 4.1. Énoncé de l'algorithme d'Euclide

En mathématiques, l'algorithme d'Euclide est un algorithme qui calcule le __plus grand commun diviseur__ (PGCD) de deux entiers, c'est-à-dire le plus grand entier qui divise les deux entiers, en laissant un reste nul. 

Il s'agit de __divisions euclidiennes en cascade__, les résultats de l'une servent à poser la suivante. On cherche ci-dessous le PGCD de A et B, avec A et B des entiers strictement positifs.

- On calcule le reste $r_{1}$ dans la division euclidienne du dividende A par le diviseur B.
- Le diviseur B devient le dividende, le reste $r_{1}$ devient le diviseur, et on recommence.
- Arrêt lorsque le reste $r_{i}$ de la division est nul.
- Le dernier reste non nul $r_{i-1}$ est le PGCD des nombres A et B.

En voici une étape posée :

![Euclide](data/Division_Euclide.png){: .center}


### 4.2. Codage de l'algorithme d'Euclide

En __pseudo-code__, cet algorithme devient :

    Calcul_PGCD(a,b)
        c ← reste de a dans la division euclidienne par b
        TANT_QUE c ≠ 0
            a ← b
            b ← c
            c ← reste de a dans la division euclidienne par b
        FIN TANT_QUE
        Renvoyer b   # Renvoie le dernier reste non nul qui est le PGCD
  

!!! question "Exercice : codage en Python du calcul du PGCD"
    
    Coder une fonction `PGCD(a, b)` exécutant l'algorithme d'Euclide. 

    {{IDE("data/algo_pgcd_void")}}

    ??? danger "Solution"

        ```python
        def PGCD(a,b) :
            c = a%b
            while c !=0 :
                a = b
                b = c
                c = a%b
            return b
        ```

## ^^5 - Algorithmes liés au parcours séquentiel d'un tableau^^ :heart:

Les trois algorithmes présentés ci-dessous sont au programme officiel de NSI et doivent être connus __par cœur__.

### 5.1 - Algorithme de recherche d'une valeur dans un tableau

Cet algorithme doit, à partir d'un tableau `tab` et d'une valeur `val`, renvoyer VRAI si la valeur `val` est l'un des éléments du tableau `tab` et FAUX sinon.

En __pseudo-code__, cet algorithme s'écrit :

    Recherche(tab,val)
        POUR k ALLANT DE 1 À longueur(tab)  # choix de commencer la numérotation du tableau à 1
            SI tab[k] = val ALORS
                RENVOYER Vrai
            FIN_SI
        FIN_POUR  
        RENVOYER Faux 
        
!!! question "Exercice : code Python de l'algorithme de recherche d'une valeur dans un tableau"

    Écrire le code Python de cet algorithme.

    {{ IDE("data/algo_recherche_void")}}

    ??? danger "Solution"

        ```python
        def recherche(tab,val) :
            for k in range(len(tab)):
                if tab[k] == val :
                    return True
            return False
        ```


### 5.2 - Algorithme de calcul d'un extremum dans un tableau

Cet algorithme doit, à partir d'un tableau `tab` non vide de nombres, calculer le minimum (ou le maximum) des nombres du tableau.

En __pseudo-code__, l'algorithme de calcul du minimum s'écrit :

    Minimum(tab)
        mini ← tab[1]  # choix de commencer la numérotation du tableau à 1
        POUR k ALLANT DE 1 À longueur(tab)  
            SI tab[k] <  mini ALORS
                mini ← tab[k]
            FIN_SI
        FIN_POUR  
        RENVOYER mini  


!!! question "Exercice : code Python de l'algorithme de calcul du minimum des valeurs d'un tableau"

    Donner le code Python de cet algorithme.

    {{ IDE("data/algo_maxi_void")}}
    ??? danger "Solution"

        ```python
        def minimum(tab) :
            mini = tab[0]
            for k in range(len(tab)) :
                if tab[k] < mini :
                    mini = tab[k]
            return mini
        ```

### 5.3 - Algorithme de calcul de la moyenne des valeurs d'un tableau

Cet algorithme doit, à partir d'un tableau `tab` non vide de nombres, déterminer la moyenne des nombres du tableau.

En __pseudo-code__, l'algorithme de calcul de la moyenne s'écrit :

    Moyenne(tab)
        somme ← 0 
        POUR k ALLANT DE 1 À longueur(tab)  # choix de commencer la numérotation du tableau à 1
            somme ← somme + tab[k]
        FIN_POUR
        RENVOYER somme/longueur(tab)  
        

!!! question "Exercice : code Python de l'algorithme de calcul de la moyenne des valeurs d'un tableau"

    Donner le code Python de cet algorithme.

    {{ IDE("data/algo_moyenne_void")}}

    ??? danger "Solution"

        ```python
        def moyenne(tab) :
            somme = 0
            for k in range(len(tab)) :
                somme = somme + tab[k]
            return somme/len(tab)
        ``` 

## ^^6 - Notion de complexité d'un algorithme^^

La notion de __complexité algorithmique__ est une sorte de __quantification de la performance d'un algorithme__.

!!! tip "Définition du coût d'un algorithme"
    Le coût d'un algorithme est l'ordre de grandeur du nombre d'opérations arithmétiques ou logiques que doit effectuer un algorithme pour résoudre le problème auquel il est destiné.

Cet ordre de grandeur dépend évidemment de la taille $n$ des données (souvent un tableau de valeurs) en entrée :

- Si ce __coût est proportionnel est à $n$__, on parlera de __coût linéaire__ et il noté __O($n$)__.
- Si ce __coût est proportionnel est à $n^2$__, on parlera de __coût quadratique__ et il noté __O($n^2$)__.
- Si toutefois, le nombre d'opérations élémentaires ne dépend pas de la taille des données d'entrée (ou s'il n'y a pas de données d'entrée !), on parlera de coût constant, et il sera noté __O(1)__.
  
> Remarque : la notation O(...) est appelée notation de Landau et se lit "O de...". Cette notation détermine la _classe_ de complexité de l'algorithme.

Connaître la complexité d'un algorithme permet :

- pour un algorithme donné, de pouvoir anticiper le temps de calcul d'un programme si la taille des données d'entrée est multipliée par 2, par 10, par 100, etc.
- de pouvoir comparer l'efficacité de deux algorithmes différents qui remplissent le même rôle (_voir le graphique ci-dessous_). On préférera alors utiliser l'algorithme qui a la plus faible complexité.

![Classes de complexité](data/ordres-de-complexite.png){: .center}

!!! tip "Complexité des algorithmes liés au parcours séquentiel d'un tableau :heart:" 
    Les algorithmes de parcours séquentiels d'un tableau (ceux de la partie 5) ont un __coût linéaire__, c'est-à-dire proportionnel à la taille du tableau.


Nous vérifierons ceci expérimentalement en TP. Cela se comprend bien _"intuitivement"_ : le temps de calcul est proportionnel au nombre de tours de boucle FOR qu'il faut effectuer, et lui-même est proportionnel à la taille du tableau. 

!!! question "Exercice"
    Pour effectuer un calcul de moyenne sur un tableau de 5000 nombres, un ordinateur a mis un certain temps T.

    {{ multi_qcm(
        [
            "Quel temps lui faudra-t-il approximativement pour effectuer un calcul de moyenne sur un tableau comportant cinq millions de nombres ?",
            ["le même temps T", "environ 5×T", "environ 100×T", "environ 1000×T"],[4],
        ],
        shuffle = False)}}

??? danger "Explication"
    Pour passer de 5000 à 5 millions, il faut multiplier par 1000. La taille du tableau est multipliée par 1000, donc le temps de calcul est aussi multiplié par 1000 car les deux quantités sont proportionnelles (***algorithme de complexité linéaire***).

## ^^7 - Exercices et TP^^

Télécharger le [carnet Jupyter](Algo_Intro_TP_Exos.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

## ^^8 - QCM d'entrainement^^

Sur la [page](https://pasallard.gitlab.io/qcm_nsi_premiere/){target="_blank"}, faire le QCM **"Introduction à l'algorithmique"**.