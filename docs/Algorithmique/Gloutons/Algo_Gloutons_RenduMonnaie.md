# Les algorithmes gloutons :boar:


## ^^1. Un exemple classique d'algorithme glouton : le problème du rendu de monnaie^^

Lors d'un achat en argent liquide, il arrive souvent que l'on soit obligé de rendre la monnaie. Supposons qu'un achat induise un rendu de 49 € : quelles pièces peuvent être rendues ? 

La réponse, bien qu'évidente, n'est pas unique. Quatre billets de 10 €, 1 billet de 5 € et deux pièces de 2 € conviennent. Mais quarante-neuf pièces de 1 € conviennent également ! Et il y a d'autres possibilités...

Si la question est de __rendre la monnaie avec un minimum de pièces__, le problème change de nature.

Comment rendre la monnaie en utilisant le plus petit nombre de pièces ? Voici une proposition d'algorithme, qualifié d'_algorithme glouton_ : 

!!! tip "Principe de l'algorithme glouton de rendu de monnaie"
    On choisit la plus grande pièce disponible inférieure à la somme à rendre, puis on recommence sur la somme qui reste à rendre.

!!! question "Quelques essais à la main"
    Avec un tel algorithme, quel va être le rendu de 49€ ? Et de 63 € ?

??? danger "Solution"
    Avec cet algorithme, le rendu de 49€ sera : deux billets de 20€, un billet de 5€ et deux pièces de 2 €.
    Et le rendu de 63 € sera : un billet de 50€, un billet de 10€, une pièce de 2€ et une pièce de 1 €.


## ^^2. Description des entrées et sorties de l'algorithme^^

Comme données de base de l'algorithme, on aura :

- le __système de monnaie__ utilisé, représenté par un __tableau de nombres entiers classés par ordre décroissant__.
Par exemple, `systeme_monnaie = [200, 100, 50, 20, 10, 5, 2, 1]`.

- la quantité N d'argent à rendre, où N est un nombre entier (on avait pris N = 49 dans l'exemple d'introduction).

L'algorithme devra être capable de gérer n'importe quel système de monnaie et n'importe quelle quantité d'argent à rendre.

La sortie de l'algorithme devra être un tableau de nombres entiers énumérant chaque pièce ou billet utilisé. Par exemple, avec le système de monnaie ci-dessus, on veut que l'appel `rendu_monnaie(49)` renvoie le tableau `[20,20,5,2,2]`.


## ^^3.  Une version d'algorithme glouton de rendu de monnaie, avec une boucle TANT QUE^^

!!! tip "Rendu de monnaie, avec une boucle TANT QUE"
    Voici en pseudo-code un premier algorithme de rendu, utilisant une boucle TANT QUE.

        FONCTION RENDU_MONNAIE(somme_a_rendre)

            systeme_monnaie ← [200, 100, 50, 20, 10, 5, 2, 1]
            pieces_rendues ← []
            i ←  0

            TANT_QUE somme_a_rendre > 0
                SI somme_a_rendre ≥ systeme_monnaie[i]
                    AJOUTER LA VALEUR systeme_monnaie[i] À pieces_rendues
                    somme_a_rendre ← somme_a_rendre − systeme_monnaie[i] 
                SINON
                    i ← i + 1
                FIN_SI
            FIN_TANT_QUE
            RENVOYER pieces_rendues


Vérifiez que vous avez bien compris la logique de fonctionnement de cet algorithme, puis complétez sa traduction en Python.

{{ IDE("data/glouton1")}}

??? danger "Solution"

    ```python
    def rendu_de_monnaie(somme_a_rendre):
        '''
        Calcule le nombre de pièces / billets à rendre pour une somme donnée
        Entrée : nombre entier, somme sur laquelle il faut rendre la monnaie
        Sortie : liste de l'ensemble des pièces / billets à rendre

        '''
        systeme_monnaie = [200, 100, 50, 20, 10, 5, 2, 1]
        pieces_rendues = []
        i = 0
        while somme_a_rendre > 0:
            if somme_a_rendre >= systeme_monnaie[i]:
                pieces_rendues.append(systeme_monnaie[i])
                somme_a_rendre -= systeme_monnaie[i]           
            else:
                i += 1 # on passe à la pièce suivante du système de monnaie
        return pieces_rendues
    ```


!!! question "Exercice"

    === "Question"
        Que se passerait-il si on enlevait les pièces de 1€ dans notre système de monnaie et qu'on lance l'algorithme sur un rendu de 11 € ?
    === "Réponse"
        On obtient une erreur `list index out of range`. En effet, quand il ne reste plus que 1 € à rendre, le programme passe toujours dans la partie `else` : la variable `i` s'incrémente à chaque fois jusqu'à atteindre la longueur du tableau `systeme_monnaie`. Or une instruction du type `tab[len(tab)]` est interdite !

        Cet algorithme suppose donc que le système de monnaie est conçu de telle sorte qu'une solution existe toujours !



## ^^4.  Une autre version d'algorithme glouton de rendu de monnaie, avec une boucle POUR^^

!!! tip "Rendu de monnaie, avec une boucle POUR"
    Voici en pseudo-code un autre algorithme de rendu, utilisant cette fois une boucle POUR.

        FONCTION RENDU_MONNAIE(somme_a_rendre)

            systeme_monnaie ← [200, 100, 50, 20, 10, 5, 2, 1]
            N ← Longueur de systeme_monnaie
            pieces_rendues ← []
        
            POUR k ALLANT de 0 à N-1 # choix de numéroter de 0 à N-1 les cases de systeme_monnaie
                val_piece ← systeme_monnaie[k]
                nb_piece ← Quotient entier de la division de somme_a_rendre par val_piece
                SI nb_piece > 0 # si il y a besoin d'utiliser cette pièce
                    AJOUTER nb_piece FOIS LA VALEUR val_piece À pieces_rendues
                    somme_a_rendre ← somme_a_rendre − nb_piece * val_piece 
                FIN_SI
            FIN_POUR
            RENVOYER pieces_rendues


Vérifiez que vous avez bien compris la logique de fonctionnement de cet algorithme, puis complétez sa traduction en Python.


{{ IDE("data/glouton2") }}


??? danger "Solution"

    ```python
    def rendu_de_monnaie_v2(somme_a_rendre):
        '''
        Calcule le nombre de pièces / billets à rendre pour une somme donnée
        Entrée : nombre entier, somme sur laquelle il faut rendre la monnaie
        Sortie : liste de l'ensemble des pièces / billets à rendre

        '''
        systeme_monnaie = [200, 100, 50, 20, 10, 5, 2, 1]
        pieces_rendues = []

        for k in range(len(systeme_monnaie)) :
            val_piece = systeme_monnaie[k]
            nb_piece = somme_a_rendre // val_piece 
            if nb_piece > 0 : 
                pieces_rendues = pieces_rendues + [val_piece]*nb_piece
                somme_a_rendre -= nb_piece*val_piece
        return pieces_rendues
    ```

!!! question "Exercice"
    === "Question"
        Que se passerait-il, avec ce deuxième algorithme, si on enlevait les pièces de 1€ dans notre système de monnaie et qu'on lance l'algorithme sur un rendu de 11 € ?
    === "Réponse"
        La version 2 de l'algorithme ne renvoie pas une erreur dans  le cas d'un rendu de 11€ avec un système de monnaie où il n'y a pas de pièces de 1€. 

        Il oublie simplement de dire qu'il n'a pas pu rendre parfaitement la monnaie : on peut modifier le code pour qu'il renvoie un couple `(pieces_rendues, somme_à_rendre)` et ainsi être alerté d'une "arnaque".




## ^^5. Une solution pas forcément optimale^^

Si le système de monnaie est "mal fait", l'algorithme de rendu peut proposer une solution qui utilise beaucoup plus de pièces que ce qu'un humain pourrait trouver.

!!! question "Exercice : comment rendre 18 Brouzoufs ?"

    Imaginons un pays dont la monnaie s'appelle le Brouzouf et où il n'y ait que des pièces de 1, 9 et 10 Brouzoufs.

    Comment rendre 18 Brouzoufs ?

    1. Donner votre solution d'être humain.
    2. Donner la solution d'un des deux algorithmes ci-dessus.
    3. Laquelle des deux solutions répond mieux au problème ?

??? danger "Solution"
    Le rendu proposé par l'algorithme utilise 9 pièces de 1 Brouzoufs, alors qu'un humain aurait utilisé seulement 2 pièces de 9 Brouzoufs.

!!! info "Analyse critique de l'algorithme glouton du rendu de monnaie"

    - Le système de monnaie (type de pièces disponibles) permet ou non de répondre de façon correcte au problème initial de minimisation du nombre de pièces. 
    - Dans le système de pièces européen, l'algorithme glouton donne toujours une solution optimale.


## ^^6. QCM^^


{{ multi_qcm(
    [
        """
        Dans un pays imaginaire, la monnaie utilisée est le ducat et il existe des pièces de 1,4 et 6 ducats.

        On considère le problème où l'on doit rendre 8 ducats de monnaie.

        Indiquer le rendu de monnaie donné par un algorithme glouton.
        """,
        ["4;4","4;1;1;1;1","6;1;1","1;1;1;1;1;1;1;1"],[3],
    ],
    [
        """
        On souhaite écrire une fonction `#!py monnaie` qui prend en paramètre un nombre `#!py s` représentant une somme d'euros à rendre et qui renvoie le **nombre** de pièces qui vont être utilisées pour rendre cette somme d'argent en appliquant la méthode gloutonne vue en cours.

        Voici trois fonctions Python, mais une seule répond à la consigne demandée :

        ```python
        euros = [1, 2, 5, 10, 20, 50, 100] 

        def monnaie_1(s) :	    	
            i = len(euros) - 1        		
            p = 0                     		
            while s > 0 :
                if s >= euros[i] :    		
                    p +=1   	
                    s -= euros[i]     	
                else :
                    i = i - 1
            return p

        def monnaie_2(s) :	    	
            i = 0       		
            p = 0                     		
            while s > 0 :
                if s >= euros[i] :    		
                    p +=1   	
                    s -= euros[i]     	
                else :
                    i = i + 1
            return p

        def monnaie_3(s) :	    	
            i = len(euros) - 1        		
            p = 0                     		
            while s > 0 :
                if s > euros[i] :    		
                    p +=1   	
                    s -= euros[i]     	
                else :
                    i = i - 1
            return p
        ```

        Quelle est la fonction correcte ?
        """,
        ["monnaie_1","monnaie_2","monnaie_3"],[1],
    ],
    shuffle = False)}}

## ^^7. TP : le rendu de monnaie et les nombres romains^^

Télécharger le [carnet Jupyter](TP_algo_gloutons.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.