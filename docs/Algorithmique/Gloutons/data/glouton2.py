def rendu_de_monnaie_v2(somme_a_rendre):
    '''
    Calcule le nombre de pièces / billets à rendre pour une somme donnée
    
    Entrée : entier, somme sur laquelle il faut rendre la monnaie
    Sortie : liste de l'ensemble des pièces / billets à rendre

    '''
    systeme_monnaie = [200, 100, 50, 20, 10, 5, 2, 1]
    pieces_rendues = []
    pass #compléter à partir d'ici

#un test
r49 = rendu_de_monnaie_v2(49)
print(r49)
assert r49 == [20, 20, 5, 2, 2], "problème de code"